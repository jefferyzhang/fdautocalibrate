#include "stdafx.h"
#include <atlstr.h>
#include "cheader.h"
#include "IniRecord.h"

CIniRecord inirecords;

CIniRecord::CIniRecord():amount(0) {

}

int CIniRecord::InitIniFile(CString sIni, int nstart) {
	int ret = ERROR_SUCCESS;
	ENTER_FUNCTION();
	if (INVALID_FILE_ATTRIBUTES == GetFileAttributes(sIni) && GetLastError() == ERROR_FILE_NOT_FOUND)
	{
		//File not found
		return ret;
	}
	amount = nstart;
	for (int i = 1; i < nstart; i++) {
		CString strLab; strLab.Format(_T("%d"), i);
		TCHAR Item[1024] = { 0 };
		GetPrivateProfileString(_T("label"), strLab, _T(""), Item, 1024, sIni);
		labels[i] = Item;
		memset(Item, 0, 1024);
		GetPrivateProfileString(_T("label_3.0"), strLab, _T(""), Item, 1024, sIni);
		labels_3[i] = Item;
		memset(Item, 0, 1024);
		GetPrivateProfileString(_T("label_2.0"), strLab, _T(""), Item, 1024, sIni);
		labels_2[i] = Item;
		memset(Item, 0, 1024);
		GetPrivateProfileString(_T("locationpaths"), strLab, _T(""), Item, 1024, sIni);
		locationpaths[i] = Item;
		memset(Item, 0, 1024);
		GetPrivateProfileString(_T("locationpaths_2.0"), strLab, _T(""), Item, 1024, sIni);
		locationpaths_2[i] = Item;
		memset(Item, 0, 1024);
		GetPrivateProfileString(_T("locationpaths_3.0"), strLab, _T(""), Item, 1024, sIni);
		locationpaths_3[i] = Item;
		memset(Item, 0, 1024);
	}
	return ret;
}

void  CIniRecord::WriteToMap(CString appname, int i, CString s) {
	if (appname.CompareNoCase(_T("label")) == 0) {
		labels[i] = s;
	}else if (appname.CompareNoCase(_T("label_3.0")) == 0) {
		labels_3[i] = s;
	}
	else if(appname.CompareNoCase(_T("label_2.0")) == 0) {
		labels_2[i] = s;
	}
	else if(appname.CompareNoCase(_T("locationpaths")) == 0) {
		locationpaths[i] = s;
	}
	else if(appname.CompareNoCase(_T("locationpaths_2.0")) == 0) {
		locationpaths_2[i] = s;
	}
	else if(appname.CompareNoCase(_T("locationpaths_3.0")) == 0) {
		locationpaths_3[i] = s;
	}

}

BOOL  CIniRecord::CheckLabels(CString s) {
	BOOL ret = FALSE;
	for (const auto &myPair : labels) {
		if (s.CompareNoCase(myPair.second) == 0) {
			logIt(_T("Label duplicate %s"), s);
			return TRUE;
		}
	}
	return ret;
}

BOOL  CIniRecord::CheckLabels_2(CString s) {
	BOOL ret = FALSE;
	for (const auto &myPair : labels_2) {
		if (s.CompareNoCase(myPair.second) == 0) {
			logIt(_T("Label2 duplicate %s"), s);
			return TRUE;
		}
	}
	return ret;
}

BOOL  CIniRecord::CheckLabels_3(CString s) {
	BOOL ret = FALSE;
	for (const auto &myPair : labels_3) {
		if (s.CompareNoCase(myPair.second) == 0) {
			logIt(_T("Label 3 duplicate %s"), s);
			return TRUE;
		}
	}
	return ret;
}

BOOL  CIniRecord::CheckLocationpaths(CString s) {
	BOOL ret = FALSE;
	for (const auto &myPair : locationpaths) {
		if (s.CompareNoCase(myPair.second) == 0) {
			logIt(_T("Locationpaths duplicate %s"), s);
			return TRUE;
		}
	}
	return ret;
}

BOOL  CIniRecord::CheckLocationpaths_2(CString s) {
	BOOL ret = FALSE;
	for (const auto &myPair : locationpaths_2) {
		if (s.CompareNoCase(myPair.second) == 0) {
			logIt(_T("Locationpaths 2 duplicate %s"), s);
			return TRUE;
		}
	}
	return ret;
}

BOOL  CIniRecord::CheckLocationpaths_3(CString s) {
	BOOL ret = FALSE;
	for (const auto &myPair : locationpaths_3) {
		if (s.CompareNoCase(myPair.second) == 0) {
			logIt(_T("Locationpaths 3 duplicate %s"), s);
			return TRUE;
		}
	}
	return ret;
}

