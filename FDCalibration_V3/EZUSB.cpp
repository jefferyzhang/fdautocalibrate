#include "stdafx.h"
#include "EZUSB.h"


EZUSB::EZUSB()
{
	insertWatcher = nullptr;
	removeWatcher = nullptr;
}

/// <summary>
/// 添加USB事件监视器
/// </summary>
/// <param name="usbInsertHandler">USB插入事件处理器</param>
/// <param name="usbRemoveHandler">USB拔出事件处理器</param>
/// <param name="withinInterval">发送通知允许的滞后时间</param>
BOOL EZUSB::AddUSBEventWatcher(EventArrivedEventHandler^ usbInsertHandler, EventArrivedEventHandler^ usbRemoveHandler, TimeSpan withinInterval)
{
	try
	{
		ManagementScope^ Scope = gcnew ManagementScope("root\\CIMV2");
		Scope->Options->EnablePrivileges = true;
		// USB插入监视
		if (usbInsertHandler != nullptr)
		{
			WqlEventQuery^ InsertQuery = gcnew WqlEventQuery("__InstanceCreationEvent",
				withinInterval,
				"TargetInstance isa 'Win32_USBControllerDevice'"); //
			insertWatcher = gcnew ManagementEventWatcher(Scope, InsertQuery);
			insertWatcher->EventArrived += usbInsertHandler;
			insertWatcher->Start();
		}
		// USB拔出监视
		if (usbRemoveHandler != nullptr)
		{
			WqlEventQuery^ RemoveQuery = gcnew WqlEventQuery("__InstanceDeletionEvent",
				withinInterval,
				"TargetInstance isa 'Win32_USBControllerDevice'");
			removeWatcher = gcnew ManagementEventWatcher(Scope, RemoveQuery);
			removeWatcher->EventArrived += usbRemoveHandler;
			removeWatcher->Start();
		}
		return true;
	}
	catch (...)
	{
		RemoveUSBEventWatcher();
		return false;
	}
}
/// <summary>
/// 移去USB事件监视器
/// </summary>
void EZUSB::RemoveUSBEventWatcher()
{
	if (insertWatcher != nullptr)
	{
		insertWatcher->Stop();
		insertWatcher = nullptr;
	}
	if (removeWatcher != nullptr)
	{
		removeWatcher->Stop();
		removeWatcher = nullptr;
	}
}
/// <summary>
/// 定位发生插拔的USB设备
/// </summary>
/// <param name="e">USB插拔事件参数</param>
/// <returns>发生插拔现象的USB控制设备ID</returns>
List<USBControllerDevice^>^ EZUSB::WhoUSBControllerDevice(EventArrivedEventArgs^ e)
{
	ManagementBaseObject^ mbo = (ManagementBaseObject^)(e->NewEvent["TargetInstance"]);
	if (mbo != nullptr && mbo->ClassPath->ClassName == "Win32_USBControllerDevice")
	{
		String^ splitchar = "=";
		String^ Antecedent = (mbo["Antecedent"]->ToString())->Replace("\"", String::Empty)->Split(splitchar->ToCharArray())[1];
		String^ Dependent = (mbo["Dependent"]->ToString())->Replace("\"", String::Empty)->Split(splitchar->ToCharArray())[1];
		List<USBControllerDevice^>^ listusbdevs = gcnew List<USBControllerDevice^>(1);
		listusbdevs->Add(gcnew USBControllerDevice(Antecedent, Dependent));
		return listusbdevs;
	}
	return nullptr;
}
