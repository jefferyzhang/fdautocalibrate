#pragma once
using namespace System;
using namespace System::Diagnostics;
using namespace System::Threading;
using namespace System::Text;
using namespace System::Collections::Generic;

ref class CRunExe
{
public:
	CRunExe();
	int startRunExe(String^ exepath, String^ sparam, int timeout, Dictionary<String^, String^>^ env);
	List<String^>^ getExeStdOut();
	List<String^>^ getExeStdError();
private:
	AutoResetEvent^ stdoutEvt;
	AutoResetEvent^ stderrEvt;
	List<String^>^ stdoutSB;
	List<String^>^ stderrSB;
	void StdoutReceived_Handler(Object^ sender, DataReceivedEventArgs^ e);
	void StderrReceived_Handler(Object^ sender, DataReceivedEventArgs^ e);
};