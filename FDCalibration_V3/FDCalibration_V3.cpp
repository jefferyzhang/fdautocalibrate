// FDCalibration_V3.cpp : main project file.

#include "stdafx.h"
#include "cheader.h"
#include <set>
#include <tlhelp32.h>
#include <rapidjson/document.h>
#include <rapidjson/writer.h>
#include <rapidjson/stringbuffer.h>
#include <iostream>
#include "IniRecord.h"
#include "ConfigInfo.h"
using namespace rapidjson;


using namespace System;
using namespace System::Collections::Specialized;
using namespace System::Configuration::Install;
using namespace System::IO;
using namespace System::Diagnostics;
using namespace System::Management;
using namespace System::Text::RegularExpressions;
#include "EZUSB.h"
#include "RunExe.h"

void logIt(System::String^ msg)
{
	System::String^ ss = System::String::Format(L"[CALIBRATION]{0}", msg);
	System::Diagnostics::Trace::WriteLine(ss);
	Console::WriteLine(ss);
}

void Usage()
{
	Console::WriteLine("command Line:");
	Console::WriteLine("\t -hubvid/donglevid=   [connect hub vid/dongle can use any device.]");
	Console::WriteLine("\t -hubpid/donglepid=   [connect hub pid/dongle can use any device., if hub chip change. default is now fdhub.]");
	Console::WriteLine("\t -inifile=   (full path ini file name. Must)");
	Console::WriteLine("\t -startlabel=   [modified for label. default is 1]");
	Console::WriteLine("\t -vertical=[true/false]   [hub label Arrangement, default is true], Ignore this");
	Console::WriteLine("\t -timeout=   [wait for device plug in, s, default 60s]");
	Console::WriteLine("\t -hubcount   [get hub count, mean 10 ports, it is only for futuredial 20 ports hub]"); 
	Console::WriteLine("\t -single   Only one futuredial support HUB connect PC. One Cable one hub");
	Console::WriteLine("\t -newconfig  for plug dongle, if no this parameter, the behavier like before.");//
	Console::WriteLine("\t -update  No check duplicate.");
	Console::WriteLine("\t -getconfig  get new version config file.");
}

HANDLE hEventWaitPlugin = CreateEvent(NULL, FALSE, FALSE, NULL);
BOOL bCheckHub = false;

void USBEventHandler(Object^ sender, EventArrivedEventArgs^ e)
{
	if (e->NewEvent->ClassPath->ClassName == "__InstanceCreationEvent")
	{
		logIt("USB plug in time:" + DateTime::Now);
	}
	else if (e->NewEvent->ClassPath->ClassName == "__InstanceDeletionEvent")
	{
		logIt("USB plug out time:" + DateTime::Now);
	}
	for each(USBControllerDevice^ Device in EZUSB::WhoUSBControllerDevice(e))
	{//Dependent��HID\\VID_3689&PID_8762\\7&17E8BD4A&0&0000
		logIt("\tAntecedent:" + Device->Antecedent);
		logIt("\tDependent:" + Device->Dependent);
		String^ sInstanceid = Device->Dependent->ToUpper()->Replace("\\\\", "\\");
		extern int DongleVID;
		extern int DonglePID;
		String^ sInst = String::Format("USB\\VID_{0:X4}&PID_{1:X4}", DongleVID, DonglePID);
		if (sInstanceid->StartsWith(sInst))
		{
			extern TCHAR sDongleInstanceID[MAX_PATH];
			using namespace Runtime::InteropServices;
			const wchar_t* lpInstanceid =
				(const wchar_t*)(Marshal::StringToHGlobalUni(sInstanceid)).ToPointer();
			ZeroMemory(sDongleInstanceID, MAX_PATH);
			_sntprintf_s(sDongleInstanceID, MAX_PATH, lpInstanceid);

			SetEvent(hEventWaitPlugin);
		}

		if (bCheckHub) {
			SetEvent(hEventWaitPlugin);
		}
		logIt(_T("%s\n"), _T("Device Plug in"));
	}
}

void CheckConfigFromDC() 
{
	//fdAccessDMCFiles.exe -command=download -sourceFile= 
	//-destFile=%apsthome%\temp\fdcalibrationv3config.xml -timeout=30 
	//-file=fdcalibrationv3config.xml -path=%apsthome%
	String^ sPath = System::IO::Path::Combine(AppDomain::CurrentDomain->BaseDirectory, "fdcalibrationv3config.xml");
	DateTime dt = File::GetLastWriteTime(sPath);
	if (File::Exists(sPath) && (DateTime::Now - dt).Days < 1) {
		return;
	}

	CRunExe^ runExe = gcnew CRunExe();
	String^ dmcexepath = Environment::ExpandEnvironmentVariables("%APSTHOME%\\fdAccessDMCFiles.exe");
	String^ sparm = String::Format("-destFile=%apsthome%\\temp\\fdcalibrationv3config.xml -command=download -sourceFile= -timeout=30 -file=fdcalibrationv3config.xml -path=%apsthome%");
	int ret = 0;
	if ((ret = runExe->startRunExe(dmcexepath, sparm, 30000, nullptr)) == 0)
	{
		if (!System::IO::File::Exists(Environment::ExpandEnvironmentVariables("%apsthome%\\temp\\fdcalibrationv3config.xml"))) 
		{
			return;
		}
		String^ fileString = System::IO::File::ReadAllText(Environment::ExpandEnvironmentVariables("%apsthome%\\temp\\fdcalibrationv3config.xml"));
		XmlDocument^ doc = gcnew XmlDocument();
		try
		{
			
			if (!File::Exists(sPath))
			{
				logIt("file config not exist.");
				File::AppendAllText(sPath, fileString);
				return;
			}
			File::SetLastWriteTime(sPath, DateTime::Now);

			doc->LoadXml(fileString);
			XmlNode^ node = doc->DocumentElement->SelectSingleNode("/hubs");
			if (node != nullptr && node->Attributes["version"] != nullptr)
			{
				XmlDocument^ docfile = gcnew XmlDocument();
				docfile->Load(sPath);
				XmlNode^ nodefile = docfile->DocumentElement->SelectSingleNode("/hubs");
				if (nodefile != nullptr && nodefile->Attributes["version"] != nullptr)
				{
					Version^ ver1 = gcnew Version(node->Attributes["version"]->Value);
					Version^ ver2 = gcnew Version(nodefile->Attributes["version"]->Value);
					if (ver1->CompareTo(ver2) > 0)
					{
						//File::AppendAllText(sPath, fileString);
						File::WriteAllText(sPath, fileString);
						return;
					}
					else
					{
						logIt("version is the same or before version.");
					}
				}
			}

		}
		catch (...)
		{
		}

	}

}

void CheckConfigFromServer()
{
	// Can get config file from Server. Check <hubs version="">
	System::Net::WebClient^ client = gcnew Net::WebClient();
	client->Credentials = gcnew Net::NetworkCredential(L"fd_eng", L"FDeng");
	String^ url = "ftp://ftp2.futuredial.com/ModusLink/CalibrationDB/fdcalibrationv3config.xml";
	try
	{
		array<byte>^ newFileData = client->DownloadData(url);
		String^ fileString = System::Text::Encoding::UTF8->GetString(newFileData);

		XmlDocument^ doc = gcnew XmlDocument();
		try
		{
			String^ sPath = System::IO::Path::Combine(AppDomain::CurrentDomain->BaseDirectory, "fdcalibrationv3config.xml");
			if (!File::Exists(sPath))
			{
				logIt("file config not exist.");
				File::AppendAllText(sPath, fileString);
				return;
			}

			doc->LoadXml(fileString);
			XmlNode^ node = doc->DocumentElement->SelectSingleNode("/hubs");
			if (node != nullptr && node->Attributes["version"] != nullptr)
			{
				XmlDocument^ docfile = gcnew XmlDocument();
				docfile->Load(sPath);
				XmlNode^ nodefile = docfile->DocumentElement->SelectSingleNode("/hubs");
				if (nodefile != nullptr && nodefile->Attributes["version"] != nullptr)
				{
					Version^ ver1 = gcnew Version(node->Attributes["version"]->Value);
					Version^ ver2 = gcnew Version(nodefile->Attributes["version"]->Value);
					if (ver1->CompareTo(ver2) > 0)
					{
						//File::AppendAllText(sPath, fileString);
						File::WriteAllText(sPath, fileString);
						return;
					}
					else
					{
						logIt("version is the same or before version.");
					}
				}
			}
			
		}
		catch (...)
		{
		}

	}
	catch (Exception^ e)
	{
	}

}

// Function to get the parent process ID
DWORD GetParentProcessId()
{
	HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (hSnapshot == INVALID_HANDLE_VALUE)
	{
		return -1;
	}
	DWORD processId = GetCurrentProcessId();
	PROCESSENTRY32 pe32;
	pe32.dwSize = sizeof(PROCESSENTRY32);

	if (Process32First(hSnapshot, &pe32))
	{
		do
		{
			if (pe32.th32ProcessID == processId)
			{
				DWORD parentProcessId = pe32.th32ParentProcessID;
				CloseHandle(hSnapshot);
				return parentProcessId;
			}
		} while (Process32Next(hSnapshot, &pe32));
	}

	CloseHandle(hSnapshot);
	return -1;
}

HANDLE hEventExit = nullptr;
// Function to monitor the parent process and exit if it terminates
void  MonitorParentProcess()
{
	DWORD parentProcessId = GetParentProcessId();
	if (parentProcessId > 0) {
		HANDLE hParentProcess = OpenProcess(SYNCHRONIZE, FALSE, parentProcessId);
		if (hParentProcess == NULL)
		{
			return;
		}
#define EVNT_CNT	2
		HANDLE waithandles[EVNT_CNT] = { hParentProcess , hEventExit };
		// Wait for the parent process to exit
		DWORD waitResult = WaitForMultipleObjects(EVNT_CNT, waithandles, FALSE, INFINITE);
		if (waitResult == WAIT_OBJECT_0)
		{
			logIt("parent exit");
			CloseHandle(hParentProcess);
			ExitProcess(0);  // Exit the current process
		}
		else if (waitResult == WAIT_OBJECT_0 + 1) {
			logIt("self exit");
			CloseHandle(hParentProcess);
		}
	}
}


int main(array<System::String ^> ^args)
{
	extern int FDHUBVID;
	extern int FDHUBPID;
	extern TCHAR sIniFileName[MAX_PATH];

	_sntprintf_s(sIniFileName, MAX_PATH, _T("E:\\temp\\calibration.ini"));

	logIt(String::Format("{0} start: ++ version: {1}",
		Path::GetFileName(Process::GetCurrentProcess()->MainModule->FileName),
		Process::GetCurrentProcess()->MainModule->FileVersionInfo->FileVersion));

	//_sntprintf_s(sIniFileName, 1024, _T("J:\\temp\\calibration.ini"));
	InstallContext^ myInstallContext = gcnew InstallContext(nullptr, args);
	if (myInstallContext->IsParameterTrue("debug"))
	{
		logIt("Wait any key to dubug, Please attach...\n");
		System::Console::ReadKey();
	}
	if (myInstallContext->IsParameterTrue("help"))
	{
		Usage();
		return ERROR_SUCCESS;
	}
	if (myInstallContext->Parameters->ContainsKey("getconfig"))
	{
		//CheckConfigFromServer();
		CheckConfigFromDC();
		return ERROR_SUCCESS;
	}
	if (myInstallContext->Parameters->ContainsKey("vertical"))
	{
		extern BOOL bVertical;
		bVertical = String::Compare("true", myInstallContext->Parameters["vertical"], true)==0;
	}
	if (myInstallContext->Parameters->ContainsKey("hubvid"))
	{
		FDHUBVID = Convert::ToInt32(myInstallContext->Parameters["hubvid"]);
	}
	if (myInstallContext->Parameters->ContainsKey("hubpid"))
	{
		FDHUBPID = Convert::ToInt32(myInstallContext->Parameters["hubpid"]);
	}

	//read config file for HUB List
	//[FDHUB]
	//fdHubVidPid = { "FDVID_PID":["VID_0BDA&PID_5401", "VID_0BDA&PID_5411"] }
	if (myInstallContext->Parameters->ContainsKey("newconfig"))
	{
		CalibrateConfig2SupportList();
	}
	else
	{
		String^ sApsthomePath = Environment::GetEnvironmentVariable("APSTHOME");
		if (!String::IsNullOrEmpty(sApsthomePath))
		{
			String^ iniconfig = Path::Combine(sApsthomePath, "config.ini");
			using namespace Runtime::InteropServices;
			const wchar_t* lpIniFileName =
				(const wchar_t*)(Marshal::StringToHGlobalUni(iniconfig)).ToPointer();
			TCHAR value[1024] = { 0 };
			ZeroMemory(value, sizeof(value));
			GetPrivateProfileString(_T("FDHUB"), _T("fdHubVidPid"), _T(""), value, sizeof(value), lpIniFileName);
			if (_tcslen(value) > 0)
			{
				logIt(_T("%s\n"), value);
				CString s; s.Format(_T("VID_%04X&PID_%04X"), 1234, 3214);
				typedef GenericDocument<UTF16<>> WDocument;
				typedef GenericValue<UTF16<>> WValue;
				WDocument d;
				if (!d.Parse(value).HasParseError())
				{
					if (d.HasMember(_T("FDVID_PID")))
					{
						const WValue& a = d[_T("FDVID_PID")];
						for (WValue::ConstValueIterator itr = a.Begin(); itr != a.End(); ++itr)
						{
							//wprintf(itr->GetString());
							extern std::set<CString> gsupportlisthub;
							gsupportlisthub.insert(itr->GetString());
						}
					}
				}
				else
				{
					logIt(_T("%s\n"),_T("Jason Format Error."));
				}
			}
		}
	}
	if (myInstallContext->Parameters->ContainsKey("donglevid"))
	{
		extern int DongleVID;
		DongleVID = Convert::ToInt32(myInstallContext->Parameters["donglevid"]);
	}
	if (myInstallContext->Parameters->ContainsKey("donglepid"))
	{
		extern int DonglePID;
		DonglePID = Convert::ToInt32(myInstallContext->Parameters["donglepid"]);
	}
	if (myInstallContext->Parameters->ContainsKey("timeout"))
	{
		extern int g_timeout;
		g_timeout = Convert::ToInt32(myInstallContext->Parameters["timeout"]);
	}
	if (myInstallContext->IsParameterTrue("hubcount"))
	{
		int ret = GetFDFastHubCount();
		Console::WriteLine("hubcount=" + ret);
		return ret;
	}
	extern BOOL g_bUpdate;
	g_bUpdate = myInstallContext->IsParameterTrue("update");

	TCHAR path[MAX_PATH] = { 0 };
	GetModuleFileName(NULL, path, MAX_PATH);
	PathRemoveFileSpec(path);
	_tcscpy_s(sIniFileName, path);
	PathAppend(sIniFileName, _T("calibration_new.ini"));

	if (myInstallContext->Parameters->ContainsKey("inifile"))
	{
		String^ sFN = myInstallContext->Parameters["inifile"];
		using namespace Runtime::InteropServices;
		const wchar_t* lpIniFileName =
			(const wchar_t*)(Marshal::StringToHGlobalUni(sFN)).ToPointer();
		ZeroMemory(sIniFileName, MAX_PATH);
		_sntprintf_s(sIniFileName, MAX_PATH, lpIniFileName);
	}
	
	extern int gStartLabel;
	if (myInstallContext->Parameters->ContainsKey("startlabel"))
	{
		gStartLabel = Convert::ToInt32(myInstallContext->Parameters["startlabel"]);
	}

	if (myInstallContext->Parameters->ContainsKey("single"))
	{
		DWORD ret = CalibrateStaticHub();
		FreeAllUsbHubList();
		return ret;
	}
	
	extern CIniRecord inirecords;
	inirecords.InitIniFile(sIniFileName, gStartLabel);

	hEventExit = CreateEvent(NULL, TRUE, FALSE, NULL);
	System::Threading::Thread^ ml = gcnew System::Threading::Thread(gcnew System::Threading::ThreadStart(&MonitorParentProcess));
	ml->Start();

	extern int g_timeout;
	EZUSB^ ezusb = gcnew EZUSB();
	ezusb->AddUSBEventWatcher(gcnew EventArrivedEventHandler(USBEventHandler), nullptr, TimeSpan(0, 0, 1));
	extern HANDLE hEventHubPlugin;
	init_win_message();
	Sleep(100);
	Console::WriteLine("[info] wait for dongle plug in\n");
	HANDLE hEvents[2] = { hEventWaitPlugin, hEventHubPlugin };
	//DWORD dwRet = WaitForSingleObject(hEventWaitPlugin, g_timeout * 1000);
	//DWORD dwRet = ERROR_SUCCESS;
	///*
	DWORD dwRet = WaitForMultipleObjects(sizeof(hEvents) / sizeof(HANDLE), hEvents, FALSE, g_timeout * 1000);
	Console::WriteLine("[info] event arrived or timeout\n");
	if (dwRet == WAIT_TIMEOUT)
	{
		logIt(_T("%s\n"),_T("[timeout] can not detect dongle plug in"));
		return ERROR_TIMEOUT;
	}
	else if (dwRet == WAIT_OBJECT_0)
	{
		//
		if (myInstallContext->Parameters->ContainsKey("newconfig"))
		{
			dwRet = GetHubInfoV2();
		}
		else
		{
			dwRet = GetHubInfo(FDHUBVID, FDHUBPID);
		}
	}
	else if (dwRet == WAIT_OBJECT_0+1)
	{
		bCheckHub = true;
		//Hub plug in
		while (WaitForSingleObject(hEventWaitPlugin, 5000) == WAIT_OBJECT_0) {
		//while (WaitForMultipleObjects(sizeof(hEvents) / sizeof(HANDLE), hEvents, FALSE, 5000)!= ERROR_TIMEOUT){
			ResetEvent(hEventWaitPlugin);
			bCheckHub = true;
			Sleep(50);
			logIt(_T("%s\n"), _T("detect Device plug in, waiting..."));
		}
		//Sleep(2000);
		uninit_win_message();
		dwRet = CalibrateHubPlugIn();
		FreeAllUsbHubList();
		SetEvent(hEventExit);
		EXIT_FUNCTRET(dwRet);
		CloseHandle(hEventExit);

		return dwRet;
	}
	else
	{
		dwRet = GetLastError();
	}
	//*/
	uninit_win_message();
	SetEvent(hEventExit);
	EXIT_FUNCTRET(dwRet);
	CloseHandle(hEventExit);
	return dwRet;
}

