#pragma once
#include <map>
class CIniRecord
{
public:
	CIniRecord();
	std::map<int, CString>  labels;
	std::map<int, CString>  labels_2;
	std::map<int, CString>  labels_3;
	std::map<int, CString>  locationpaths;
	std::map<int, CString>  locationpaths_2;
	std::map<int, CString>  locationpaths_3;
	int amount;

	int InitIniFile(CString sIni, int nstart);

	BOOL  CheckLabels(CString s);
	BOOL  CheckLabels_2(CString s);
	BOOL  CheckLabels_3(CString s);
	BOOL  CheckLocationpaths(CString s);
	BOOL  CheckLocationpaths_2(CString s);
	BOOL  CheckLocationpaths_3(CString s);

	void  WriteToMap(CString appname, int i, CString s);
};

