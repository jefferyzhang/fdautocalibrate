#pragma once
using namespace System;
using namespace System::Management;
using namespace System::Collections::Generic;
using namespace System::Xml;
using namespace System::Text;

ref class CHub
{
public:
	String^ pid;
	String^ vid;
	String^ usb30pidvid;
	int portcnt;
	String^ sComment;
	String^ locationpath;
	int autocnt = 0;
	Dictionary<int, Object^>^ hubports = gcnew Dictionary<int, Object^>();
	Dictionary<int, int>^ hubportsLabel1 = gcnew Dictionary<int, int>();
	void SetLocationPath(String^ s)
	{
		if (!String::IsNullOrEmpty(s))
		{
			locationpath = s;
			for each(auto ss in hubports)
			{
				if (ss.Value->GetType() == CHub::typeid)
				{
					CHub^ temp = (CHub^)ss.Value;
					temp->SetLocationPath(String::Format("{0}#USB({1})", s, ss.Key));
				}
			}
		}
	}
	BOOL IsSupportUsb30()
	{
		return !String::IsNullOrWhiteSpace(usb30pidvid);
	}	
	BOOL IsCurrentUSB30(String^ HubName)
	{
		if (!IsSupportUsb30()) return FALSE;		
		return HubName->IndexOf(usb30pidvid, StringComparison::OrdinalIgnoreCase) > 0;
	}
	
	String^ ToString() override
	{
		StringBuilder^ sb = gcnew StringBuilder();
		int nLevel = 0;
		AddSB(sb, nLevel);
		return sb->ToString();
	}
private:
	void AddSB(StringBuilder^ sb, int &nLevel)
	{
		String^ sTab = gcnew String(' ', nLevel);
		sb->AppendFormat("{7}VID={0},PID={1},USB30VIDPID={3},PortCount={4},Comment={5}, LocationPath={6}\n", vid, pid, usb30pidvid, portcnt, sComment, locationpath, sTab);
		for each(auto hport in hubports)
		{
			if (hport.Value->GetType() == CHub::typeid)
			{
				CHub^ temp = (CHub^)hport.Value;
				AddSB(sb, ++nLevel);
				nLevel--;
			}
			else
			{
				sb->AppendFormat("{0}port={1} ==> label={2}\n", sTab, hport.Key, Convert::ToInt16(hport.Value));
			}
		}

	}
};

ref class CConfigInfo
{
public:
	CConfigInfo();
	List<CHub^>^ hubs = gcnew List<CHub^>();
public:
	DWORD LoadConfigXml();
private:
	void ParseHub(CHub^ hub, XmlNode^ node);
};

