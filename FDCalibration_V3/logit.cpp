#include "stdafx.h"
#include "cheader.h"
#include <atlstr.h>

#include <iostream>
#include <fstream>
#include <iostream>
#include <ctime>

using std::endl;

std::wofstream logfile;
bool IsLogFile = true;
void OpenLogFile() {
	if (IsLogFile && !logfile.is_open()) {
		char value[MAX_PATH] = { 0 };
		char fileName[MAX_PATH + 1] = { 0 };
		DWORD len = GetEnvironmentVariableA("APSTHOME", value, MAX_PATH);
		if (len > 0)
			value[len] = 0;



		time_t rawtime;
		struct tm  timeinfo;
		char buffer[100] = { 0 };

		time(&rawtime);
		localtime_s(&timeinfo, &rawtime);
		strftime(buffer, sizeof(buffer), "logs\\Calibration%Y%m%d%H%M%S.log", &timeinfo);

		std::string s(value);
		s.append(buffer);

		logfile.open(s, std::ios_base::app | std::ios_base::in);
	}
}

void logIt(TCHAR* fmt, ...)
{
	OpenLogFile();
	va_list args;

	CString sLog;
	va_start(args, fmt);
	sLog.FormatV(fmt, args);
	va_end(args);
	CString sLogpid;
	sLogpid.Format(_T("[CALIBRATION]%s"), sLog);
	_tprintf(sLog);
	fflush(stdout);
	if (logfile.is_open()) {
		logfile << sLog.GetString() << std::endl;
	}
	OutputDebugString(sLogpid);
}
