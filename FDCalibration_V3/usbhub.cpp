#include "stdafx.h"
#include "cheader.h"
#include <SetupAPI.h>
#include <devpkey.h>
#include <map>
#include <set>
#include <list>
#include "IniRecord.h"


#pragma comment(lib,"Setupapi.lib")

#include "ConfigInfo.h"
#using <system.dll>

#define HUBINDEXCONNECTDONGLEPORT		1

using namespace System;
using namespace System::Text::RegularExpressions;
using namespace System::Text;
using namespace Runtime::InteropServices;


int DongleVID = 0x3689; 
int DonglePID = 0x8762;

int FDHUBVID = 0xBDA;
int FDHUBPID = 0x5401;


TCHAR sIniFileName[MAX_PATH] = { 0 };
TCHAR sDongleInstanceID[MAX_PATH] = { 0 };
int  gStartLabel = 1;
BOOL bVertical = true;
int g_timeout = 60;
BOOL g_bUpdate = false;

#define CHILDCONPORT1		2	//connect hub port
#define CHILDCONPORT2		3
#define HUBPORTCOUNT		4

std::set<CString> gsupportlisthub;

extern std::map<CString, CString> ginstallHub; //hub plug in get hub symblink and location path

typedef struct __tagHubInfo{
	CString sHubName;
	int portcount;
	CString sLocationpath;
	BOOL isUsed;
	PUSB_NODE_CONNECTION_INFORMATION_EX    pConnectionInfoEx;
	PUSB_PORT_CONNECTOR_PROPERTIES         pPortConnectorProps;
	PUSB_HUB_CAPABILITIES_EX			   pHubCapabilityEx;
	PUSB_HUB_INFORMATION_EX				   pHubInfoEx = NULL;

	USB_HUB_TYPE             HubType;

	__tagHubInfo()
	{
		portcount = 0;
		isUsed = false;
		pConnectionInfoEx = NULL;
		pPortConnectorProps = NULL;
		pHubCapabilityEx = NULL;
		pHubInfoEx = NULL;
		HubType = (USB_HUB_TYPE)0;
	}
	~__tagHubInfo()
	{
		if (pConnectionInfoEx != NULL)
		{
			free(pConnectionInfoEx);
		}
		if (pPortConnectorProps != NULL)
		{
			free(pPortConnectorProps);
		}
		if (pHubCapabilityEx != NULL)
		{
			free(pHubCapabilityEx);
		}
	}
	BOOL IsRootHub()
	{
		BOOL bRet = pHubCapabilityEx == NULL ? FALSE : pHubCapabilityEx->CapabilityFlags.HubIsRoot;
		return bRet;
	}

	USB_HUB_TYPE GetHubType()
	{
		if (pHubInfoEx != NULL)
			return pHubInfoEx->HubType;
		else return UsbRootHub;
	}
}HUBINFO, *PHUBINFO;

std::map<CString, PHUBINFO> allhubinfo; //HubName ==> HubInfo

void PrintError(DWORD dwError, __in PCTSTR sFile, ULONG   Line)
{
	LPVOID lpMsgBuf;
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM,
		NULL,
		dwError,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&lpMsgBuf,
		0, NULL);
	logIt(_T("Error: %s=>File:%s=>Line:%d=>ErrorCode:%d\n"), lpMsgBuf, sFile, Line, dwError);
	LocalFree(lpMsgBuf);

}

BOOL CheckDuplicateV2(std::list<CString> locpaths);
void GetHubCalibration(std::map<CString, PHUBINFO> lochubinfo, CHub^ pHub, int startindex, int &Count);
DWORD GetPeerHub(CString locPath, CString &sPeerLocationPath, std::map<CString, PHUBINFO> alist);

int GetDeviceCommonSymbl(TCHAR *sName, PDEVPROPKEY pdevkey, PBYTE pdata, DWORD &nLen, DWORD &nType)
{
	int ret = ERROR_UNKNOWN_FEATURE;
	if (sName == NULL || _tcslen(sName) == 0)
		return ERROR_INVALID_PARAMETER;

	TCHAR symblName[1024] = { 0 };

	if ((_tcsncicmp(sName, _T("\\\\?\\"), 4) == 0) || (_tcsncicmp(sName, _T("\\\\.\\"), 4) == 0))
	{
		_stprintf_s(symblName, _T("%ws"), sName);
	}
	else
	{
		_stprintf_s(symblName, _T("\\\\?\\%ws"),sName);
	}

	HDEVINFO hDevInfo = SetupDiCreateDeviceInfoList(NULL, NULL);
	if (hDevInfo != INVALID_HANDLE_VALUE)
	{
		DWORD sz = 0;
		SP_DEVICE_INTERFACE_DATA devIntData;
		devIntData.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);
		if (SetupDiOpenDeviceInterface(hDevInfo, symblName, 0, &devIntData))
		{
			BYTE b[4096];
			ZeroMemory(b, sizeof(b));
			sz = 0;

			SP_DEVINFO_DATA devInfoData = { 0 };
			devInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
			SetupDiGetDeviceInterfaceDetail(hDevInfo, &devIntData, NULL, 0, &sz, &devInfoData);
			if (GetLastError() == ERROR_INSUFFICIENT_BUFFER)
			{
				if (SetupDiGetDeviceProperty(hDevInfo, &devInfoData, pdevkey, &nType, b, 2048, &sz, 0))
				{
					if (sz <= nLen){
						memcpy_s(pdata, nLen, b, sz);
						ret = ERROR_SUCCESS;
					}
					else
						ret = ERROR_INSUFFICIENT_BUFFER;
					nLen = sz;
				}
				else
				{
					ret = GetLastError();
					PrintError(ret, _T(__FILE__), __LINE__);
				}
			}
			else
			{
				ret = GetLastError();
				PrintError(ret, _T(__FILE__), __LINE__);
			}
		}
		else
		{
			ret = GetLastError();
			PrintError(ret, _T(__FILE__), __LINE__);
		}

		SetupDiDestroyDeviceInfoList(hDevInfo);
	}
	else
	{
		ret = GetLastError();
		PrintError(ret, _T(__FILE__), __LINE__);
	}

	return ret;
}

DWORD Get_USB_HUB_INFORMATION_EX(HANDLE hHubDevice, __out PUSB_HUB_INFORMATION_EX* hubInfoEx)
{
	ENTER_FUNCTION();
	DWORD dwRet(ERROR_SUCCESS);
	BOOL        success = FALSE;
	if (hHubDevice == INVALID_HANDLE_VALUE)
	{
		dwRet = ERROR_INVALID_PARAMETER;
		return dwRet;
	}
	ULONG nBytes = 0;

	*hubInfoEx = (PUSB_HUB_INFORMATION_EX)malloc(sizeof(USB_HUB_INFORMATION_EX));
	if (*hubInfoEx == NULL)
	{
		dwRet = ERROR_NOT_ENOUGH_MEMORY;
		PrintError(dwRet, _T(__FILE__), __LINE__);
		return dwRet;
	}
	//
	// Obtain Hub Capabilities
	//
	success = DeviceIoControl(hHubDevice,
		IOCTL_USB_GET_HUB_INFORMATION_EX,
		*hubInfoEx,
		sizeof(USB_HUB_INFORMATION_EX),
		*hubInfoEx,
		sizeof(USB_HUB_INFORMATION_EX),
		&nBytes,
		NULL);

	//
	// Fail gracefully
	//
	if (!success || nBytes < sizeof(USB_HUB_INFORMATION_EX))
	{
		*hubInfoEx = NULL;
		dwRet = GetLastError();
		PrintError(dwRet, _T(__FILE__), __LINE__);
	}

	return dwRet;
}

DWORD Get_USB_HUB_CAPABILITIES_EX(HANDLE hHubDevice, __out PUSB_HUB_CAPABILITIES_EX *hubCapabilityEx)
{
	ENTER_FUNCTION();
	DWORD dwRet(ERROR_SUCCESS);
	BOOL        success = FALSE;
	if (hHubDevice == INVALID_HANDLE_VALUE)
	{
		dwRet = ERROR_INVALID_PARAMETER;
		return dwRet;
	}
	ULONG nBytes = 0;

	*hubCapabilityEx = (PUSB_HUB_CAPABILITIES_EX)malloc(sizeof(USB_HUB_CAPABILITIES_EX));
	if (*hubCapabilityEx == NULL)
	{
		dwRet = ERROR_NOT_ENOUGH_MEMORY;
		PrintError(dwRet, _T(__FILE__), __LINE__);
		return dwRet;
	}
	//
	// Obtain Hub Capabilities
	//
	success = DeviceIoControl(hHubDevice,
		IOCTL_USB_GET_HUB_CAPABILITIES_EX,
		*hubCapabilityEx,
		sizeof(USB_HUB_CAPABILITIES_EX),
		*hubCapabilityEx,
		sizeof(USB_HUB_CAPABILITIES_EX),
		&nBytes,
		NULL);

	//
	// Fail gracefully
	//
	if (!success || nBytes < sizeof(USB_HUB_CAPABILITIES_EX))
	{
		*hubCapabilityEx = NULL;
		dwRet = GetLastError();
		PrintError(dwRet, _T(__FILE__), __LINE__);
	}

	return dwRet;
}

DWORD Get_USB_NODE_CONNECTION_INFORMATION_EX(HANDLE hHubDevice, int hubport, __out PUSB_NODE_CONNECTION_INFORMATION_EX *connectionInfoEx)
{
	ENTER_FUNCTION();
	DWORD dwRet(ERROR_SUCCESS);
	BOOL        success = FALSE;
	if (hHubDevice == INVALID_HANDLE_VALUE || hubport < 1)
	{
		dwRet = ERROR_INVALID_PARAMETER;
		return dwRet;
	}
	ULONG nBytesEx;
	PUSB_NODE_CONNECTION_INFORMATION_EX_V2 connectionInfoExV2 = NULL;
	//
	// Allocate space to hold the connection info for this port.
	// For now, allocate it big enough to hold info for 30 pipes.
	//
	// Endpoint numbers are 0-15.  Endpoint number 0 is the standard
	// control endpoint which is not explicitly listed in the Configuration
	// Descriptor.  There can be an IN endpoint and an OUT endpoint at
	// endpoint numbers 1-15 so there can be a maximum of 30 endpoints
	// per device configuration.
	//
	// Should probably size this dynamically at some point.
	//

	nBytesEx = sizeof(USB_NODE_CONNECTION_INFORMATION_EX) +
		(sizeof(USB_PIPE_INFO) * 30);

	*connectionInfoEx = (PUSB_NODE_CONNECTION_INFORMATION_EX)malloc(nBytesEx);

	if (*connectionInfoEx == NULL)
	{
		dwRet = ERROR_NOT_ENOUGH_MEMORY;
		PrintError(dwRet, _T(__FILE__), __LINE__);
		return dwRet;
	}

	connectionInfoExV2 = (PUSB_NODE_CONNECTION_INFORMATION_EX_V2)
		malloc(sizeof(USB_NODE_CONNECTION_INFORMATION_EX_V2));

	if (connectionInfoExV2 == NULL)
	{
		free(*connectionInfoEx);
		dwRet = ERROR_NOT_ENOUGH_MEMORY;
		PrintError(dwRet, _T(__FILE__), __LINE__);
		return dwRet;
	}
	ULONG nBytes = 0;
	connectionInfoExV2->ConnectionIndex = hubport;
	connectionInfoExV2->Length = sizeof(USB_NODE_CONNECTION_INFORMATION_EX_V2);
	connectionInfoExV2->SupportedUsbProtocols.Usb300 = 1;

	success = DeviceIoControl(hHubDevice,
		IOCTL_USB_GET_NODE_CONNECTION_INFORMATION_EX_V2,
		connectionInfoExV2,
		sizeof(USB_NODE_CONNECTION_INFORMATION_EX_V2),
		connectionInfoExV2,
		sizeof(USB_NODE_CONNECTION_INFORMATION_EX_V2),
		&nBytes,
		NULL);

	if (!success || nBytes < sizeof(USB_NODE_CONNECTION_INFORMATION_EX_V2))
	{
		free(connectionInfoExV2);
		connectionInfoExV2 = NULL;
	}

	(*connectionInfoEx)->ConnectionIndex = hubport;

	success = DeviceIoControl(hHubDevice,
		IOCTL_USB_GET_NODE_CONNECTION_INFORMATION_EX,
		*connectionInfoEx,
		nBytesEx,
		*connectionInfoEx,
		nBytesEx,
		&nBytesEx,
		NULL);

	if (success)
	{
		//
		// Since the USB_NODE_CONNECTION_INFORMATION_EX is used to display
		// the device speed, but the hub driver doesn't support indication
		// of superspeed, we overwrite the value if the super speed
		// data structures are available and indicate the device is operating
		// at SuperSpeed.
		// 

		if ((*connectionInfoEx)->Speed == UsbHighSpeed
			&& connectionInfoExV2 != NULL
			&& connectionInfoExV2->Flags.DeviceIsOperatingAtSuperSpeedOrHigher)
		{
			(*connectionInfoEx)->Speed = UsbSuperSpeed;
		}
	}
	else
	{
		PUSB_NODE_CONNECTION_INFORMATION    connectionInfo = NULL;

		// Try using IOCTL_USB_GET_NODE_CONNECTION_INFORMATION
		// instead of IOCTL_USB_GET_NODE_CONNECTION_INFORMATION_EX
		//

		nBytes = sizeof(USB_NODE_CONNECTION_INFORMATION) +
			sizeof(USB_PIPE_INFO) * 30;

		connectionInfo = (PUSB_NODE_CONNECTION_INFORMATION)malloc(nBytes);

		if (connectionInfo == NULL)
		{

			free(*connectionInfoEx);
			if (connectionInfoExV2 != NULL)
			{
				free(connectionInfoExV2);
			}
			dwRet = ERROR_NOT_ENOUGH_MEMORY;
			PrintError(dwRet, _T(__FILE__), __LINE__);
			return dwRet;
		}

		connectionInfo->ConnectionIndex = hubport;

		success = DeviceIoControl(hHubDevice,
			IOCTL_USB_GET_NODE_CONNECTION_INFORMATION,
			connectionInfo,
			nBytes,
			connectionInfo,
			nBytes,
			&nBytes,
			NULL);

		if (!success)
		{
			dwRet = GetLastError();
			free(connectionInfo);
			free(*connectionInfoEx);

			if (connectionInfoExV2 != NULL)
			{
				free(connectionInfoExV2);
			}
			PrintError(dwRet, _T(__FILE__), __LINE__);
			return dwRet;
		}

		// Copy IOCTL_USB_GET_NODE_CONNECTION_INFORMATION into
		// IOCTL_USB_GET_NODE_CONNECTION_INFORMATION_EX structure.
		//
		(*connectionInfoEx)->ConnectionIndex = connectionInfo->ConnectionIndex;
		(*connectionInfoEx)->DeviceDescriptor = connectionInfo->DeviceDescriptor;
		(*connectionInfoEx)->CurrentConfigurationValue = connectionInfo->CurrentConfigurationValue;
		(*connectionInfoEx)->Speed = connectionInfo->LowSpeed ? UsbLowSpeed : UsbFullSpeed;
		(*connectionInfoEx)->DeviceIsHub = connectionInfo->DeviceIsHub;
		(*connectionInfoEx)->DeviceAddress = connectionInfo->DeviceAddress;
		(*connectionInfoEx)->NumberOfOpenPipes = connectionInfo->NumberOfOpenPipes;
		(*connectionInfoEx)->ConnectionStatus = connectionInfo->ConnectionStatus;

		memcpy(&(*connectionInfoEx)->PipeList[0],
			&connectionInfo->PipeList[0],
			sizeof(USB_PIPE_INFO) * 30);

		free(connectionInfo);
	}


	return dwRet;
}

DWORD Get_USB_PORT_CONNECTOR_PROPERTIES(HANDLE hHubDevice, int hubport, __out PUSB_PORT_CONNECTOR_PROPERTIES *ppPortConnectorProps)
{
	ENTER_FUNCTION();
	DWORD dwRet(ERROR_SUCCESS);
	BOOL        success = FALSE;
	USB_PORT_CONNECTOR_PROPERTIES          portConnectorProps;
	ZeroMemory(&portConnectorProps, sizeof(portConnectorProps));
	ULONG nBytes = 0;

	if (hHubDevice == INVALID_HANDLE_VALUE || hubport < 1)
	{
		dwRet = ERROR_INVALID_PARAMETER;
		return dwRet;
	}

	//
	// Now query USBHUB for the structures
	// for this port.  This will tell us if a device is attached to this
	// port, among other things.
	// The fault tolerate code is executed first.
	//

	portConnectorProps.ConnectionIndex = hubport;
	success = DeviceIoControl(hHubDevice,
		IOCTL_USB_GET_PORT_CONNECTOR_PROPERTIES,
		&portConnectorProps,
		sizeof(USB_PORT_CONNECTOR_PROPERTIES),
		&portConnectorProps,
		sizeof(USB_PORT_CONNECTOR_PROPERTIES),
		&nBytes,
		NULL);

	if (success && nBytes == sizeof(USB_PORT_CONNECTOR_PROPERTIES))
	{
		int nLen = portConnectorProps.ActualLength;
		PUSB_PORT_CONNECTOR_PROPERTIES pPortConnectorProps = (PUSB_PORT_CONNECTOR_PROPERTIES)malloc(nLen);
		ZeroMemory(pPortConnectorProps, nLen);
		if (pPortConnectorProps != NULL)
		{
			(pPortConnectorProps)->ConnectionIndex = hubport;
			nBytes = 0;
			success = DeviceIoControl(hHubDevice,
				IOCTL_USB_GET_PORT_CONNECTOR_PROPERTIES,
				pPortConnectorProps,
				nLen,
				pPortConnectorProps,
				nLen,
				&nBytes,
				NULL);

			if (!success || nBytes < portConnectorProps.ActualLength)
			{
				PrintError(GetLastError(), _T(__FILE__), __LINE__);
				free(pPortConnectorProps);
				pPortConnectorProps = NULL;
			}
			*ppPortConnectorProps = pPortConnectorProps;
		}
		else{
			logIt(_T("Malloc failed."));
			dwRet = ERROR_NOT_ENOUGH_MEMORY;
		}
	}
	else
	{
		dwRet = GetLastError();
		PrintError(dwRet, _T(__FILE__), __LINE__);
	}

	return dwRet;
}

void FreeAllUsbHubList()
{
	for (std::map<CString, PHUBINFO>::iterator iter = allhubinfo.begin(); iter != allhubinfo.end(); iter++) 
	{
		if (iter->second != NULL)
		{
			PHUBINFO pHI = iter->second;
			delete pHI;
		}
	}
	allhubinfo.clear();
}

DWORD ListAllUsbHub()
{
	ENTER_FUNCTION();
	HANDLE                              hHub;
	HDEVINFO                            hDeviceInfo;
	SP_DEVICE_INTERFACE_DATA            _sdid;
	PSP_DEVICE_INTERFACE_DETAIL_DATA    _psdidd;
	DWORD dwLength;
	DWORD dwBytesX;
	DWORD dwRet = ERROR_SUCCESS;

	hDeviceInfo = SetupDiGetClassDevs(&GUID_DEVINTERFACE_USB_HUB, NULL, NULL, DIGCF_PRESENT | DIGCF_DEVICEINTERFACE);
	if (hDeviceInfo != INVALID_HANDLE_VALUE)
	{
		_sdid.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);
		for (int i = 0; SetupDiEnumDeviceInterfaces(hDeviceInfo, NULL, &GUID_DEVINTERFACE_USB_HUB, i, &_sdid); i++)
		{
			SetupDiGetDeviceInterfaceDetail(hDeviceInfo, &_sdid, NULL, 0, &dwBytesX, NULL );
			dwLength = dwBytesX;
			_psdidd = (PSP_DEVICE_INTERFACE_DETAIL_DATA)GlobalAlloc(GPTR, dwLength);
			if (_psdidd == NULL)
			{
				continue;
			}

			_psdidd->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);
			if (!SetupDiGetDeviceInterfaceDetail(hDeviceInfo, &_sdid, _psdidd, dwLength, &dwBytesX, NULL))
			{
				GlobalFree(_psdidd);
				continue;
			}

			BYTE pdata[4096] = { 0 };
			DWORD nLen = sizeof(pdata);
			DWORD nType = 0;
			CString sLocPaths;
			CString sHubName = _psdidd->DevicePath;
			if (DWORD dwRet = GetDeviceCommonSymbl(_psdidd->DevicePath, (PDEVPROPKEY)&DEVPKEY_Device_LocationPaths, pdata, nLen, nType) == ERROR_SUCCESS)
			{
				if (nType == DEVPROP_TYPE_STRING_LIST)
				{
					//add locationPaths
					sLocPaths = (TCHAR *)pdata;
				}
			}
			else
			{
				PrintError(dwRet, _T(__FILE__), __LINE__);
				GlobalFree(_psdidd);
				continue;
			}

			hHub = CreateFile(_psdidd->DevicePath, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, NULL);
			GlobalFree(_psdidd);
			if (hHub == INVALID_HANDLE_VALUE)
			{
				PrintError(GetLastError(), _T(__FILE__), __LINE__);
				continue;
			}

			DWORD BytesReturned;
			int hubportcnt = 0;
			USB_NODE_INFORMATION nodeInformation;
			ZeroMemory(&nodeInformation, sizeof(USB_NODE_INFORMATION));
			if (DeviceIoControl(hHub, IOCTL_USB_GET_NODE_INFORMATION, &nodeInformation, sizeof(USB_NODE_INFORMATION), &nodeInformation, sizeof(USB_NODE_INFORMATION), &BytesReturned, NULL))
			{
				hubportcnt = nodeInformation.u.HubInformation.HubDescriptor.bNumberOfPorts;
			}
			else
			{
				PrintError(GetLastError(), _T(__FILE__), __LINE__);
				CloseHandle(hHub);
				continue;
			}
			PUSB_HUB_CAPABILITIES_EX hubCapabilityEx = NULL;
			Get_USB_HUB_CAPABILITIES_EX(hHub, &hubCapabilityEx);

			PUSB_HUB_INFORMATION_EX hubInfoEx = NULL;
			Get_USB_HUB_INFORMATION_EX(hHub, &hubInfoEx);

			CloseHandle(hHub);

			PHUBINFO pinfo = new HUBINFO();
			if (pinfo != NULL)
			{
				pinfo->sHubName = sHubName;
				pinfo->sLocationpath = sLocPaths;
				pinfo->portcount = hubportcnt;
				pinfo->pHubCapabilityEx = hubCapabilityEx;
				pinfo->pHubInfoEx = hubInfoEx;
				if (hubInfoEx != NULL) {
					pinfo->HubType = hubInfoEx->HubType;
				}
				logIt(_T("find:%s==locatiopath:%s==portcount:%d\n"), sHubName.GetString(), sLocPaths.GetString(), hubportcnt);
				//allhubinfo.insert(std::map<CString, PHUBINFO> ::value_type(sHubName, pinfo));
				allhubinfo[sHubName] = pinfo;
			}
			else
			{
				if (hubCapabilityEx != NULL)
				{
					free(hubCapabilityEx);
				}
				if (hubInfoEx != NULL)
				{
					free(hubInfoEx);
				}
			}
		}
	}
	else
	{
		dwRet = GetLastError();
		PrintError(dwRet, _T(__FILE__), __LINE__);
	}
	if (hDeviceInfo != INVALID_HANDLE_VALUE){
		SetupDiDestroyDeviceInfoList(hDeviceInfo);
	}
	return dwRet;
}

BOOL CheckFDHub(USHORT vid, USHORT pid)
{
	BOOL bRet = false;
	//gsupportlisthub
	CString s; s.Format(_T("VID_%04X&PID_%04X"), vid, pid);
	for each(auto a in gsupportlisthub)
	{
		if (a.CompareNoCase(s) == 0)
		{
			logIt(_T("find %s\n"), s.GetString());
			bRet = true;
			break;
		}
	}

	return bRet;
}

DWORD	GetHubIsFDRootHub(LPCTSTR hubname, int nVid, int nPid, BOOL bFindHubCnt)
{
	DWORD dwRet(ERROR_SUCCESS);
	int index = 0;
	BOOL bFindDongle = bFindHubCnt;
	BOOL bFindPort2HUB = FALSE;
	BOOL bFindPort3HUB = FALSE;
	ENTER_FUNCTION();

	HANDLE hub_handle = CreateFile(hubname, GENERIC_WRITE, FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, NULL);
	if (hub_handle != INVALID_HANDLE_VALUE)
	{
		DWORD BytesReturned;
		USB_NODE_INFORMATION nodeInformation;
		ZeroMemory(&nodeInformation, sizeof(USB_NODE_INFORMATION));
		BOOL success = DeviceIoControl(hub_handle, IOCTL_USB_GET_NODE_INFORMATION, &nodeInformation, sizeof(USB_NODE_INFORMATION), &nodeInformation, sizeof(USB_NODE_INFORMATION), &BytesReturned, NULL);
		if (success)
		{
			if (nodeInformation.u.HubInformation.HubDescriptor.bNumberOfPorts != HUBPORTCOUNT)
			{
				logIt(_T("Not fd auto detect HUB. hub port count!=4\n"));
			}
			for (index = 1; index <= nodeInformation.u.HubInformation.HubDescriptor.bNumberOfPorts && index < 4; index++)
			{
				//USB 3.0, sizeof(USB_NODE_CONNECTION_INFORMATION)buf is small. 
				TCHAR buf[1024] = { 0 };
				DWORD dwLen = sizeof(buf) / sizeof(buf[0]);
				PUSB_NODE_CONNECTION_INFORMATION pconnectionInformation = (PUSB_NODE_CONNECTION_INFORMATION)buf;
				//ZeroMemory(&connectionInformation,sizeof(USB_NODE_CONNECTION_INFORMATION));
				pconnectionInformation->ConnectionIndex = index;
				success = DeviceIoControl(hub_handle, IOCTL_USB_GET_NODE_CONNECTION_INFORMATION, pconnectionInformation, dwLen, pconnectionInformation, dwLen, &BytesReturned, NULL);
				if (success)
				{
					if (pconnectionInformation->ConnectionStatus == DeviceConnected)
					{
						if (index == HUBINDEXCONNECTDONGLEPORT &&
							pconnectionInformation->DeviceDescriptor.idVendor == DongleVID &&
							pconnectionInformation->DeviceDescriptor.idProduct == DonglePID)
						{
							bFindDongle = TRUE;
						}
						if ((index == CHILDCONPORT1 || index == CHILDCONPORT2) &&
							CheckFDHub(pconnectionInformation->DeviceDescriptor.idVendor, pconnectionInformation->DeviceDescriptor.idProduct) //&&
							//pconnectionInformation->DeviceDescriptor.idVendor == nVid &&
							//pconnectionInformation->DeviceDescriptor.idProduct == nPid
							)
						{
							if (index == CHILDCONPORT1) bFindPort2HUB = TRUE;
							else if (index == CHILDCONPORT2) bFindPort3HUB = TRUE;
						}
					}
				}
				else
				{
					dwRet = GetLastError();
					PrintError(dwRet, _T(__FILE__), __LINE__);
				}

				if (!bFindDongle)
				{
					logIt(_T("Port 1 not find fd dongle.\n"));
					break;
				}
			}
		}
		else
		{
			dwRet = GetLastError();
			PrintError(dwRet, _T(__FILE__), __LINE__);
		}
		CloseHandle(hub_handle);
	}
	else
	{
		dwRet = GetLastError();
		PrintError(dwRet, _T(__FILE__), __LINE__);
	}

	if (bFindDongle && bFindPort2HUB && bFindPort3HUB) dwRet = 0;
	else
	{
		logIt(_T("Not Found FD Root Hub.\n"));
		dwRet = ERROR_NOT_FOUND;
	}

	EXIT_FUNCTRET(dwRet);
	return dwRet;
}

DWORD CreateCalibrateFile(std::map<CString, CString>  &locationpathtosymblics, LPCTSTR fdRootHubLocationpaths, int indexlabel)
{
	int PortIndexArray[] = {1,4,2,3};
	int LabelIndexArray[] = {0,1,4,5};
	int nOtherPort = 1;
	int nFirstChildOffset = 4;
	int nSecondChildOffset = 12;
	if (bVertical)
	{
		nOtherPort = 5;
		nFirstChildOffset = 1;
		nSecondChildOffset = 3;
		LabelIndexArray[0] = 0;
		LabelIndexArray[1] = 5;
		LabelIndexArray[2] = 1;
		LabelIndexArray[3] = 6;
	}

	DWORD dwRet(ERROR_SUCCESS);
	ENTER_FUNCTION();
	CString sLocatpath;
	int nCount = indexlabel;
	if (locationpathtosymblics.find(fdRootHubLocationpaths) != locationpathtosymblics.end())
	{
		CString sHubName = locationpathtosymblics[fdRootHubLocationpaths];
		sHubName = sHubName.TrimLeft(_T("\\\\?\\")).TrimLeft(_T("\\\\.\\"));
		CString strLab; strLab.Format(_T("%d"), indexlabel);
		logIt(_T("[detected][label=%s]\n"), strLab);
		if (!WritePrivateProfileString(_T("label"), strLab, _T("1@")+sHubName, sIniFileName))
		{
			dwRet = GetLastError();
			PrintError(dwRet, _T(__FILE__), __LINE__);
		}
		sLocatpath.Format(_T("1@%s"), fdRootHubLocationpaths);
		if (!WritePrivateProfileString(_T("locationpaths"), strLab, sLocatpath, sIniFileName))
		{
			dwRet = GetLastError();
			PrintError(dwRet, _T(__FILE__), __LINE__);
		}
		nCount++;
		strLab.Format(_T("%d"), indexlabel + nOtherPort);
		logIt(_T("[detected][label=%s]\n"), strLab);
		if (!WritePrivateProfileString(_T("label"), strLab, _T("4@") + sHubName, sIniFileName))
		{
			dwRet = GetLastError();
			PrintError(dwRet, _T(__FILE__), __LINE__);
		}
		sLocatpath.Format(_T("4@%s"), fdRootHubLocationpaths);
		if (!WritePrivateProfileString(_T("locationpaths"), strLab, sLocatpath, sIniFileName))
		{
			dwRet = GetLastError();
			PrintError(dwRet, _T(__FILE__), __LINE__);
		}
		nCount++;
		TCHAR childLocationPath[1024] = { 0 };
		_sntprintf_s(childLocationPath, 1024, _T("%s#USB(%d)"), fdRootHubLocationpaths, CHILDCONPORT1);
		if (locationpathtosymblics.find(childLocationPath) != locationpathtosymblics.end())
		{
			sHubName = locationpathtosymblics[childLocationPath];
			sHubName = sHubName.TrimLeft(_T("\\\\?\\")).TrimLeft(_T("\\\\.\\"));

			for (int i = 0; i < 4; i++)
			{
				strLab.Format(_T("%d"), indexlabel + LabelIndexArray[i] + nFirstChildOffset);
				logIt(_T("[detected][label=%s]\n"), strLab);
				sLocatpath.Format(_T("%d@%s"), PortIndexArray[i], sHubName);
				if (!WritePrivateProfileString(_T("label"), strLab, sLocatpath, sIniFileName))
				{
					dwRet = GetLastError();
					PrintError(dwRet, _T(__FILE__), __LINE__);
				}
				sLocatpath.Format(_T("%d@%s"), PortIndexArray[i], childLocationPath);
				if (!WritePrivateProfileString(_T("locationpaths"), strLab, sLocatpath, sIniFileName))
				{
					dwRet = GetLastError();
					PrintError(dwRet, _T(__FILE__), __LINE__);
				}
				nCount++;
			}
		}
		else
			dwRet = ERROR_NOT_FOUND;

		_sntprintf_s(childLocationPath, 1024, _T("%s#USB(%d)"), fdRootHubLocationpaths, CHILDCONPORT2);
		if (locationpathtosymblics.find(childLocationPath) != locationpathtosymblics.end())
		{
			sHubName = locationpathtosymblics[childLocationPath];
			sHubName = sHubName.TrimLeft(_T("\\\\?\\")).TrimLeft(_T("\\\\.\\"));
			for (int i = 0; i < 4; i++)
			{
				strLab.Format(_T("%d"), indexlabel + LabelIndexArray[i] + nSecondChildOffset);
				logIt(_T("[detected][label=%s]\n"), strLab);
				sLocatpath.Format(_T("%d@%s"), PortIndexArray[i], sHubName);
				if (!WritePrivateProfileString(_T("label"), strLab, sLocatpath, sIniFileName))
				{
					dwRet = GetLastError();
					PrintError(dwRet, _T(__FILE__), __LINE__);
				}
				sLocatpath.Format(_T("%d@%s"), PortIndexArray[i], childLocationPath);
				if (!WritePrivateProfileString(_T("locationpaths"), strLab, sLocatpath, sIniFileName))
				{
					dwRet = GetLastError();
					PrintError(dwRet, _T(__FILE__), __LINE__);
				}
				nCount++;
			}
		}
		else
			dwRet = ERROR_NOT_FOUND;

	}
	if (nCount - 1 > 0) 
	{
		sLocatpath.Format(_T("%d"), nCount - 1);
		WritePrivateProfileString(_T("amount"), _T("labels"), sLocatpath, sIniFileName);
	}
	EXIT_FUNCTRET(dwRet);
	return dwRet;
}

BOOL CheckDuplicate(std::map<CString, CString>  locationpathtosymblics, LPCTSTR fdRootHubLocationpaths)
{
	BOOL bRet = FALSE;
	ENTER_FUNCTION();
	TCHAR value[1024] = { 0 };
	std::set<CString> listhub;
	for (int i = 1; i < gStartLabel; i++)
	{
		CString s; s.Format(_T("%d"), i);
		ZeroMemory(value, sizeof(value));
		GetPrivateProfileString(_T("label"), s, _T(""),value, sizeof(value), sIniFileName);
		if (_tcslen(value) > 0)
		{
			TCHAR* pp = _tcsstr(value, _T("@"));
			pp = pp == NULL ? pp : pp + _tcslen(_T("@"));
			if (pp != NULL)
			{
				logIt(_T("%s\n"),pp);
				auto result = listhub.insert(pp);
				if (!result.second) {
					logIt(_T("found %s in the label\n"), pp);
				}
			}
		}
	}

	if (locationpathtosymblics.find(fdRootHubLocationpaths) != locationpathtosymblics.end())
	{
		CString s = locationpathtosymblics[fdRootHubLocationpaths];
		s.TrimLeft(_T("\\\\?\\")).TrimLeft(_T("\\\\.\\"));
		logIt(_T("item %s\n"), s);
		if (listhub.find(s) != listhub.end())
		{
			bRet = TRUE;
		}
	}
	TCHAR childLocationPath[1024] = { 0 };
	_sntprintf_s(childLocationPath, 1024, _T("%s#USB(%d)"), fdRootHubLocationpaths, CHILDCONPORT1);
	if (!bRet && locationpathtosymblics.find(childLocationPath) != locationpathtosymblics.end())
	{
		CString s = locationpathtosymblics[fdRootHubLocationpaths];
		s.TrimLeft(_T("\\\\?\\")).TrimLeft(_T("\\\\.\\"));
		logIt(_T("item %s\n"), s);
		if (listhub.find(s) != listhub.end())
		{
			bRet = TRUE;
		}
	}
	_sntprintf_s(childLocationPath, 1024, _T("%s#USB(%d)"), fdRootHubLocationpaths, CHILDCONPORT2);
	if (!bRet && locationpathtosymblics.find(childLocationPath) != locationpathtosymblics.end())
	{
		CString s = locationpathtosymblics[fdRootHubLocationpaths];
		s.TrimLeft(_T("\\\\?\\")).TrimLeft(_T("\\\\.\\"));
		logIt(_T("item %s\n"), s);
		if (listhub.find(s) != listhub.end())
		{
			bRet = TRUE;
		}
	}


	logIt(_T("CheckDuplicate-- %s\n"), bRet?_T("TRUE"):_T("FALSE"));
	return bRet;
}

DWORD GetDongleParentInstanceID(CString &sParentId)
{
	DWORD dwRet(ERROR_SUCCESS);
	ENTER_FUNCTION();
	if (_tcsclen(sDongleInstanceID) == 0)
	{
		return ERROR_NOT_INSTALLED;
	}
	HDEVINFO hDevInfo = SetupDiCreateDeviceInfoList(NULL, NULL);
	if (hDevInfo != INVALID_HANDLE_VALUE)
	{

		SP_DEVINFO_DATA devInfoData;
		devInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
		if (SetupDiOpenDeviceInfo(hDevInfo, sDongleInstanceID, NULL, 0, &devInfoData))
		{
			DEVPROPTYPE type;
			BYTE b[2048] = { 0 };
			ZeroMemory(b, sizeof(b));
			DWORD sz = sizeof(b);
			if (SetupDiGetDeviceProperty(hDevInfo, &devInfoData, &DEVPKEY_Device_Parent, &type, b, sz, &sz, 0))
			{
				sParentId = (TCHAR *)b;
			}
			else
			{
				dwRet = GetLastError();
				PrintError(dwRet, _T(__FILE__), __LINE__);
			}
		}
		else
		{
			dwRet = GetLastError();
			PrintError(dwRet, _T(__FILE__), __LINE__);
		}
	}
	else
	{
		dwRet = GetLastError();
		PrintError(dwRet, _T(__FILE__), __LINE__);
	}
	return dwRet;
}

DWORD GetInstanceIDLocationPath(CString sInstanceid, CString &lpath)
{
	DWORD dwRet(ERROR_SUCCESS);
	ENTER_FUNCTION();
	if (_tcsclen(sDongleInstanceID) == 0)
	{
		return ERROR_NOT_INSTALLED;
	}
	HDEVINFO hDevInfo = SetupDiCreateDeviceInfoList(NULL, NULL);
	if (hDevInfo != INVALID_HANDLE_VALUE)
	{

		SP_DEVINFO_DATA devInfoData;
		devInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
		if (SetupDiOpenDeviceInfo(hDevInfo, sDongleInstanceID, NULL, 0, &devInfoData))
		{
			DEVPROPTYPE type;
			BYTE b[2048] = { 0 };
			ZeroMemory(b, sizeof(b));
			DWORD sz = sizeof(b);
			if (SetupDiGetDeviceProperty(hDevInfo, &devInfoData, &DEVPKEY_Device_LocationPaths, &type, b, sz, &sz, 0))
			{
				if (type == DEVPROP_TYPE_STRING_LIST)
				{
					lpath = (TCHAR *)b;
				}
			}
			else
			{
				dwRet = GetLastError();
				PrintError(dwRet, _T(__FILE__), __LINE__);
			}
		}
		else
		{
			dwRet = GetLastError();
			PrintError(dwRet, _T(__FILE__), __LINE__);
		}
	}
	else
	{
		dwRet = GetLastError();
		PrintError(dwRet, _T(__FILE__), __LINE__);
	}
	return dwRet;
}

BOOL CheckFDSupportHub(CString sHubInstanceID)
{
	ENTER_FUNCTION();
	BOOL bRet = FALSE;
	for each(auto a in gsupportlisthub)
	{
		if (sHubInstanceID.MakeUpper().Find(a) == 0)
		{
			logIt(_T("find %s\n"), sHubInstanceID);
			bRet = true;
			break;
		}
	}
	return bRet;
}

BOOL CheckLabelX(CHub^ hub, CString hubinstanceid, int currentx, int &hublevel, int plusLabel = 1)
{
	ENTER_FUNCTION();
	BOOL bRet = FALSE;
	hublevel++;
	for each(auto ss in hub->hubports)
	{
		if (ss.Value->GetType() == CHub::typeid)
		{
			CHub^ temp = (CHub^)ss.Value;
			bRet = CheckLabelX(temp, hubinstanceid, currentx, hublevel, plusLabel);
			if (bRet) break;
			hublevel--;
		}
		else
		{
			//plug children first port ,it has some problem;
			//USB\VID_0BDA&PID_5411\5&38864a90&0&9
			String^ sHubpidvid = String::Format("USB\\VID_{0}&PID_{1}", hub->vid->ToUpper(), hub->pid->ToUpper());
			String^ shubinid = gcnew String(hubinstanceid.GetString());
			if (!shubinid->StartsWith(sHubpidvid, StringComparison::OrdinalIgnoreCase)) 
				continue;
			if (currentx == ss.Key && Convert::ToInt16(ss.Value) == plusLabel)
			{
				bRet = TRUE;
				break;
			}
		}
	}

	return bRet;
}

DWORD ListAllUsbHubLocation(std::map<CString, PHUBINFO> &locpathhubinfo)
{
	ENTER_FUNCTION();
	HANDLE                              hHub;
	HDEVINFO                            hDeviceInfo;
	SP_DEVICE_INTERFACE_DATA            _sdid;
	PSP_DEVICE_INTERFACE_DETAIL_DATA    _psdidd;
	DWORD dwLength;
	DWORD dwBytesX;
	DWORD dwRet = ERROR_SUCCESS;

	hDeviceInfo = SetupDiGetClassDevs(&GUID_DEVINTERFACE_USB_HUB, NULL, NULL, DIGCF_PRESENT | DIGCF_DEVICEINTERFACE);
	if (hDeviceInfo != INVALID_HANDLE_VALUE)
	{
		_sdid.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);
		for (int i = 0; SetupDiEnumDeviceInterfaces(hDeviceInfo, NULL, &GUID_DEVINTERFACE_USB_HUB, i, &_sdid); i++)
		{
			SetupDiGetDeviceInterfaceDetail(hDeviceInfo, &_sdid, NULL, 0, &dwBytesX, NULL);
			dwLength = dwBytesX;
			_psdidd = (PSP_DEVICE_INTERFACE_DETAIL_DATA)GlobalAlloc(GPTR, dwLength);
			if (_psdidd == NULL)
			{
				continue;
			}

			_psdidd->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);
			if (!SetupDiGetDeviceInterfaceDetail(hDeviceInfo, &_sdid, _psdidd, dwLength, &dwBytesX, NULL))
			{
				GlobalFree(_psdidd);
				continue;
			}

			BYTE pdata[4096] = { 0 };
			DWORD nLen = sizeof(pdata);
			DWORD nType = 0;
			CString sLocPaths;
			CString sHubName = _psdidd->DevicePath;
			if (DWORD dwRet = GetDeviceCommonSymbl(_psdidd->DevicePath, (PDEVPROPKEY)&DEVPKEY_Device_LocationPaths, pdata, nLen, nType) == ERROR_SUCCESS)
			{
				if (nType == DEVPROP_TYPE_STRING_LIST)
				{
					//add locationPaths
					sLocPaths = (TCHAR *)pdata;
				}
			}
			else
			{
				PrintError(dwRet, _T(__FILE__), __LINE__);
				GlobalFree(_psdidd);
				continue;
			}

			hHub = CreateFile(_psdidd->DevicePath, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, NULL);

			GlobalFree(_psdidd);

			DWORD BytesReturned;
			int hubportcnt = 0;
			USB_NODE_INFORMATION nodeInformation;
			ZeroMemory(&nodeInformation, sizeof(USB_NODE_INFORMATION));
			if (DeviceIoControl(hHub, IOCTL_USB_GET_NODE_INFORMATION, &nodeInformation, sizeof(USB_NODE_INFORMATION), &nodeInformation, sizeof(USB_NODE_INFORMATION), &BytesReturned, NULL))
			{
				hubportcnt = nodeInformation.u.HubInformation.HubDescriptor.bNumberOfPorts;
			}
			else
			{
				PrintError(GetLastError(), _T(__FILE__), __LINE__);
				CloseHandle(hHub);
				continue;
			}

			CloseHandle(hHub);

			PHUBINFO pinfo = new HUBINFO();
			if (pinfo != NULL)
			{
				pinfo->sHubName = sHubName;
				pinfo->sLocationpath = sLocPaths;
				pinfo->portcount = hubportcnt;
				logIt(_T("find:%s==locatiopath:%s==portcount:%d\n"), sHubName, sLocPaths, hubportcnt);
				//locpathhubinfo.insert(std::map<CString, PHUBINFO> ::value_type(sLocPaths, pinfo));
				locpathhubinfo[sLocPaths] = pinfo;
			}
		}
	}
	else
	{
		dwRet = GetLastError();
		PrintError(dwRet, _T(__FILE__), __LINE__);
	}
	if (hDeviceInfo != INVALID_HANDLE_VALUE){
		SetupDiDestroyDeviceInfoList(hDeviceInfo);
	}
	return dwRet;
}

BOOL CheckConnect1Label(CHub^ hub, CString sHubinstanceid, CString sDongleLocationpath, String^ &rootlocpath, int &nlevel)
{
	ENTER_FUNCTION();
	BOOL bRet = FALSE;

	//PCIROOT(0)#PCI(1400)#USBROOT(0)#USB(3)
	//PCIROOT(0)#PCI(1400)#USBROOT(0)#USB(4)#USBMI(1)
	array<wchar_t>^ sp = { '#' };
	array<String^>^ aIDs = (gcnew String(sDongleLocationpath.GetString()))->Split(sp, StringSplitOptions::RemoveEmptyEntries);
	int portindex = 0;
	int hublocpathlen = aIDs->Length;
	for (int i = aIDs->Length - 1; i >= 0; i--)
	{
		Regex^ pidvidregex = gcnew Regex("USB\\((\\d+)\\)$", RegexOptions::IgnoreCase);
		Match^ m = pidvidregex->Match(aIDs[i]);
		if (m->Success)
		{
			portindex = Convert::ToInt32(m->Groups[1]->Value);
			hublocpathlen = i + 1;
			break;
		}
	}
		
	int hublevel = 0;
	if (portindex > 0 && CheckLabelX(hub, sHubinstanceid, portindex, hublevel))
	{
		StringBuilder^ builder = gcnew StringBuilder();
		builder->Append(aIDs[0]);
		for (int i = 1; i < hublocpathlen - hublevel; i++)
		{
			builder->Append("#");
			builder->Append(aIDs[i]);
		}
		rootlocpath= builder->ToString();

		nlevel = hublevel;
		bRet = TRUE;
	}


	return bRet;
}

BOOL Findlocationpathsinhubmap(CHub^ pHub, std::map<CString, PHUBINFO> locpathhubinfo)
{
	ENTER_FUNCTION();
	BOOL bRet = TRUE;

	bRet = locpathhubinfo.find((const wchar_t*)(Marshal::StringToHGlobalUni(pHub->locationpath)).ToPointer()) != locpathhubinfo.end();
	logIt(_T("Findlocationpathsinhubmap %s\n"), (const wchar_t*)(Marshal::StringToHGlobalUni(pHub->locationpath)).ToPointer());
	if (!bRet) return bRet;
	for each(auto ss in pHub->hubports)
	{
		if (ss.Value->GetType() == CHub::typeid)
		{
			CHub^ temp = (CHub^)ss.Value;
			bRet = Findlocationpathsinhubmap(temp, locpathhubinfo);
			if (!bRet) break;
		}
	}
	return bRet;
}

BOOL SetLocalPathsPlugDongle(CHub^ pHub, std::map<CString, PHUBINFO> locpathhubinfo, String^ locpth, std::list<CString> &localpaths, int nLevel)
{
	ENTER_FUNCTION();
	BOOL bmatched = TRUE;
	if (pHub == nullptr || String::IsNullOrEmpty(locpth)) return FALSE;
	if (nLevel == 1)
	{
		pHub->SetLocationPath(locpth);
		localpaths.push_back((const wchar_t*)(Marshal::StringToHGlobalUni(locpth)).ToPointer());
	}
	else
	{
		array<wchar_t>^ sp = { '#' };
		array<String^>^ aIDs = locpth->Split(sp, StringSplitOptions::RemoveEmptyEntries);
		StringBuilder^ sb = gcnew StringBuilder();
		sb->Append(aIDs[0]);
		for (int i = 1; i < aIDs->Length - nLevel; i++)
		{
			sb->Append("#");
			sb->Append(aIDs[i]);
		}
		pHub->SetLocationPath(sb->ToString());
		using namespace Runtime::InteropServices;
		localpaths.push_back((const wchar_t*)(Marshal::StringToHGlobalUni(locpth)).ToPointer());
	}

	bmatched = Findlocationpathsinhubmap(pHub, locpathhubinfo);


	return bmatched;
}

DWORD	GetHubInfoV2()
{
	DWORD dwRet(ERROR_SUCCESS);
	ENTER_FUNCTION();

	CConfigInfo^ info = gcnew CConfigInfo();
	dwRet = info->LoadConfigXml();
	if (dwRet != ERROR_SUCCESS) return dwRet;

	if (info->hubs->Count == 0) return ERROR_NOT_FOUND;

	std::map<CString, PHUBINFO> locpathhubinfo;
	dwRet = ListAllUsbHubLocation(locpathhubinfo);
	if (dwRet != ERROR_SUCCESS) return dwRet;

	int startindex = gStartLabel - 1;

	CString sDongleLocPath;
	if ((dwRet = GetInstanceIDLocationPath(sDongleInstanceID, sDongleLocPath)) == ERROR_SUCCESS)
	{
		CString sHubinstanceid;
		BOOL bSupport = false;
		if ((dwRet = GetDongleParentInstanceID(sHubinstanceid)) == ERROR_SUCCESS && (bSupport=CheckFDSupportHub(sHubinstanceid)))
		{
			BOOL bFind = false;
			int nCount = 0;

			for each(auto hub in info->hubs)
			{
				String^ Rootlocpath; int nLevel=0;
				if (CheckConnect1Label(hub, sHubinstanceid, sDongleLocPath, Rootlocpath, nLevel))
				{
					std::list<CString> locpathlist;
					if (SetLocalPathsPlugDongle(hub, locpathhubinfo, Rootlocpath, locpathlist, nLevel)) //
					{
						if (!g_bUpdate)
						{
							//do checkini
							if (CheckDuplicateV2(locpathlist))
							{
								logIt(_T("[duplicate][label=%d]\n"), gStartLabel);
								dwRet = ERROR_DUP_NAME;
								return dwRet;
							}
						}
						for each(auto lp in locpathlist)
						{
							CString sPeerLocationPath;
							GetPeerHub(lp, sPeerLocationPath, locpathhubinfo);
							logIt(_T("find peer location path %s\n"), sPeerLocationPath);
							if (!sPeerLocationPath.IsEmpty())
							{
								hub->SetLocationPath(gcnew String(sPeerLocationPath.GetString()));
								GetHubCalibration(locpathhubinfo, hub, startindex, nCount);
							}
							nCount = 0;
							hub->SetLocationPath(gcnew String(lp.GetString()));
							GetHubCalibration(locpathhubinfo, hub, startindex, nCount);
							logIt(_T("[count][count=%d]\n"), nCount);
							startindex += nCount;
							bFind = true;
							dwRet = ERROR_SUCCESS;
						}
						if (bFind) break;
					}
					else
					{
						logIt(_T("Not Found this Hub:%s"), (const wchar_t*)(Marshal::StringToHGlobalUni(Rootlocpath)).ToPointer());
					}
				}
			}
		}
		else
		{
			if (!bSupport)
			{
				dwRet = ERROR_NOT_SUPPORTED;
			}
		}
	}


	return  dwRet;
}

DWORD	GetHubInfo(int nvid, int npid)
{
	DWORD dwRet(ERROR_SUCCESS);

	ENTER_FUNCTION();

	HDEVINFO                         deviceInfo;
	SP_DEVICE_INTERFACE_DATA         deviceInterfaceData;
	SP_DEVINFO_DATA					 deviceInfoData;
	PSP_DEVICE_INTERFACE_DETAIL_DATA deviceDetailData;
	ULONG                            index;
	ULONG                            requiredLength;

	std::map<CString, CString>  locationpathtosymblics;
	TCHAR fdRootHubLocationpaths[1024] = { 0 };

	BOOL	bFindFDRootHub = FALSE;

	deviceInfo = SetupDiGetClassDevs((LPGUID)&GUID_DEVINTERFACE_USB_HUB,
		NULL,
		NULL,
		(DIGCF_PRESENT | DIGCF_DEVICEINTERFACE));

	if (deviceInfo != INVALID_HANDLE_VALUE)
	{
		deviceInterfaceData.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);
		deviceInfoData.cbSize = sizeof(SP_DEVINFO_DATA);

		for (index = 0;
			SetupDiEnumDeviceInterfaces(deviceInfo,
			0,
			(LPGUID)&GUID_DEVINTERFACE_USB_HUB,
			index,
			&deviceInterfaceData);
		index++)
		{
			TCHAR	buf[1024] = { 0 };
			int ln = sizeof(buf) / sizeof(buf[0]);

			memset(buf, 0, sizeof(buf));
			deviceDetailData = (PSP_DEVICE_INTERFACE_DETAIL_DATA)buf;

			deviceDetailData->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);
			requiredLength = ln;

			if (SetupDiGetDeviceInterfaceDetail(deviceInfo,
				&deviceInterfaceData,
				deviceDetailData,
				requiredLength,
				&requiredLength,
				&deviceInfoData))
			{
				CString sName = deviceDetailData->DevicePath;
				Regex^ pidvidregex = gcnew Regex(".*?#VID_(?<vid>[A-Fa-f0-9]+).*?PID_(?<pid>[A-Fa-f0-9]+)#.*?", RegexOptions::IgnoreCase);
				Match^ m = pidvidregex->Match(gcnew String(sName.GetString()));
				int pid = 0, vid = 0;
				if (m->Success)
				{
					vid = Convert::ToInt32(m->Groups["vid"]->Value, 16);
					pid = Convert::ToInt32(m->Groups["pid"]->Value, 16);
				}
				else continue;

				//if (pid == npid && vid == nvid)
				if (CheckFDHub(vid, pid))
				{
					logIt(_T("find hub: %s\n"), sName);
					DEVPROPTYPE type;
					BYTE b[2048];
					ZeroMemory(b, sizeof(b));
					DWORD sz = 0;

					if (SetupDiGetDeviceProperty(deviceInfo, &deviceInfoData, &DEVPKEY_Device_LocationPaths, &type, b, 2048, &sz, 0))
					{
						TCHAR *locationPaths = (TCHAR *)b; //Only first, like:PCIROOT(0)#PCI(0201)#USBROOT(0)#USB(3)
						locationpathtosymblics[locationPaths] = sName;//locationpathtosymblics.insert(map<CString, CString>::value_type(locationPaths, sName))

						if (!bFindFDRootHub && GetHubIsFDRootHub(sName, nvid, npid, false) == ERROR_SUCCESS)
						{
							bFindFDRootHub = TRUE;
							_sntprintf_s(fdRootHubLocationpaths, 1024, _T("%s"), locationPaths);							
						}
					}
					else
					{
						PrintError(GetLastError(), _T(__FILE__), __LINE__);
					}
				}
			}
			else
			{				
				PrintError(GetLastError(), _T(__FILE__), __LINE__);
			}
		}

		SetupDiDestroyDeviceInfoList(deviceInfo);
	}
	else
	{
		dwRet = GetLastError();
	}

	if (bFindFDRootHub)
	{
		if (!g_bUpdate)
		{
			//do checkini
			if (CheckDuplicate(locationpathtosymblics, fdRootHubLocationpaths))
			{
				logIt(_T("[duplicate][label=%d]\n"), gStartLabel);
				dwRet = ERROR_DUP_NAME;
				return dwRet;
			}
		}

		dwRet = CreateCalibrateFile(locationpathtosymblics, fdRootHubLocationpaths, gStartLabel);
	}

	if (dwRet == ERROR_SUCCESS && !bFindFDRootHub) dwRet = ERROR_NOT_FOUND;

	EXIT_FUNCTRET(dwRet);
	return dwRet;
}

BOOL CheckFDHubsymblink(CString s)
{
	BOOL bRet = false;
	//gsupportlisthub
	for each(auto a in gsupportlisthub)
	{
		if (s.Find(a) != -1)
		{
			bRet = true;
			break;
		}
	}

	return bRet;
}

int GetFDFastHubCount()
{
	int ret = 0;
	DWORD dwRet(ERROR_SUCCESS);

	ENTER_FUNCTION();

	HDEVINFO                         deviceInfo;
	SP_DEVICE_INTERFACE_DATA         deviceInterfaceData;
	SP_DEVINFO_DATA					 deviceInfoData;
	PSP_DEVICE_INTERFACE_DETAIL_DATA deviceDetailData;
	ULONG                            index;
	ULONG                            requiredLength;

	std::map<CString, CString>  Driverkeytosymblics;
	TCHAR fdRootHubLocationpaths[1024] = { 0 };

	BOOL	bFindFDRootHub = FALSE;

	deviceInfo = SetupDiGetClassDevs((LPGUID)&GUID_DEVINTERFACE_USB_HUB,
		NULL,
		NULL,
		(DIGCF_PRESENT | DIGCF_DEVICEINTERFACE));

	if (deviceInfo != INVALID_HANDLE_VALUE)
	{
		deviceInterfaceData.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);
		deviceInfoData.cbSize = sizeof(SP_DEVINFO_DATA);

		for (index = 0;  
			SetupDiEnumDeviceInterfaces(deviceInfo, 0, (LPGUID)&GUID_DEVINTERFACE_USB_HUB, index, &deviceInterfaceData); 
			index++)
		{
			TCHAR	buf[1024] = { 0 };
			int ln = sizeof(buf) / sizeof(buf[0]);

			memset(buf, 0, sizeof(buf));
			deviceDetailData = (PSP_DEVICE_INTERFACE_DETAIL_DATA)buf;

			deviceDetailData->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);
			requiredLength = ln;

			if (SetupDiGetDeviceInterfaceDetail(deviceInfo, &deviceInterfaceData, deviceDetailData,
				requiredLength, &requiredLength, &deviceInfoData))
			{
				CString sName = deviceDetailData->DevicePath;
				//is fd hub?? check //usb#vid_0bda&pid_5401#5&3244c158&0&3#{f18a0e88-c30c-11d0-8815-00a0c906bed8}
				sName.MakeUpper();
				//CString sSubStr; sSubStr.Format(_T("USB#VID_%04X&PID_%04X#"), FDHUBVID, FDHUBPID);
				//if (sName.Find(sSubStr)!=-1)
				if (CheckFDHubsymblink(sName))
				{
					logIt(_T("find hub: %s\n"), sName);
					DEVPROPTYPE type;
					BYTE b[2048];
					ZeroMemory(b, sizeof(b));
					DWORD sz = 0;

					if (SetupDiGetDeviceProperty(deviceInfo, &deviceInfoData, &DEVPKEY_Device_Driver, &type, b, 2048, &sz, 0))
					{
						Driverkeytosymblics[(TCHAR*)b] = sName;
					}
					else
					{
						PrintError(GetLastError(), _T(__FILE__), __LINE__);
					}
				}
			}
			else
			{
				PrintError(GetLastError(), _T(__FILE__), __LINE__);
			}
		}

		SetupDiDestroyDeviceInfoList(deviceInfo);
	}
	else
	{
		dwRet = GetLastError();
	}
	EXIT_FUNCTRET(dwRet);

	if (Driverkeytosymblics.size() < 3) return 0;

	if (Driverkeytosymblics.size() % 3 != 0)
	{
		logIt(_T("What is the problem. why has FD PID VID hub chip.\n"));
	}
	else{
		return Driverkeytosymblics.size() / 3;
	}

	//if has fd Hub count 

	std::map<CString, CString>::iterator itr = Driverkeytosymblics.begin();
	while (itr != Driverkeytosymblics.end()) {
		if (GetHubIsFDRootHub(itr->second, FDHUBVID, FDHUBPID, true) == ERROR_SUCCESS) ret++;
		itr++;
	}

	EXIT_FUNCTRET(dwRet);
	return ret;
}

BOOL MatchLocalPaths(CHub^ pHub, std::map<CString, PHUBINFO> lochubinfo)
{

	using namespace Runtime::InteropServices;
	const wchar_t* lpLocpth =
		(const wchar_t*)(Marshal::StringToHGlobalUni(pHub->locationpath)).ToPointer();
	BOOL bRet = lochubinfo.find(lpLocpth) != lochubinfo.end();
	if (!bRet) return bRet;
	for each(auto ss in pHub->hubports)
	{
		if (ss.Value->GetType() == CHub::typeid)
		{
			CHub^ temp = (CHub^)ss.Value;
			bRet = MatchLocalPaths(temp, lochubinfo);
			if (!bRet) return bRet;
		}
	}
	return bRet;
}

BOOL FindLocalPaths(CHub^ pHub, std::map<CString, PHUBINFO> lochubinfo, std::list<CString> &localpaths)
{
	ENTER_FUNCTION();
	BOOL bmatched = FALSE;
	for (std::map<CString, PHUBINFO>::iterator iter = lochubinfo.begin(); iter != lochubinfo.end(); iter++)
	{
		pHub->SetLocationPath(gcnew String(iter->first.GetString()));
		
		String^ hubname = gcnew String(iter->second->sHubName.GetString());

		if (MatchLocalPaths(pHub, lochubinfo))
		{
			bmatched = TRUE;
			if (pHub->IsCurrentUSB30(hubname))
			{
				localpaths.insert(localpaths.begin(), iter->first);
			}
			else
			{
				localpaths.push_back(iter->first);
			}
		}
	}
	return bmatched;
}

void GetHubCalibration(std::map<CString, PHUBINFO> lochubinfo, CHub^ pHub, int startindex, int &Count)
{
	ENTER_FUNCTION();
	const wchar_t* lpLocpth =
		(const wchar_t*)(Marshal::StringToHGlobalUni(pHub->locationpath)).ToPointer();

	if (lochubinfo.find(lpLocpth) == lochubinfo.end()) return;
	CString sHubName = lochubinfo[lpLocpth]->sHubName;
	sHubName = sHubName.TrimLeft(_T("\\\\?\\")).TrimLeft(_T("\\\\.\\"));

	BOOL bUsb30 = pHub->IsCurrentUSB30(gcnew String(sHubName.GetString()));
	if (bUsb30) 
	{
		bUsb30 = lochubinfo[lpLocpth]->GetHubType()==Usb30Hub;
	}
	extern CIniRecord inirecords;
	BOOL isCheckSec = FALSE; 
	if (pHub->autocnt > 0) {
		isCheckSec = (startindex / pHub->autocnt) % 2 == 1;
		//logIt(_T("start=%d, count=%d, ischeck=%d"), startindex, pHub->autocnt, isCheckSec);
	}
	for each(auto ss in pHub->hubports)
	{
		if (ss.Value->GetType() == CHub::typeid)
		{
			CHub^ temp = (CHub^)ss.Value;
			GetHubCalibration(lochubinfo, temp, startindex, Count);
		}
		else
		{
			if (pHub->autocnt == 0 || !isCheckSec)
				if (String::IsNullOrEmpty(ss.Value->ToString()) || String::Compare(ss.Value->ToString(), gcnew String("0"), true) == 0) continue;

			int curLabel = Convert::ToInt16(ss.Value);
			if (isCheckSec) {
				if (!pHub->hubportsLabel1->ContainsKey(ss.Key)) {
					continue;
				}
				curLabel = pHub->hubportsLabel1[ss.Key];
				logIt(_T("use current label=%d"), curLabel);
			}

			CString strLab; 
			strLab.Format(_T("%d"), startindex+ curLabel);
			CString sLocatpath;
			sLocatpath.Format(_T("%d@%s"), ss.Key, sHubName);	
			int nLabel = startindex + curLabel;
			logIt(_T("%d=%s"), nLabel, sLocatpath);
			if (!bUsb30)
			{
				if (inirecords.CheckLabels(sLocatpath)) continue;
				if (!WritePrivateProfileString(_T("label"), strLab, sLocatpath, sIniFileName))
				{
					PrintError(GetLastError(), _T(__FILE__), __LINE__);
				}
				inirecords.WriteToMap(_T("label"), nLabel, sLocatpath);

				if (inirecords.CheckLabels_2(sLocatpath)) continue;
				if (!WritePrivateProfileString(_T("label_2.0"), strLab, sLocatpath, sIniFileName))
				{
					PrintError(GetLastError(), _T(__FILE__), __LINE__);
				}
				inirecords.WriteToMap(_T("label_2.0"), nLabel, sLocatpath);
			}
			else
			{
				if (inirecords.CheckLabels_3(sLocatpath)) continue;
				if (!WritePrivateProfileString(_T("label_3.0"), strLab, sLocatpath, sIniFileName))
				{
					PrintError(GetLastError(), _T(__FILE__), __LINE__);
				}
				inirecords.WriteToMap(_T("label_3.0"), nLabel, sLocatpath);
			}

			sLocatpath.Format(_T("%d@%s"), ss.Key, lpLocpth);
			logIt(_T("%d=%s"), nLabel, sLocatpath);
			if (!bUsb30)
			{
				if (inirecords.CheckLocationpaths(sLocatpath)) continue;
				if (!WritePrivateProfileString(_T("locationpaths"), strLab, sLocatpath, sIniFileName))
				{
					PrintError(GetLastError(), _T(__FILE__), __LINE__);
				}
				inirecords.WriteToMap(_T("locationpaths"), nLabel, sLocatpath);

				if (inirecords.CheckLocationpaths_2(sLocatpath)) continue;
				if (!WritePrivateProfileString(_T("locationpaths_2.0"), strLab, sLocatpath, sIniFileName))
				{
					PrintError(GetLastError(), _T(__FILE__), __LINE__);
				}
				inirecords.WriteToMap(_T("locationpaths_2.0"), nLabel, sLocatpath);
			}
			else
			{
				if (inirecords.CheckLocationpaths_3(sLocatpath)) continue;
				if (!WritePrivateProfileString(_T("locationpaths_3.0"), strLab, sLocatpath, sIniFileName))
				{
					PrintError(GetLastError(), _T(__FILE__), __LINE__);
				}
				inirecords.WriteToMap(_T("locationpaths_3.0"), nLabel, sLocatpath);
			}

			logIt(_T("[detected][label=%s]\n"), strLab);
			if (!bUsb30)
				Count++;

		}
	}
	if (!bUsb30 && Count + gStartLabel - 1 > 0)
	{
		sHubName.Format(_T("%d"), Count + gStartLabel - 1);
		WritePrivateProfileString(_T("amount"), _T("labels"), sHubName, sIniFileName);
	}
}

String^ GetHubIDsFromXML(CHub^ hub)
{
	ENTER_FUNCTION();
	String^ sID = String::Format("usb#vid_{0}&pid_{1}#", hub->vid, hub->pid);
	if (hub->IsSupportUsb30()) sID = String::Format("{0}|usb#{1}#", sID, hub->usb30pidvid);
	for each(auto ss in hub->hubports)
	{
		if (ss.Value->GetType() == CHub::typeid)
		{
			CHub^ temp = (CHub^)ss.Value;
			String^ ss = GetHubIDsFromXML(temp);
			if (sID->IndexOf(ss) < 0)
				sID += "|" + ss;
		}
	}
	return sID;
}

String^ GetHubDependentFromXML(CHub^ hub)
{
	ENTER_FUNCTION();
	String^ sID = String::Format("usb\\\\vid_{0}&pid_{1}\\\\", hub->vid, hub->pid);
	if (hub->IsSupportUsb30()) sID = String::Format("{0}|usb\\\\{1}\\\\", sID, hub->usb30pidvid);
	for each(auto ss in hub->hubports)
	{
		if (ss.Value->GetType() == CHub::typeid)
		{
			CHub^ temp = (CHub^)ss.Value;
			String^ ss = GetHubDependentFromXML(temp);
			if (sID->IndexOf(ss) < 0)
				sID += "|" + ss;
		}
	}
	return sID;
}
//
//BOOL IsSupportCalibrateHub(CConfigInfo^ info, CString Dependent, std::map<CString, PHUBINFO> &lochubinfo)
//{
//	ENTER_FUNCTION();
//	for each(auto hub in info->hubs)
//	{
//		String^ sID = GetHubDependentFromXML(hub);
//		System::Diagnostics::Trace::WriteLine(sID);
//
//		Regex^ pidvidregex = gcnew Regex(sID, RegexOptions::IgnoreCase | RegexOptions::Compiled);
//		Match^ m = pidvidregex->Match(gcnew String(Dependent.GetString()));
//		if (m->Success)
//		{
//			PHUBINFO pinfo = new HUBINFO();
//			pinfo->sHubName = _T("\\\\?\\") + Dependent.Replace(_T("\\"), _T("#")) + _T("#{f18a0e88-c30c-11d0-8815-00a0c906bed8}");
//		}
//
//	}
//	return TRUE;
//}

BOOL CheckDuplicateV2(std::list<CString> locpaths)
{
	ENTER_FUNCTION();
	BOOL bRet = FALSE;
	ENTER_FUNCTION();
	TCHAR value[1024] = { 0 };
	std::set<CString> listhub;
	for (int i = 1; i <= gStartLabel; i++)
	{
		CString s; s.Format(_T("%d"), i);
		ZeroMemory(value, sizeof(value));
		GetPrivateProfileString(_T("locationpaths"), s, _T(""), value, sizeof(value), sIniFileName);
		if (_tcslen(value) > 0)
		{
			TCHAR* pp = _tcsstr(value, _T("@"));
			pp = pp == NULL ? pp : pp + _tcslen(_T("@"));
			if (pp != NULL)
			{
				logIt(_T("%s\n"),pp);
				auto result = listhub.insert(pp);
				if (!result.second) {
					logIt(_T("Insert %s, it show duplicate\n"), pp);
				}
			}
		}
	}

	for each(CString Locationpath in locpaths)
	{
		bRet = listhub.find(Locationpath) != listhub.end();
		if (bRet) break;
	}
	return bRet;
}

DWORD GetPeerHub(CString locPath, CString &sPeerLocationPath, std::map<CString, PHUBINFO> alist)
{
	ENTER_FUNCTION();
	DWORD dwRet(ERROR_INVALID_PARAMETER);
	if (locPath.IsEmpty()) return dwRet;
	String^ sLP = gcnew String(locPath.GetString());
	String^ sPatten = "(.*?#USBROOT\\(\\d+\\))#USB\\((\\d+)\\)"; 
	//PCIROOT(0)#PCI(1400)#USBROOT(0)#USB(4)
	//PCIROOT(0)#PCI(1D00)#USBROOT(0)
	Regex^ regRootusb = gcnew Regex(sPatten, RegexOptions::IgnoreCase | RegexOptions::Compiled);
	Match^ m = regRootusb->Match(sLP);
	if (m->Success)
	{
		String^ rootLocPaht = m->Groups[1]->Value;
		String^ hubPort = m->Groups[2]->Value;
		const wchar_t* lprootlocpath =
			(const wchar_t*)(Marshal::StringToHGlobalUni(rootLocPaht)).ToPointer();
		
		PHUBINFO pHI = NULL;
		for (std::map<CString, PHUBINFO>::iterator iter = alist.begin(); iter != alist.end(); iter++)
		{
			if (iter->second != NULL)
			{
				pHI = iter->second;
				if (pHI->sLocationpath.CompareNoCase(lprootlocpath) == 0)
				{
					break;
				}
			}
		}

		if (pHI != NULL)
		{
			HANDLE hHub = CreateFile(pHI->sHubName, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, NULL);
			if (hHub != INVALID_HANDLE_VALUE)
			{
				PUSB_PORT_CONNECTOR_PROPERTIES pPortConnectorProps = NULL;
				dwRet = Get_USB_PORT_CONNECTOR_PROPERTIES(hHub, Convert::ToInt16(hubPort), &pPortConnectorProps);
				//pPortConnectorProps->CompanionPortNumber;
				//sPeerLocationPath = locPath.Left(locPath.Find(_T("#USB(")) + _tcsclen(_T("#USB(")+);
				if (pPortConnectorProps != NULL)
				{
					StringBuilder^ sb = gcnew StringBuilder();
					int nLength = sLP->IndexOf("#USB(") + 5;
					sb->Append(sLP->Substring(0, nLength));
					sb->Append(pPortConnectorProps->CompanionPortNumber);
					int nStart = sLP->IndexOf(")", nLength);
					sb->Append(sLP->Substring(nStart));

					sPeerLocationPath =
						(const wchar_t*)(Marshal::StringToHGlobalUni(sb->ToString())).ToPointer();


					if (pPortConnectorProps != NULL)
					{
						free(pPortConnectorProps);
					}
					dwRet = ERROR_SUCCESS;
				}
			}
			else
			{
				dwRet = GetLastError();
				PrintError(dwRet, _T(__FILE__), __LINE__);
			}
		}
		else
		{
			dwRet = ERROR_NOT_FOUND;
		}

	}
	return dwRet;
}

DWORD CalibrateStaticHub()
{
	ENTER_FUNCTION();
	CConfigInfo^ info = gcnew CConfigInfo();
	DWORD dwRet = info->LoadConfigXml();
	if (dwRet != ERROR_SUCCESS) return dwRet;

	if (info->hubs->Count == 0) return ERROR_NOT_FOUND;

	dwRet = ListAllUsbHub();
	if (dwRet != ERROR_SUCCESS) return dwRet;

	int startindex = gStartLabel-1;
	int findsupporthubcount = 1;

	for each(auto hub in info->hubs)
	{
		String^ sID = GetHubIDsFromXML(hub);
		System::Diagnostics::Trace::WriteLine(sID);
		Regex^ pidvidregex = gcnew Regex(sID, RegexOptions::IgnoreCase | RegexOptions::Compiled);
		std::map<CString, PHUBINFO> lochubinfo;
		for (std::map<CString, PHUBINFO>::iterator iter = allhubinfo.begin(); iter != allhubinfo.end(); iter++)
		{
			Match^ m = pidvidregex->Match(gcnew String(iter->first.GetString()));
			if (m->Success)
			{
				//lochubinfo.insert(std::map<CString, PHUBINFO>::value_type(iter->second->sLocationpath, iter->second));
				lochubinfo[iter->second->sLocationpath] = iter->second;
			}
		}

		std::list<CString> locpathlist;
		if (FindLocalPaths(hub, lochubinfo, locpathlist))
		{
			if (!g_bUpdate)
			{
				//do checkini
				if (CheckDuplicateV2(locpathlist))
				{
					logIt(_T("[duplicate][label=%d]\n"), gStartLabel);
					dwRet = ERROR_DUP_NAME;
					return dwRet;
				}
			}
			using namespace Runtime::InteropServices;
			logIt(_T("[hubcount][hubindex=%d, hubcomment=%s]\n"), findsupporthubcount++, (const wchar_t*)(Marshal::StringToHGlobalUni(hub->sComment)).ToPointer());
			int nCount = 0;
			for each(auto lp in locpathlist)
			{
				hub->SetLocationPath(gcnew String(lp.GetString()));
				GetHubCalibration(lochubinfo, hub, startindex, nCount);
				logIt(_T("[count][count=%d]\n"), nCount);
				startindex += nCount;
			}
		}
		else
		{
			logIt(_T("[hubcount]Not find match hubs.\n"));
		}
	}
	return dwRet;
}

void GetHubInfoFromAllPlugHub()
{
	ENTER_FUNCTION();
	FreeAllUsbHubList();
	for (std::map<CString, CString>::iterator iter = ginstallHub.begin(); iter != ginstallHub.end(); iter++)
	{
		PHUBINFO pinf = new HUBINFO();
		pinf->sHubName = iter->first;
		pinf->sLocationpath = iter->second;
		HANDLE hHub = CreateFile(pinf->sHubName, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, NULL);
		if (hHub == INVALID_HANDLE_VALUE) continue;
		DWORD BytesReturned;
		USB_NODE_INFORMATION nodeInformation;
		ZeroMemory(&nodeInformation, sizeof(USB_NODE_INFORMATION));
		if (DeviceIoControl(hHub, IOCTL_USB_GET_NODE_INFORMATION, &nodeInformation, sizeof(USB_NODE_INFORMATION), &nodeInformation, sizeof(USB_NODE_INFORMATION), &BytesReturned, NULL))
		{
			pinf->portcount = nodeInformation.u.HubInformation.HubDescriptor.bNumberOfPorts;
		}
		else
		{
			PrintError(GetLastError(), _T(__FILE__), __LINE__);
		}
		PUSB_HUB_INFORMATION_EX hubInfoEx = NULL;
		Get_USB_HUB_INFORMATION_EX(hHub, &hubInfoEx);
		pinf->pHubInfoEx = hubInfoEx;

		CloseHandle(hHub);

		//allhubinfo.insert(std::map<CString, PHUBINFO> ::value_type(pinf->sHubName, pinf));
		allhubinfo[pinf->sHubName] = pinf;
	}
}

DWORD CalibrateHubPlugIn()
{
	ENTER_FUNCTION();
	DWORD dwRet = ERROR_SUCCESS;
	CConfigInfo^ info = gcnew CConfigInfo();
	dwRet = info->LoadConfigXml();
	if (dwRet != ERROR_SUCCESS) return dwRet;

	if (ginstallHub.size() == 0) return ERROR_NO_DATA_DETECTED;

	for (std::map<CString, CString>::iterator iter = ginstallHub.begin(); iter != ginstallHub.end(); iter++)
	{
		CString sHubName = iter->first;
		CString sLocationpath = iter->second;
		if (sLocationpath.IsEmpty())
		{
			int nTryCnt = 20;
			while ((dwRet=GetDeviceLocationPaths((TCHAR *)sHubName.GetString(), sLocationpath)) != ERROR_SUCCESS)
			{
				Sleep(2000);
				if (nTryCnt-- == 0) {
					logIt(_T("Get LocationPath path failed (%s)\n"), (TCHAR *)sHubName.GetString());
					break; 
				}
			}
			if (dwRet == ERROR_SUCCESS){
				iter->second = sLocationpath;
			}
			else break;
		}
	}

	if (dwRet != ERROR_SUCCESS) return dwRet;

	GetHubInfoFromAllPlugHub();
	//filter same hub interface
	std::set<CString> m_all;
	for (std::map<CString, PHUBINFO>::iterator iter = allhubinfo.begin(); iter != allhubinfo.end(); iter++) {
		auto result = m_all.insert(iter->first);
		if (!result.second) {
			logIt(_T("[duplicate][label=%d]\n"), 999);
			dwRet = ERROR_DUP_NAME;
			return dwRet;
		}
	}

	int startindex = gStartLabel - 1;

	int nCount = 0;//Plug in Only Support One Hub

	for each(auto hub in info->hubs)
	{
		String^ sID = GetHubIDsFromXML(hub);
		System::Diagnostics::Trace::WriteLine(sID);
		Regex^ pidvidregex = gcnew Regex(sID, RegexOptions::IgnoreCase | RegexOptions::Compiled);
		std::map<CString, PHUBINFO> lochubinfo;
		for (std::map<CString, PHUBINFO>::iterator iter = allhubinfo.begin(); iter != allhubinfo.end(); iter++)
		{
			Match^ m = pidvidregex->Match(gcnew String(iter->first.GetString()));
			if (m->Success)
			{
				//lochubinfo.insert(std::map<CString, PHUBINFO>::value_type(iter->second->sLocationpath, iter->second));
				lochubinfo[iter->second->sLocationpath] = iter->second;
			}
		}

		std::list<CString> locpathlist;
		if (FindLocalPaths(hub, lochubinfo, locpathlist))
		{
			if (!g_bUpdate)
			{
				//do checkini
				if (CheckDuplicateV2(locpathlist))
				{
					logIt(_T("[duplicate][label=%d]\n"), gStartLabel);
					dwRet = ERROR_DUP_NAME;
					return dwRet;
				}
			}

			for each(auto lp in locpathlist)
			{
				hub->SetLocationPath(gcnew String(lp.GetString()));
				GetHubCalibration(lochubinfo, hub, startindex, nCount);
				logIt(_T("[count][count=%d]\n"), nCount);
				startindex += nCount;
			}
		}
	}

	return dwRet;
}

String^ GetHubSupportFromXML(CHub^ hub)
{
	ENTER_FUNCTION();
	String^ sID = String::Format("VID_{0}&PID_{1}#", hub->vid, hub->pid);
	if (hub->IsSupportUsb30()) sID = String::Format("{1}", sID, hub->usb30pidvid);
	
	return sID;
}

DWORD CalibrateConfig2SupportList()
{
	DWORD dwRet = ERROR_SUCCESS;
	ENTER_FUNCTION();
	CConfigInfo^ info = gcnew CConfigInfo();
	dwRet = info->LoadConfigXml();
	if (dwRet != ERROR_SUCCESS) return dwRet;
	for each(auto hub in info->hubs)
	{
		String^ sID = GetHubIDsFromXML(hub);
		array<wchar_t>^ sp = { '|' };
		array<String^>^ aIDs = sID->Split(sp, StringSplitOptions::RemoveEmptyEntries);
		for each(String^ s in aIDs)
		{
			s = s->ToUpper();
			s = s->Replace('#', '\\');
			using namespace Runtime::InteropServices;
			gsupportlisthub.insert((const wchar_t*)(Marshal::StringToHGlobalUni(s)).ToPointer());
		}
	}
	return dwRet;
}