
#include "stdafx.h"
#include <dbt.h>
#include <Usbiodef.h>
#include <time.h>
#include <sys/stat.h>
#include <Shlwapi.h>
#include <string>

#include <SetupAPI.h>
#include <Devpropdef.h>
#include <Devpkey.h>

#include <Cfg.h>
#include <Cfgmgr32.h>

#include "cheader.h"

#include <map>

#pragma comment(lib, "Shlwapi.lib")
#pragma comment(lib, "setupapi.lib")

HWND		hWindowsMessage;
HDEVNOTIFY	hDevNotify;
HANDLE		threadWindowsMessage;
HANDLE	    hEventHubPlugin = CreateEvent(NULL, FALSE, FALSE, NULL);

std::map<CString, CString> ginstallHub;

LRESULT on_WM_CREAT(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	DEV_BROADCAST_DEVICEINTERFACE NotificationFilter;
	ZeroMemory(&NotificationFilter, sizeof(NotificationFilter));
	NotificationFilter.dbcc_size = sizeof(DEV_BROADCAST_DEVICEINTERFACE);
	NotificationFilter.dbcc_devicetype = DBT_DEVTYP_DEVICEINTERFACE;
	NotificationFilter.dbcc_classguid = GUID_DEVINTERFACE_USB_HUB;
	hDevNotify = RegisterDeviceNotification(hwnd, &NotificationFilter, DEVICE_NOTIFY_WINDOW_HANDLE);
	return 0;
}

LRESULT on_WM_DESTROY(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	if (hDevNotify != NULL)
	{
		UnregisterDeviceNotification(hDevNotify);
	}
	PostQuitMessage(0);
	return 0;
}

int GetDeviceLocationPaths(TCHAR *sName, CString &sLocpath)
{
	ENTER_FUNCTION();
	int nRet = ERROR_SUCCESS;
	HDEVINFO hDevInfo = SetupDiCreateDeviceInfoList(NULL, NULL);
	if (hDevInfo != INVALID_HANDLE_VALUE)
	{
		DWORD sz = 0;
		SP_DEVICE_INTERFACE_DATA devIntData;
		devIntData.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);
		logIt(sName);
		if (SetupDiOpenDeviceInterface(hDevInfo, sName, 0, &devIntData))
		{
			DEVPROPTYPE type;
			BYTE b[2048];
			ZeroMemory(b, sizeof(b));
			sz = 0;

			SP_DEVINFO_DATA devInfoData = { 0 };
			devInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
			SetupDiGetDeviceInterfaceDetail(hDevInfo, &devIntData, NULL, 0, &sz, &devInfoData);
			if (GetLastError() == ERROR_INSUFFICIENT_BUFFER)
			{
				if (SetupDiGetDeviceProperty(hDevInfo, &devInfoData, &DEVPKEY_Device_LocationPaths, &type, b, 2048, &sz, 0))
				{
					if (type == DEVPROP_TYPE_STRING_LIST)
					{
						TCHAR *instanceid = (TCHAR *)b;
						sLocpath = instanceid;
						DWORD dwoffset = 0;
						BOOL bFind = FALSE;
						while (instanceid != NULL && _tcslen(instanceid)>0)
						{
							logIt(_T("%s\n"),instanceid);

							dwoffset += _tcslen(instanceid) + 1;
							instanceid = (TCHAR *)b + dwoffset;
						}

					}
				}
				else
				{
					nRet = GetLastError();
					PrintError(nRet, _T(__FILE__), __LINE__);
				}
			}
			else
			{
				nRet = GetLastError();
				PrintError(nRet, _T(__FILE__), __LINE__);
			}
		}
		else
		{
			nRet = GetLastError();
			PrintError(nRet, _T(__FILE__), __LINE__);
		}

		SetupDiDestroyDeviceInfoList(hDevInfo);
	}
	else
	{
		nRet = GetLastError();
		PrintError(nRet, _T(__FILE__), __LINE__);
	}
	logIt(_T("GetDeviceLocationPaths ret = %d\n"), nRet);
	return nRet;
}

LRESULT on_WM_DEVICECHANGE(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	ENTER_FUNCTION();
	if (msg == WM_DEVICECHANGE)
	{
		if (wParam == DBT_DEVICEARRIVAL)
		{
			PDEV_BROADCAST_DEVICEINTERFACE info = (PDEV_BROADCAST_DEVICEINTERFACE)lParam;
			if (info->dbcc_devicetype == DBT_DEVTYP_DEVICEINTERFACE)
			{
				logIt(_T("USBHUBCHANGE: (DBT_DEVICEARRIVAL)%x %s\n"), wParam, info->dbcc_name);
				CString sLoc;
				GetDeviceLocationPaths(info->dbcc_name, sLoc);

				ginstallHub[info->dbcc_name] = sLoc;
				SetEvent(hEventHubPlugin);
			}
		}
		else if (wParam == DBT_DEVICEREMOVECOMPLETE)
		{
			PDEV_BROADCAST_DEVICEINTERFACE info = (PDEV_BROADCAST_DEVICEINTERFACE)lParam;
			if (info->dbcc_devicetype == DBT_DEVTYP_DEVICEINTERFACE)
			{
				logIt(_T("USBHUBCHANGE: (DBT_DEVICEREMOVECOMPLETE)%x %s\n"), wParam, info->dbcc_name);
				if (ginstallHub.find(info->dbcc_name) != ginstallHub.end())
				{
					ginstallHub.erase(info->dbcc_name);
				}
			}
		}
	}
	return 0;
}

LRESULT CALLBACK win_proc(HWND hwnd,
	UINT uMsg,
	WPARAM wParam,
	LPARAM lParam
	)
{
	LRESULT ret = 0;
	switch (uMsg)
	{
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_CREATE:
		ret = on_WM_CREAT(hwnd, uMsg, wParam, lParam);
		break;
	case WM_DESTROY:
		ret = on_WM_DESTROY(hwnd, uMsg, wParam, lParam);
		break;
	case WM_DEVICECHANGE:
		ret = on_WM_DEVICECHANGE(hwnd, uMsg, wParam, lParam);
		break;
	default:
		ret = DefWindowProc(hwnd, uMsg, wParam, lParam);
		break;
	}

	return ret;
}

DWORD WINAPI  win_thread(LPVOID lpVoid)
{
	WNDCLASS cls;
	ZeroMemory(&cls, sizeof(WNDCLASS));
	cls.lpfnWndProc = win_proc;
	cls.lpszClassName = _T("fdUsbHubMonitor");
	cls.hInstance = GetModuleHandle(NULL);
	if (RegisterClass(&cls))
	{
		HWND hwnd = CreateWindowEx(0, cls.lpszClassName, _T("fdUsbHubMonitor"), 0xcf0000, 0, 0, 0, 0, 0, 0, GetModuleHandle(NULL), 0);
		if (hwnd != NULL)
		{
			hWindowsMessage = hwnd;
			MSG msg;
			BOOL bRet;
			while ((bRet = GetMessage(&msg, NULL, 0, 0)) != 0)
			{
				if (bRet == -1)
				{
					// handle the error and possibly exit
				}
				else
				{
					TranslateMessage(&msg);
					DispatchMessage(&msg);
				}
			}

		}
	}
	return 0;
}

void init_win_message()
{
	threadWindowsMessage = CreateThread(NULL, 0, win_thread, NULL, 0, NULL);
}

void uninit_win_message()
{
	ENTER_FUNCTION();
	if (hEventHubPlugin){
		CloseHandle(hEventHubPlugin);
		hEventHubPlugin = NULL;
	}
	if (hWindowsMessage != NULL && IsWindow(hWindowsMessage))
	{
		SendMessage(hWindowsMessage, WM_CLOSE, 0, 0);
		hWindowsMessage = NULL;
	}
	if (threadWindowsMessage != NULL)
	{
		WaitForSingleObject(threadWindowsMessage, 1000);
		threadWindowsMessage = NULL;
	}
}

