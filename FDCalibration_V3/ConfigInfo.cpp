#include "stdafx.h"
#include "ConfigInfo.h"
using namespace System::Xml;
using namespace System::IO;

CConfigInfo::CConfigInfo()
{
}

DWORD CConfigInfo::LoadConfigXml()
{
	DWORD dwRet = ERROR_SUCCESS;
	String^ sPath = System::IO::Path::Combine(AppDomain::CurrentDomain->BaseDirectory, "fdcalibrationv3config.xml");
	if (!File::Exists(sPath))
	{
		return ERROR_FILE_INVALID;
	}
	XmlDocument^ doc = gcnew XmlDocument();
	try
	{
		doc->Load(sPath);
		XmlNodeList^ nodes = doc->DocumentElement->SelectNodes("/hubs/hub");
		for each(XmlNode^ node in nodes)
		{
			if (node->NodeType == XmlNodeType::Comment) continue;
			CHub^ hub = gcnew CHub();
			ParseHub(hub, node);
			hubs->Add(hub);
		}
	}
	catch (...)
	{
		dwRet = ERROR_XML_PARSE_ERROR;
	}
	return dwRet;
}

void CConfigInfo::ParseHub(CHub^ hub, XmlNode^ node)
{
	hub->pid = node->Attributes["pid"]->Value;
	hub->vid = node->Attributes["vid"]->Value;
	hub->usb30pidvid = node->Attributes["usb30"] == nullptr ? "" : node->Attributes["usb30"]->Value;
	hub->portcnt = Convert::ToInt16(node->Attributes["portcnt"]->Value);
	hub->sComment = node->Attributes["comment"] == nullptr ? "" : node->Attributes["comment"]->Value;
	hub->autocnt = node->Attributes["autocnt"] == nullptr || String::IsNullOrWhiteSpace(node->Attributes["autocnt"]->Value) ? 0 : Convert::ToInt32(node->Attributes["autocnt"]->Value);
	if (node->HasChildNodes)
	{
		XmlNodeList^ pipes = node->ChildNodes;
		for each(XmlNode^ pipe in pipes)
		{
			if (pipe->NodeType == XmlNodeType::Comment) continue;
			int pipindex = Convert::ToInt16(pipe->Attributes["index"]->Value);
			if (pipe->Attributes["label1"] != nullptr && !String::IsNullOrWhiteSpace(pipe->Attributes["label1"]->Value)) {
				int label = Convert::ToInt16(pipe->Attributes["label1"]->Value);
				hub->hubportsLabel1->Add(pipindex, label);
			}
			if (pipe->Attributes["label"] != nullptr)
			{
				if (String::IsNullOrWhiteSpace(pipe->Attributes["label"]->Value)) {
					int nl = 0;
					hub->hubports->Add(pipindex, nl);
				}
				else {
					int label = Convert::ToInt16(pipe->Attributes["label"]->Value);
					hub->hubports->Add(pipindex, label);
				}
			}
			else
			{
				if (pipe->HasChildNodes)
				{
					CHub^ hubchild = gcnew CHub();
					ParseHub(hubchild, pipe->FirstChild);
					hubchild->autocnt = hub->autocnt;
					hub->hubports->Add(pipindex, hubchild);
				}
			}
		}
	}
}