#include "stdafx.h"
#include "RunExe.h"
#include "cheader.h"

using namespace System::IO;

CRunExe::CRunExe()
{
	stdoutSB = gcnew List<String^>();
	stderrSB = gcnew List<String^>();
	stdoutEvt = gcnew AutoResetEvent(false);
	stderrEvt = gcnew AutoResetEvent(false);
}
void CRunExe::StdoutReceived_Handler(Object^ sender, DataReceivedEventArgs^ e)
{
	if (e->Data == nullptr)
	{
		stdoutEvt->Set();
	}
	else
	{
		Trace::WriteLine(String::Format("[stdout]: {0}", e->Data));
		stdoutSB->Add(e->Data);
	}
}
void CRunExe::StderrReceived_Handler(Object^ sender, DataReceivedEventArgs^ e)
{
	if (e->Data == nullptr)
	{
		stderrEvt->Set();
	}
	else
	{
		Trace::WriteLine(String::Format("[stderr]: {0}", e->Data));
		stderrSB->Add(e->Data);
	}
}

List<String^>^ CRunExe::getExeStdOut()
{
	return stdoutSB;
}

List<String^>^ CRunExe::getExeStdError()
{
	return stderrSB;
}

int CRunExe::startRunExe(String^ exepath, String^ sparam, int timeout, Dictionary<String^, String^>^ env)
{
	int retI = ERROR_TIMEOUT;
	Trace::WriteLine(String::Format("startRunExe: ++ exe: {0} param: {1} timeout: {2}", exepath, sparam, timeout));
	stdoutSB->Clear();
	stderrSB->Clear();
	stdoutEvt->Reset();
	stderrEvt->Reset();
	Process^ p = gcnew Process();
	p->StartInfo->FileName = exepath;
	p->StartInfo->Arguments = sparam;
	p->StartInfo->WorkingDirectory = System::IO::Path::GetDirectoryName(exepath);
	p->StartInfo->UseShellExecute = false;
	p->StartInfo->CreateNoWindow = true;
	p->StartInfo->RedirectStandardError = true;
	p->StartInfo->RedirectStandardOutput = true;

	if (env != nullptr && env->Count > 0) {
		for each(KeyValuePair<String^, String^>^ kv in env) {
			if (!p->StartInfo->EnvironmentVariables->ContainsKey(kv->Key)) {
				Trace::WriteLine(String::Format("Environment Add {0}={1}", kv->Key, kv->Value));
				p->StartInfo->EnvironmentVariables->Add(kv->Key, kv->Value);
			}
			else {
				Trace::WriteLine(String::Format("Environment Replace {0}={1}", kv->Key, kv->Value));
				p->StartInfo->EnvironmentVariables[kv->Key] = kv->Value;
			}
		}
	}

	p->OutputDataReceived += gcnew DataReceivedEventHandler(this, &CRunExe::StdoutReceived_Handler);
	p->ErrorDataReceived += gcnew DataReceivedEventHandler(this, &CRunExe::StderrReceived_Handler);
	p->Start();
	p->BeginErrorReadLine();
	p->BeginOutputReadLine();
	if (p->WaitForExit(timeout * 1000))
	{
		// process exit.
		retI = p->ExitCode;
		// wait for pipe complete.
		EventWaitHandle::WaitAll(gcnew array<AutoResetEvent^>{stdoutEvt, stderrEvt}, timeout);
	}
	else
	{
		// timeout
		p->Kill();
		retI = ERROR_TIMEOUT;
	}
	Trace::WriteLine(String::Format("startRunExe: ++ ret: {0} ", retI));
	return retI;
}
