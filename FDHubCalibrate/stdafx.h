// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include <Windows.h>
#include <tchar.h>

#include <InitGuid.h>
#include <cfgmgr32.h>
#include <setupapi.h>
#include <usbiodef.h>
#include <usbioctl.h>

#include <atlstr.h>

#include <Strsafe.h>
#include <Shlwapi.h>
#include <devioctl.h>
#include <Cfgmgr32.h>
#include <Devpkey.h>
#include <Devpropdef.h>


#pragma comment(lib,"Setupapi.lib")
#pragma comment(lib,"Cfgmgr32.lib")

#include <string>