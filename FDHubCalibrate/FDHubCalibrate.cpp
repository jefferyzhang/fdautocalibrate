// FDHubCalibrate.cpp : main project file.

#include "stdafx.h"
#include <cfgmgr32.h>
#include "FDHubCalibrate.h"
#include "UsbHub.h"
#include "Process.h"

using namespace System;
using namespace System::Configuration::Install;
using namespace System::IO;
using namespace System::Diagnostics;
using namespace System::Collections::Specialized;

TCHAR sIniFileName[MAX_PATH] = { 0 };
int  gStartLabel = 1;
int g_timeout = 60;
BOOL g_bUpdate = false;
BOOL g_5X4Mode = true;
int BASELABEL[] = {-1, 17, 13, 9, 1, 5 };
map<int, int> portlabels = { { 1, 1 }, { 2, 4 }, { 3, 2 }, {4, 3} };

int runExeChangeMode(LPCTSTR lpLoc, int Mode);

void logIt(
	String^ format,
	... cli::array<Object^>^ arg
	)
{
	String^ msg = String::Format(format, arg);
	System::Diagnostics::Trace::WriteLine(System::String::Format(L"[FDHubControl]: {0}", msg));
}


void Usage()
{
	Console::WriteLine("command Line:");
	Console::WriteLine("\t -inifile=   (full path ini file name. Must)");
	Console::WriteLine("\t -startlabel=   [modified for label. default is 1]");
	Console::WriteLine("\t -timeout=   [wait for device plug in, s, default 60s]");
	Console::WriteLine("\t -single   Only one futuredial support HUB connect PC. One Cable one hub");
	Console::WriteLine("\t -mode=5X4  or 1X20  Only futuredial designed HUB connect PC. ");
}

int main(cli::array<System::String^>^ args)
{
	logIt("{0} start: ++ version: {1}",
		Path::GetFileName(Process::GetCurrentProcess()->MainModule->FileName),
		Process::GetCurrentProcess()->MainModule->FileVersionInfo->FileVersion);

	InstallContext^ myInstallContext = gcnew InstallContext(nullptr, args);
	if (myInstallContext->IsParameterTrue("debug"))
	{
		logIt("Wait any key to dubug, Please attach...\n");
		System::Console::ReadKey();
	}
	if (myInstallContext->IsParameterTrue("help"))
	{
		Usage();
		return ERROR_SUCCESS;
	}
	g_bUpdate = myInstallContext->IsParameterTrue("update");
	if (myInstallContext->Parameters->ContainsKey("timeout"))
	{
		g_timeout = Convert::ToInt32(myInstallContext->Parameters["timeout"]);
	}

	TCHAR path[MAX_PATH] = { 0 };
	GetModuleFileName(NULL, path, MAX_PATH);
	PathRemoveFileSpec(path);
	_tcscpy_s(sIniFileName, path);
	PathAppend(sIniFileName, _T("calibration_new.ini"));

	if (myInstallContext->Parameters->ContainsKey("inifile"))
	{
		String^ sFN = myInstallContext->Parameters["inifile"];
		using namespace Runtime::InteropServices;
		const wchar_t* lpIniFileName =
			(const wchar_t*)(Marshal::StringToHGlobalUni(sFN)).ToPointer();
		ZeroMemory(sIniFileName, MAX_PATH);
		_sntprintf_s(sIniFileName, MAX_PATH, lpIniFileName);
	}

	if (myInstallContext->Parameters->ContainsKey("startlabel"))
	{
		extern int gStartLabel;
		gStartLabel = Convert::ToInt32(myInstallContext->Parameters["startlabel"]);
	}

	if (myInstallContext->Parameters->ContainsKey("mode"))
	{
		g_5X4Mode = String::Compare("5X4", myInstallContext->Parameters["mode"], true)==0;
	}

	if (gStartLabel == 1 && !g_bUpdate){
		WritePrivateProfileString(_T("ControlHub"), NULL, NULL, sIniFileName);
	}

	if (myInstallContext->Parameters->ContainsKey("single"))
	{
		CUsbHub usbhub;
		usbhub.ListAllUsbHub();
		int ret = usbhub.CalibrationHubs();
		usbhub.FreeAllUsbHubList();
		return ret;
	}

	extern HANDLE hEventHubPlugin;
	extern HANDLE hEventHubChange;
	init_win_message();
	int nTimeout = 5;
	DWORD dwRet = WaitForSingleObject(hEventHubPlugin, g_timeout * 1000);
	if (dwRet == WAIT_OBJECT_0)
	{
		Sleep(5000);
		while (IsDeviceInstallInProgress()) {
			Sleep(5000);
		}
		while (WaitForSingleObject(hEventHubChange, nTimeout * 1000) == WAIT_TIMEOUT)
		{
			break;
		}
		CUsbHub usbhub;
		if (g_5X4Mode){
			//Change Hub to 5X4
			//Console::WriteLine("\t -mode=[single|multi] [-ctlpc=] [-hublocation=]");
			//-mode=multi -hublocation=PCIROOT(0)#PCI(1400)#USBROOT(0)#USB(9)#USB(3)
			//runExeChangeMode(LPCTSTR lpLoc, 2)
			extern std::map<CString, CString, cmpIngnoreCase> ginstallHub;
			CString shubloc;
			int iretry = 10;
			do{
				for each (auto item in ginstallHub)
				{
					logIt(_T("find hub %s\n"), item.first);
					if (usbhub.CheckFDSmartHub(item.first, USB5806_20))
					{
						logIt(_T("find main hub %s==\n"), item.second);
						shubloc = item.second;
						if (shubloc.IsEmpty()) {
							GetDeviceLocationPaths((TCHAR *)item.first.GetString(), item.second);
							shubloc = item.second;
						}
						break;
					}
				}
				Sleep(5000);
			} while (shubloc.IsEmpty()&& iretry-->0);
			
			if (shubloc.IsEmpty())
			{
				logIt(_T("Not find main hub\n"));
				return ERROR_NOT_FOUND;
			}

			if (shubloc.GetLength() > 0)
			{
				runExeChangeMode(shubloc, 2);
			}		
		}
		Sleep(15000);
		nTimeout = 5;
		while (IsDeviceInstallInProgress()) {
			Sleep(5000);
		}
		while (WaitForSingleObject(hEventHubChange, nTimeout * 1000) == WAIT_TIMEOUT)
		{
			break;
		}
		usbhub.ListUsbHubChange();
		int ret = usbhub.CalibrationHubs();
		usbhub.FreeAllUsbHubList();
		return ret;
	}
	else if (dwRet == WAIT_TIMEOUT)
	{
		logIt("[timeout]can not detect hub plug in\n");
		uninit_win_message();
		return ERROR_TIMEOUT;
	}

	uninit_win_message();

    return 0;
}

int runExeChangeMode(LPCTSTR lpLoc, int Mode)
{
	String^ exe = System::Environment::ExpandEnvironmentVariables("%APSTHOME%\\FDHubControl.exe");
	if (!File::Exists(exe))
	{
		exe = Path::Combine(AppDomain::CurrentDomain->BaseDirectory, "FDHubControl.exe");
	}
	TCHAR args[MAX_PATH] = { 0 };
	_stprintf_s(args, _T("-mode=%ws -hublocation=%ws"), Mode == 1 ? _T("single") : _T("multi"), lpLoc);
	CProcess p;
	TCHAR exePath[MAX_PATH] = { 0 };
	using namespace Runtime::InteropServices;
	const wchar_t* lpexe =
		(const wchar_t*)(Marshal::StringToHGlobalUni(exe)).ToPointer();
	_sntprintf_s(exePath, MAX_PATH, lpexe);
	p.RunExe(exePath, args, 10000);
	return 1;
}