#pragma once
#include <map>

struct cmpIngnoreCase {
	bool operator()(const CString& a, const CString& b) const {
		return a.CompareNoCase(b) < 0;
	}
};



void logIt(TCHAR* fmt, ...);
void PrintError(DWORD dwError, __in PCTSTR sFile, ULONG   Line);

#define ENTER_FUNCTION()	logIt(_T("%S ++\n"), __FUNCTION__)
#define EXIT_FUNCTION()		logIt(_T("%S --\n"), __FUNCTION__)
#define EXIT_FUNCTRET(ret)		logIt(_T("%S -- return=%d\n"), __FUNCTION__, ret)
#define PRINT_ERR(error)	PrintError(error, _T(__FILE__), __LINE__)

void init_win_message();
void uninit_win_message();

#define VID_HUB		0x0424
#define PID_5806	0x2806
#define PID_5734	0x2734
#define PID_5806U3	0x5806
#define PID_5734U3	0x5734


#define USB5806		0x0FD0 //bcdDevice
#define USB5734_1	0x0FD1 //bcdDevice
#define USB5734_2	0x0FD2 //bcdDevice
#define USB5734_3	0x0FD3 //bcdDevice
#define USB5734_4	0x0FD4 //bcdDevice
#define USB5734_5	0x0FD5 //bcdDevice

extern int BASELABEL[];
#define BCDTOLABEL(n, hub)	do{if(n<USB5806||n>0x0FD5) {hub=-1;break;}hub=BASELABEL[n-USB5806];}while(0);
#define BCDTOINDEX(n, hubindex)	do{if(n<USB5806||n>0x0FD5) {hubindex=-1;break;}hubindex=n-USB5806;}while(0);

extern std::map<int, int> portlabels;

int GetDeviceLocationPaths(TCHAR *sName, CString &sLocpath);
BOOL IsDeviceInstallInProgress(VOID);
