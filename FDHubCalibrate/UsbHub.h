#pragma once

#include <string>
#include <map>
#include <Usbioctl.h>
#include <regex>
#include "FDHubCalibrate.h"

using namespace std;

enum HUBSYLE
{
	USB5806_20 = 1,
	USB5806_30 = 2,
	USB5734_20 = 4,
	USB5734_30 = 8
};

typedef struct __tagHubInfo{
	CString sHubName;
	int portcount;
	CString sLocationpath;
	BOOL isUsed;
	PUSB_NODE_CONNECTION_INFORMATION_EX    pConnectionInfoEx;
	PUSB_PORT_CONNECTOR_PROPERTIES         pPortConnectorProps;
	PUSB_HUB_CAPABILITIES_EX			   pHubCapabilityEx;
	CString sPortChain;

	CString CalcChain(map<wstring, int> mLindexs)
	{
		sPortChain.Empty();
		if (sLocationpath.GetLength() == 0 || mLindexs.size() == 0)
		{
			return sPortChain;
		}
		int index = 0;

		for each(auto dd in mLindexs)
		{
			if (sLocationpath.FindOneOf(dd.first.c_str()) == 0){
				index = dd.second;
				break;
			}
		}
		if (index > 0) {
			sPortChain = std::to_wstring(index).c_str();
		}

		index = sLocationpath.FindOneOf(_T("#USB("));
		if (index < 1) {
			logIt(_T("Location paths format error.\n"));
		}
		else{
			CString usbloc = sLocationpath.Mid(index);
			usbloc.Replace(_T("#USB"), _T("-"));
			usbloc.Replace(_T("("), _T(""));
			usbloc.Replace(_T(")"), _T(""));
			logIt((TCHAR *)usbloc.GetString());
			sPortChain += usbloc;
		}

		return sPortChain;
	}

	__tagHubInfo()
	{
		portcount = 0;
		isUsed = false;
		pConnectionInfoEx = NULL;
		pPortConnectorProps = NULL;
		pHubCapabilityEx = NULL;
	}
	~__tagHubInfo()
	{
		if (pConnectionInfoEx != NULL)
		{
			free(pConnectionInfoEx);
		}
		if (pPortConnectorProps != NULL)
		{
			free(pPortConnectorProps);
		}
		if (pHubCapabilityEx != NULL)
		{
			free(pHubCapabilityEx);
		}
	}
	BOOL IsRootHub()
	{
		BOOL bRet = pHubCapabilityEx == NULL ? FALSE : pHubCapabilityEx->CapabilityFlags.HubIsRoot;
		return bRet;
	}

	int GetHubIndexFromLocPaths()
	{
		int nPort = 0;
		try {
			std::wregex reg(L".*?#USB\\((\\d+)\\)$");
			std::wsmatch res;
			std::wstring ss = (TCHAR *)sLocationpath.GetString();
			if (std::regex_search(ss, res, reg) && res.size() > 1)
			{
				nPort = _tstoi(res.str(1).c_str());
			}
		}
		catch (std::regex_error& ) {
			// Syntax error in the regular expression
		}
		return nPort;
	}
}HUBINFO, *PHUBINFO;


class CUsbHub
{
public:
	CUsbHub();
	~CUsbHub();


	wstring GetIndexFromLocation(wstring sloc);
	int GetLocationPathRootHub(TCHAR *HubName, int ndex);
	PUSB_NODE_CONNECTION_INFORMATION_EX   GetHubInfo(TCHAR *HubName);
	PUSB_NODE_CONNECTION_INFORMATION_EX   GetHubConnectionInfo(TCHAR *HubName, int hubport);
	void FreeAllUsbHubList();
	DWORD ListAllUsbHub();
	DWORD ListUsbHubChange();
	int CalibrationHubs();
	DWORD Get_USB_PORT_CONNECTOR_PROPERTIES(HANDLE hHubDevice, int hubport, __out PUSB_PORT_CONNECTOR_PROPERTIES *ppPortConnectorProps);
	int GetHubPeerHub(TCHAR *HubName, TCHAR CompanionHub[MAX_PATH], int &CompanionPort);
	BOOL CheckFDSmartHub(CString s, int checkstyle);
private:
	VOID	EnumerateHostControllers();
	TCHAR*  GetRootHubName(
		HANDLE HostController
		);

	DWORD Get_USB_HUB_CAPABILITIES_EX(HANDLE hHubDevice, __out PUSB_HUB_CAPABILITIES_EX *hubCapabilityEx);
	int WriteCalibration(CString sHub2, CString sLocpath2, CString sHub3, CString sLocpath3, int hub, int hubindex);

	map<wstring, int> mLocatindexs;
	std::map<CString, PHUBINFO, cmpIngnoreCase> allhubinfo; //HubName ==> HubInfo
};

