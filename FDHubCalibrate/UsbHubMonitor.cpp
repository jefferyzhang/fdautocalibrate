
#include "stdafx.h"
#include <dbt.h>
#include <Usbiodef.h>
#include <time.h>
#include <sys/stat.h>
#include <Shlwapi.h>
#include <string>

#include <SetupAPI.h>
#include <Devpropdef.h>

#include <Cfg.h>
#include <Cfgmgr32.h>

#include "FDHubCalibrate.h"

#include <map>
#include <regex>

#pragma comment(lib, "Shlwapi.lib")
#pragma comment(lib, "setupapi.lib")

HWND		hWindowsMessage;
HDEVNOTIFY	hDevNotify;
HANDLE		threadWindowsMessage;
HANDLE	    hEventHubPlugin = CreateEvent(NULL, TRUE, FALSE, NULL);
HANDLE	    hEventHubChange = CreateEvent(NULL, TRUE, FALSE, NULL);


std::map<CString, CString, cmpIngnoreCase> ginstallHub;

LRESULT on_WM_CREAT(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	DEV_BROADCAST_DEVICEINTERFACE NotificationFilter;
	ZeroMemory(&NotificationFilter, sizeof(NotificationFilter));
	NotificationFilter.dbcc_size = sizeof(DEV_BROADCAST_DEVICEINTERFACE);
	NotificationFilter.dbcc_devicetype = DBT_DEVTYP_DEVICEINTERFACE;
	NotificationFilter.dbcc_classguid = GUID_DEVINTERFACE_USB_HUB;
	hDevNotify = RegisterDeviceNotification(hwnd, &NotificationFilter, DEVICE_NOTIFY_WINDOW_HANDLE);
	return 0;
}

LRESULT on_WM_DESTROY(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	if (hDevNotify != NULL)
	{
		UnregisterDeviceNotification(hDevNotify);
	}
	PostQuitMessage(0);
	return 0;
}

BOOL
IsDeviceInstallInProgress(VOID)
{
	HMODULE hModule;
	PROC pCMP_WaitNoPendingInstallEvents;

	hModule = GetModuleHandle(TEXT("setupapi.dll"));
	if (!hModule)
	{
		// Should never happen since we're linked to SetupAPI, but...
		return FALSE;
	}

	pCMP_WaitNoPendingInstallEvents =
		GetProcAddress(hModule,
			"CMP_WaitNoPendingInstallEvents");
	if (!pCMP_WaitNoPendingInstallEvents)
	{
		// We're running on a release of the operating system that doesn't supply this function.
		// Trust the operating system to suppress AutoRun when appropriate.
		return FALSE;
	}
	return (CM_WaitNoPendingInstallEvents(0) == WAIT_TIMEOUT);
}

int GetDeviceLocationPaths(TCHAR *sName, CString &sLocpath)
{
	ENTER_FUNCTION();
	int nRet = ERROR_SUCCESS;
	HDEVINFO hDevInfo = SetupDiCreateDeviceInfoList(NULL, NULL);
	if (hDevInfo != INVALID_HANDLE_VALUE)
	{
		DWORD sz = 0;
		SP_DEVICE_INTERFACE_DATA devIntData;
		devIntData.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);
		logIt(sName);
		if (SetupDiOpenDeviceInterface(hDevInfo, sName, 0, &devIntData))
		{
			DEVPROPTYPE type;
			BYTE b[2048];
			ZeroMemory(b, sizeof(b));
			sz = 0;

			SP_DEVINFO_DATA devInfoData = { 0 };
			devInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
			SetupDiGetDeviceInterfaceDetail(hDevInfo, &devIntData, NULL, 0, &sz, &devInfoData);
			if (GetLastError() == ERROR_INSUFFICIENT_BUFFER)
			{
				if (SetupDiGetDeviceProperty(hDevInfo, &devInfoData, &DEVPKEY_Device_LocationPaths, &type, b, 2048, &sz, 0))
				{
					if (type == DEVPROP_TYPE_STRING_LIST)
					{
						TCHAR *instanceid = (TCHAR *)b;
						sLocpath = instanceid;
						DWORD dwoffset = 0;
						BOOL bFind = FALSE;
						while (instanceid != NULL && _tcslen(instanceid)>0)
						{
							logIt(_T("%s\n"), instanceid);

							dwoffset += _tcslen(instanceid) + 1;
							instanceid = (TCHAR *)b + dwoffset;
						}

					}
				}
				else
				{
					nRet = GetLastError();
					PrintError(nRet, _T(__FILE__), __LINE__);
				}
			}
			else
			{
				nRet = GetLastError();
				PrintError(nRet, _T(__FILE__), __LINE__);
			}
		}
		else
		{
			nRet = GetLastError();
			PrintError(nRet, _T(__FILE__), __LINE__);
		}

		SetupDiDestroyDeviceInfoList(hDevInfo);
	}
	else
	{
		nRet = GetLastError();
		PrintError(nRet, _T(__FILE__), __LINE__);
	}
	logIt(_T("GetDeviceLocationPaths ret = %d\n"), nRet);
	return nRet;
}

//DWORD WINAPI  ThreadGetLocation(LPVOID dbcc_name)
//{
//	CString sLoc;
//	CString dbccname = (TCHAR *)dbcc_name;
//	GetDeviceLocationPaths((TCHAR *)dbcc_name, sLoc);
//	ginstallHub.insert(std::map<CString, CString>::value_type(dbccname, sLoc));
//	return 0;
//}

BOOL isUsbHubDevice(LPCWSTR lpDeviceName)
{
	//BOOL ret = FALSE;
	//std::wregex reg(L"(?i)\\\\.*?#(vid_0424&pid_2806)#.*?#\\{f18a0e88-c30c-11d0-8815-00a0c906bed8\\}");
	//std::wsmatch res;
	//std::wstring s = lpDeviceName;
	//if (regex_match(s, res, reg))
	//{
	//	logIt(_T("isControlUsbHub match success."));
	//	ret = TRUE;
	//}
	//return ret;

	BOOL ret = FALSE;
	if (lpDeviceName != NULL)
	{
		WCHAR * device_name = _wcsdup(lpDeviceName);
		if (device_name)
		{
			_wcsupr(device_name);
			TCHAR pidvid[MAX_PATH] = { 0 };
			_stprintf_s(pidvid, _T("#VID_%04X&PID_%04X#"), VID_HUB, PID_5806);
			if (wcsstr(device_name, pidvid) != NULL)
				ret = TRUE;
			free(device_name);
		}
	}
	return ret;
}

LRESULT on_WM_DEVICECHANGE(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	ENTER_FUNCTION();
	if (msg == WM_DEVICECHANGE)
	{
		if (wParam == DBT_DEVICEARRIVAL)
		{
			PDEV_BROADCAST_DEVICEINTERFACE info = (PDEV_BROADCAST_DEVICEINTERFACE)lParam;
			if (info->dbcc_devicetype == DBT_DEVTYP_DEVICEINTERFACE)
			{
				logIt(_T("USBHUBCHANGE: %x %ws\n"), wParam, info->dbcc_name);
				if (isUsbHubDevice(info->dbcc_name))logIt(_T("found.\n"));
				CString sLoc;
				GetDeviceLocationPaths(info->dbcc_name, sLoc);
				if (ginstallHub.find(info->dbcc_name) != ginstallHub.end())
				{
					ginstallHub.erase(info->dbcc_name);
				}
				ginstallHub.insert(std::map<CString, CString>::value_type(info->dbcc_name, sLoc));
				SetEvent(hEventHubPlugin);
			}
		}
		else if (wParam == DBT_DEVICEREMOVECOMPLETE)
		{
			PDEV_BROADCAST_DEVICEINTERFACE info = (PDEV_BROADCAST_DEVICEINTERFACE)lParam;
			if (info->dbcc_devicetype == DBT_DEVTYP_DEVICEINTERFACE)
			{
				logIt(_T("USBHUBCHANGE: %x %ws\n"), wParam, info->dbcc_name);
				//GetDeviceLocationPaths(info->dbcc_name);
				if (ginstallHub.find(info->dbcc_name) != ginstallHub.end())
				{
					ginstallHub.erase(info->dbcc_name);
				}
			}
		}
		SetEvent(hEventHubChange);
	}
	return 0;
}

LRESULT CALLBACK win_proc(HWND hwnd,
	UINT uMsg,
	WPARAM wParam,
	LPARAM lParam
	)
{
	LRESULT ret = 0;
	switch (uMsg)
	{
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_CREATE:
		ret = on_WM_CREAT(hwnd, uMsg, wParam, lParam);
		break;
	case WM_DESTROY:
		ret = on_WM_DESTROY(hwnd, uMsg, wParam, lParam);
		break;
	case WM_DEVICECHANGE:
		ret = on_WM_DEVICECHANGE(hwnd, uMsg, wParam, lParam);
		break;
	default:
		ret = DefWindowProc(hwnd, uMsg, wParam, lParam);
		break;
	}

	return ret;
}

DWORD WINAPI  win_thread(LPVOID lpVoid)
{
	WNDCLASS cls;
	ZeroMemory(&cls, sizeof(WNDCLASS));
	cls.lpfnWndProc = win_proc;
	cls.lpszClassName = _T("fdUsbHubMonitor");
	cls.hInstance = GetModuleHandle(NULL);
	if (RegisterClass(&cls))
	{
		HWND hwnd = CreateWindowEx(0, cls.lpszClassName, _T("fdUsbHubMonitor"), 0xcf0000, 0, 0, 0, 0, 0, 0, GetModuleHandle(NULL), 0);
		if (hwnd != NULL)
		{
			hWindowsMessage = hwnd;
			MSG msg;
			BOOL bRet;
			while ((bRet = GetMessage(&msg, NULL, 0, 0)) != 0)
			{
				if (bRet == -1)
				{
					// handle the error and possibly exit
				}
				else
				{
					TranslateMessage(&msg);
					DispatchMessage(&msg);
				}
			}

		}
	}
	return 0;
}

void init_win_message()
{
	threadWindowsMessage = CreateThread(NULL, 0, win_thread, NULL, 0, NULL);
}

void uninit_win_message()
{
	if (hEventHubPlugin){
		CloseHandle(hEventHubPlugin);
		hEventHubPlugin = NULL;
	}
	if (hEventHubChange) {
		CloseHandle(hEventHubChange);
		hEventHubChange = NULL;
	}
	if (hWindowsMessage != NULL && IsWindow(hWindowsMessage))
	{
		SendMessage(hWindowsMessage, WM_CLOSE, 0, 0);
		hWindowsMessage = NULL;
	}
	if (threadWindowsMessage != NULL)
	{
		WaitForSingleObject(threadWindowsMessage, 1000);
		threadWindowsMessage = NULL;
	}
}

