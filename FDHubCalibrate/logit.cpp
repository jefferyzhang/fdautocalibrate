#include "stdafx.h"
#include "FDHubCalibrate.h"
#include <atlstr.h>

void logIt(TCHAR* fmt, ...)
{
	va_list args;

	CString sLog;
	va_start(args, fmt);
	sLog.FormatV(fmt, args);
	va_end(args);
	CString sLogpid;
	sLogpid.Format(_T("[SMCALIBRATION]%s"), sLog);
	//_tprintf(sLog);
	//fflush(stdout);
	OutputDebugString(sLogpid);
}

void PrintError(DWORD dwError, __in PCTSTR sFile, ULONG   Line)
{
	LPVOID lpMsgBuf;
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM,
		NULL,
		dwError,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&lpMsgBuf,
		0, NULL);
	logIt(_T("Error: %s=>File:%s=>Line:%d=>ErrorCode:%d\n"), lpMsgBuf, sFile, Line, dwError);
	LocalFree(lpMsgBuf);

}