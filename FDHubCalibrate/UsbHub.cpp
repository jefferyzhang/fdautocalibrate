#include "stdafx.h"
#include "UsbHub.h"
#include <Usbiodef.h>
#include <Usbioctl.h>

#include <VersionHelpers.h> 

#pragma comment(lib, "cfgmgr32.lib")
#pragma comment(lib, "setupapi.lib")


#include "FDHubCalibrate.h"

using namespace System;
using namespace System::IO;
using namespace System::Text::RegularExpressions;

int GetDeviceSymblInstanceIdEx(TCHAR *device_iid, LPWSTR lpReturnedString, DWORD nSize, GUID *guid);
int GetDeviceCommonSymbl(TCHAR *sName, PDEVPROPKEY pdevkey, PBYTE pdata, DWORD &nLen, DWORD &nType)
{
	int ret = ERROR_UNKNOWN_FEATURE;
	if (sName == NULL || _tcslen(sName) == 0)
		return ERROR_INVALID_PARAMETER;
	ENTER_FUNCTION();
	TCHAR symblName[1024] = { 0 };

	if ((_tcsncicmp(sName, _T("\\\\?\\"), 4) == 0) || (_tcsncicmp(sName, _T("\\\\.\\"), 4) == 0))
	{
		_stprintf_s(symblName, _T("%ws"), sName);
	}
	else
	{
		_stprintf_s(symblName, _T("\\\\?\\%ws"), sName);
	}

	HDEVINFO hDevInfo = SetupDiCreateDeviceInfoList(NULL, NULL);
	if (hDevInfo != INVALID_HANDLE_VALUE)
	{
		DWORD sz = 0;
		SP_DEVICE_INTERFACE_DATA devIntData;
		devIntData.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);
		logIt(symblName);
		if (SetupDiOpenDeviceInterface(hDevInfo, symblName, 0, &devIntData))
		{
			BYTE b[4096];
			ZeroMemory(b, sizeof(b));
			sz = 0;

			SP_DEVINFO_DATA devInfoData = { 0 };
			devInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
			SetupDiGetDeviceInterfaceDetail(hDevInfo, &devIntData, NULL, 0, &sz, &devInfoData);
			if (GetLastError() == ERROR_INSUFFICIENT_BUFFER)
			{
				if (SetupDiGetDeviceProperty(hDevInfo, &devInfoData, pdevkey, &nType, b, 2048, &sz, 0))
				{
					if (sz <= nLen){
						memcpy_s(pdata, nLen, b, sz);
						ret = ERROR_SUCCESS;
					}
					else
						ret = ERROR_INSUFFICIENT_BUFFER;
					nLen = sz;
				}
				else
				{
					ret = GetLastError();
					PrintError(ret, _T(__FILE__), __LINE__);
				}
			}
			else
			{
				ret = GetLastError();
				PrintError(ret, _T(__FILE__), __LINE__);
			}
		}
		else
		{
			ret = GetLastError();
			PrintError(ret, _T(__FILE__), __LINE__);
		}

		SetupDiDestroyDeviceInfoList(hDevInfo);
	}
	else
	{
		ret = GetLastError();
		PrintError(ret, _T(__FILE__), __LINE__);
	}

	return ret;
}

CUsbHub::CUsbHub()
{
}


CUsbHub::~CUsbHub()
{
}


wstring CUsbHub::GetIndexFromLocation(wstring sloc)
{
	wstring s;
	if (mLocatindexs.size() == 0)
	{
		EnumerateHostControllers();
	}
	if (mLocatindexs.size() == 0)
	{
		return s;
	}

	int index = 0;

	for each(auto dd in mLocatindexs)
	{
		if (sloc.find(dd.first) == 0){
			index = dd.second;
			break;
		}
	}
	if (index > 0) {
		s = std::to_wstring(index);
	}

	return s;
}


PCHAR WideStrToMultiStr(
	_In_reads_bytes_(cbWideStr) PWCHAR WideStr,
	_In_ size_t                   cbWideStr
	)
{
	ULONG  nBytes = 0;
	PCHAR  MultiStr = NULL;
	PWCHAR pWideStr = NULL;

	// Use local string to guarantee zero termination
	pWideStr = (PWCHAR)malloc((DWORD)cbWideStr + 1);
	if (NULL == pWideStr)
	{
		return NULL;
	}
	memset(pWideStr, 0, cbWideStr + 1);
	memcpy(pWideStr, WideStr, cbWideStr);

	// Get the length of the converted string
	//
	nBytes = WideCharToMultiByte(
		CP_ACP,
		WC_NO_BEST_FIT_CHARS,
		pWideStr,
		-1,
		NULL,
		0,
		NULL,
		NULL);

	if (nBytes == 0)
	{
		free(pWideStr);
		return NULL;
	}

	// Allocate space to hold the converted string
	//
	MultiStr = (PCHAR)malloc(nBytes);
	if (MultiStr == NULL)
	{
		free(pWideStr);
		return NULL;
	}

	// Convert the string
	//
	nBytes = WideCharToMultiByte(
		CP_ACP,
		WC_NO_BEST_FIT_CHARS,
		pWideStr,
		-1,
		MultiStr,
		nBytes,
		NULL,
		NULL);

	if (nBytes == 0)
	{
		free(MultiStr);
		free(pWideStr);
		return NULL;
	}

	free(pWideStr);
	return MultiStr;
}

DWORD CUsbHub::Get_USB_HUB_CAPABILITIES_EX(HANDLE hHubDevice, __out PUSB_HUB_CAPABILITIES_EX *hubCapabilityEx)
{
	ENTER_FUNCTION();
	DWORD dwRet(ERROR_SUCCESS);
	BOOL        success = FALSE;
	if (hHubDevice == INVALID_HANDLE_VALUE)
	{
		dwRet = ERROR_INVALID_PARAMETER;
		return dwRet;
	}
	ULONG nBytes = 0;

	*hubCapabilityEx = (PUSB_HUB_CAPABILITIES_EX)malloc(sizeof(USB_HUB_CAPABILITIES_EX));
	if (*hubCapabilityEx == NULL)
	{
		dwRet = ERROR_NOT_ENOUGH_MEMORY;
		PrintError(dwRet, _T(__FILE__), __LINE__);
		return dwRet;
	}
	//
	// Obtain Hub Capabilities
	//
	success = DeviceIoControl(hHubDevice,
		IOCTL_USB_GET_HUB_CAPABILITIES_EX,
		*hubCapabilityEx,
		sizeof(USB_HUB_CAPABILITIES_EX),
		*hubCapabilityEx,
		sizeof(USB_HUB_CAPABILITIES_EX),
		&nBytes,
		NULL);

	//
	// Fail gracefully
	//
	if (!success || nBytes < sizeof(USB_HUB_CAPABILITIES_EX))
	{
		*hubCapabilityEx = NULL;
		dwRet = GetLastError();
		PrintError(dwRet, _T(__FILE__), __LINE__);
	}

	return dwRet;
}

DWORD CUsbHub::Get_USB_PORT_CONNECTOR_PROPERTIES(HANDLE hHubDevice, int hubport, __out PUSB_PORT_CONNECTOR_PROPERTIES *ppPortConnectorProps)
{
	ENTER_FUNCTION();
	DWORD dwRet(ERROR_SUCCESS);
	BOOL        success = FALSE;
	USB_PORT_CONNECTOR_PROPERTIES          portConnectorProps;
	ZeroMemory(&portConnectorProps, sizeof(portConnectorProps));
	ULONG nBytes = 0;

	if (hHubDevice == INVALID_HANDLE_VALUE || hubport < 1)
	{
		dwRet = ERROR_INVALID_PARAMETER;
		return dwRet;
	}

	//
	// Now query USBHUB for the structures
	// for this port.  This will tell us if a device is attached to this
	// port, among other things.
	// The fault tolerate code is executed first.
	//

	portConnectorProps.ConnectionIndex = hubport;
	success = DeviceIoControl(hHubDevice,
		IOCTL_USB_GET_PORT_CONNECTOR_PROPERTIES,
		&portConnectorProps,
		sizeof(USB_PORT_CONNECTOR_PROPERTIES),
		&portConnectorProps,
		sizeof(USB_PORT_CONNECTOR_PROPERTIES),
		&nBytes,
		NULL);

	if (success && nBytes == sizeof(USB_PORT_CONNECTOR_PROPERTIES))
	{
		int nLen = portConnectorProps.ActualLength;
		PUSB_PORT_CONNECTOR_PROPERTIES pPortConnectorProps = (PUSB_PORT_CONNECTOR_PROPERTIES)malloc(nLen);
		ZeroMemory(pPortConnectorProps, nLen);
		if (pPortConnectorProps != NULL)
		{
			(pPortConnectorProps)->ConnectionIndex = hubport;
			nBytes = 0;
			success = DeviceIoControl(hHubDevice,
				IOCTL_USB_GET_PORT_CONNECTOR_PROPERTIES,
				pPortConnectorProps,
				nLen,
				pPortConnectorProps,
				nLen,
				&nBytes,
				NULL);

			if (!success || nBytes < portConnectorProps.ActualLength)
			{
				PrintError(GetLastError(), _T(__FILE__), __LINE__);
				free(pPortConnectorProps);
				pPortConnectorProps = NULL;
			}
			*ppPortConnectorProps = pPortConnectorProps;
		}
		else{
			logIt(_T("Malloc failed."));
			dwRet = ERROR_NOT_ENOUGH_MEMORY;
		}
	}
	else
	{
		dwRet = GetLastError();
		PrintError(dwRet, _T(__FILE__), __LINE__);
	}

	return dwRet;
}


TCHAR*  CUsbHub::GetRootHubName(
	HANDLE HostController
	)
{
	BOOL                success = 0;
	ULONG               nBytes = 0;
	USB_ROOT_HUB_NAME   rootHubName;
	PUSB_ROOT_HUB_NAME  rootHubNameW = NULL;
	TCHAR*               rootHubNameT = NULL;

	// Get the length of the name of the Root Hub attached to the
	// Host Controller
	//
	success = DeviceIoControl(HostController,
		IOCTL_USB_GET_ROOT_HUB_NAME,
		0,
		0,
		&rootHubName,
		sizeof(rootHubName),
		&nBytes,
		NULL);

	if (!success)
	{
		PRINT_ERR(GetLastError());
		goto GetRootHubNameError;
	}

	// Allocate space to hold the Root Hub name
	//
	nBytes = rootHubName.ActualLength;

	rootHubNameW = (PUSB_ROOT_HUB_NAME)malloc(nBytes);
	if (rootHubNameW == NULL)
	{
		PRINT_ERR(GetLastError());
		goto GetRootHubNameError;
	}

	// Get the name of the Root Hub attached to the Host Controller
	//
	success = DeviceIoControl(HostController,
		IOCTL_USB_GET_ROOT_HUB_NAME,
		NULL,
		0,
		rootHubNameW,
		nBytes,
		&nBytes,
		NULL);
	if (!success)
	{
		PRINT_ERR(GetLastError());
		goto GetRootHubNameError;
	}

	// Convert the Root Hub name
	//
	//rootHubNameA = WideStrToMultiStr(rootHubNameW->RootHubName, nBytes);
	rootHubNameT = (PWCHAR)malloc((DWORD)nBytes + 1);
	if (NULL == rootHubNameT)
	{
		return NULL;
	}
	memset(rootHubNameT, 0, nBytes + 1);
	memcpy(rootHubNameT, rootHubNameW->RootHubName, nBytes);

	// All done, free the uncoverted Root Hub name and return the
	// converted Root Hub name
	//
	free(rootHubNameW);

	return rootHubNameT;

GetRootHubNameError:
	// There was an error, free anything that was allocated
	//
	if (rootHubNameW != NULL)
	{
		free(rootHubNameW);
		rootHubNameW = NULL;
	}
	return NULL;
}

void CUsbHub::FreeAllUsbHubList()
{
	for (std::map<CString, PHUBINFO>::iterator iter = allhubinfo.begin(); iter != allhubinfo.end(); iter++)
	{
		if (iter->second != NULL)
		{
			PHUBINFO pHI = iter->second;
			delete pHI;
		}
	}
	allhubinfo.clear();
}

BOOL CUsbHub::CheckFDSmartHub(CString s, int checkstyle)
{
	String^ ids=""; 
	if ((checkstyle & USB5806_20) == USB5806_20)
	{
		ids += PID_5806.ToString("X4");
	}
	if ((checkstyle & USB5806_30) == USB5806_30)
	{
		if (ids->Length > 0) ids += "|";
		ids += PID_5806U3.ToString("X4");
	}
	if ((checkstyle & USB5734_20) == USB5734_20)
	{
		if (ids->Length > 0) ids += "|";
		ids += PID_5734.ToString("X4");
	}
	if ((checkstyle & USB5734_30) == USB5734_30)
	{
		if (ids->Length > 0) ids += "|";
		ids += PID_5734U3.ToString("X4");
	}
		

	//String^ sID = String::Format("usb#vid_{0}&pid_({1X4}|{2X4}|{3X4}|{4X4})#", VID_HUB, PID_5806, PID_5734, PID_5806U3, PID_5734U3);
	String^ sID = String::Format("usb#vid_{0}&pid_({1})#", VID_HUB.ToString("X4"), ids);
	Regex^ pidvidregex = gcnew Regex(sID, RegexOptions::IgnoreCase | RegexOptions::Compiled);
	Match^ m = pidvidregex->Match(gcnew String(s.GetString()));

	return m->Success;
}

DWORD CUsbHub::ListUsbHubChange()
{
	ENTER_FUNCTION();
	HANDLE                              hHub;
	HDEVINFO                            hDeviceInfo;
	SP_DEVICE_INTERFACE_DATA            _sdid;
	PSP_DEVICE_INTERFACE_DETAIL_DATA    _psdidd;
	DWORD dwLength;
	DWORD dwBytesX;
	DWORD dwRet = ERROR_SUCCESS;

	hDeviceInfo = SetupDiGetClassDevs(&GUID_DEVINTERFACE_USB_HUB, NULL, NULL, DIGCF_PRESENT | DIGCF_DEVICEINTERFACE);
	if (hDeviceInfo != INVALID_HANDLE_VALUE)
	{
		_sdid.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);
		for (int i = 0; SetupDiEnumDeviceInterfaces(hDeviceInfo, NULL, &GUID_DEVINTERFACE_USB_HUB, i, &_sdid); i++)
		{
			SetupDiGetDeviceInterfaceDetail(hDeviceInfo, &_sdid, NULL, 0, &dwBytesX, NULL);
			dwLength = dwBytesX;
			_psdidd = (PSP_DEVICE_INTERFACE_DETAIL_DATA)GlobalAlloc(GPTR, dwLength);
			if (_psdidd == NULL)
			{
				continue;
			}

			_psdidd->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);
			if (!SetupDiGetDeviceInterfaceDetail(hDeviceInfo, &_sdid, _psdidd, dwLength, &dwBytesX, NULL))
			{
				GlobalFree(_psdidd);
				continue;
			}

			BYTE pdata[4096] = { 0 };
			DWORD nLen = sizeof(pdata);
			DWORD nType = 0;
			CString sLocPaths;
			CString sHubName = _psdidd->DevicePath;

			extern std::map<CString, CString, cmpIngnoreCase> ginstallHub;
			if (ginstallHub.find(sHubName) == ginstallHub.end())
			{
				GlobalFree(_psdidd);
				continue;
			}


			if (!CheckFDSmartHub(sHubName, USB5806_20 | USB5806_30 | USB5734_20 | USB5734_30))
			{
				GlobalFree(_psdidd);
				continue;
			}
			if (DWORD dwRet = GetDeviceCommonSymbl(_psdidd->DevicePath, (PDEVPROPKEY)&DEVPKEY_Device_LocationPaths, pdata, nLen, nType) == ERROR_SUCCESS)
			{
				if (nType == DEVPROP_TYPE_STRING_LIST)
				{
					//add locationPaths
					sLocPaths = (TCHAR *)pdata;
				}
			}
			else
			{
				PrintError(dwRet, _T(__FILE__), __LINE__);
				GlobalFree(_psdidd);
				continue;
			}

			hHub = CreateFile(_psdidd->DevicePath, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, NULL);
			GlobalFree(_psdidd);
			if (hHub == INVALID_HANDLE_VALUE)
			{
				PrintError(GetLastError(), _T(__FILE__), __LINE__);
				continue;
			}

			DWORD BytesReturned;
			int hubportcnt = 0;
			USB_NODE_INFORMATION nodeInformation;
			ZeroMemory(&nodeInformation, sizeof(USB_NODE_INFORMATION));
			if (DeviceIoControl(hHub, IOCTL_USB_GET_NODE_INFORMATION, &nodeInformation, sizeof(USB_NODE_INFORMATION), &nodeInformation, sizeof(USB_NODE_INFORMATION), &BytesReturned, NULL))
			{
				hubportcnt = nodeInformation.u.HubInformation.HubDescriptor.bNumberOfPorts;
			}
			else
			{
				PrintError(GetLastError(), _T(__FILE__), __LINE__);
				CloseHandle(hHub);
				continue;
			}
			PUSB_HUB_CAPABILITIES_EX hubCapabilityEx = NULL;
			Get_USB_HUB_CAPABILITIES_EX(hHub, &hubCapabilityEx);

			CloseHandle(hHub);

			PHUBINFO pinfo = new HUBINFO();
			if (pinfo != NULL)
			{
				pinfo->sHubName = sHubName;
				pinfo->sLocationpath = sLocPaths;
				pinfo->portcount = hubportcnt;
				pinfo->pHubCapabilityEx = hubCapabilityEx;
				logIt(_T("find:%s==locatiopath:%s==portcount:%d\n"), sHubName, sLocPaths, hubportcnt);
				allhubinfo.insert(std::map<CString, PHUBINFO> ::value_type(sHubName, pinfo));
			}
			else
			{
				if (hubCapabilityEx != NULL)
				{
					free(hubCapabilityEx);
				}
			}
		}
	}
	else
	{
		dwRet = GetLastError();
		PrintError(dwRet, _T(__FILE__), __LINE__);
	}
	if (hDeviceInfo != INVALID_HANDLE_VALUE){
		SetupDiDestroyDeviceInfoList(hDeviceInfo);
	}
	return dwRet;
}


DWORD CUsbHub::ListAllUsbHub()
{
	ENTER_FUNCTION();
	HANDLE                              hHub;
	HDEVINFO                            hDeviceInfo;
	SP_DEVICE_INTERFACE_DATA            _sdid;
	PSP_DEVICE_INTERFACE_DETAIL_DATA    _psdidd;
	DWORD dwLength;
	DWORD dwBytesX;
	DWORD dwRet = ERROR_SUCCESS;

	hDeviceInfo = SetupDiGetClassDevs(&GUID_DEVINTERFACE_USB_HUB, NULL, NULL, DIGCF_PRESENT | DIGCF_DEVICEINTERFACE);
	if (hDeviceInfo != INVALID_HANDLE_VALUE)
	{
		_sdid.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);
		for (int i = 0; SetupDiEnumDeviceInterfaces(hDeviceInfo, NULL, &GUID_DEVINTERFACE_USB_HUB, i, &_sdid); i++)
		{
			SetupDiGetDeviceInterfaceDetail(hDeviceInfo, &_sdid, NULL, 0, &dwBytesX, NULL);
			dwLength = dwBytesX;
			_psdidd = (PSP_DEVICE_INTERFACE_DETAIL_DATA)GlobalAlloc(GPTR, dwLength);
			if (_psdidd == NULL)
			{
				continue;
			}

			_psdidd->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);
			if (!SetupDiGetDeviceInterfaceDetail(hDeviceInfo, &_sdid, _psdidd, dwLength, &dwBytesX, NULL))
			{
				GlobalFree(_psdidd);
				continue;
			}

			BYTE pdata[4096] = { 0 };
			DWORD nLen = sizeof(pdata);
			DWORD nType = 0;
			CString sLocPaths;
			CString sHubName = _psdidd->DevicePath;
			if (!CheckFDSmartHub(sHubName, USB5806_20 | USB5806_30 | USB5734_20 | USB5734_30))
			{
				GlobalFree(_psdidd);
				continue;
			}
			if (DWORD dwRet = GetDeviceCommonSymbl(_psdidd->DevicePath, (PDEVPROPKEY)&DEVPKEY_Device_LocationPaths, pdata, nLen, nType) == ERROR_SUCCESS)
			{
				if (nType == DEVPROP_TYPE_STRING_LIST)
				{
					//add locationPaths
					sLocPaths = (TCHAR *)pdata;
				}
			}
			else
			{
				PrintError(dwRet, _T(__FILE__), __LINE__);
				GlobalFree(_psdidd);
				continue;
			}

			hHub = CreateFile(_psdidd->DevicePath, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, NULL);
			GlobalFree(_psdidd);
			if (hHub == INVALID_HANDLE_VALUE)
			{
				PrintError(GetLastError(), _T(__FILE__), __LINE__);
				continue;
			}

			DWORD BytesReturned;
			int hubportcnt = 0;
			USB_NODE_INFORMATION nodeInformation;
			ZeroMemory(&nodeInformation, sizeof(USB_NODE_INFORMATION));
			if (DeviceIoControl(hHub, IOCTL_USB_GET_NODE_INFORMATION, &nodeInformation, sizeof(USB_NODE_INFORMATION), &nodeInformation, sizeof(USB_NODE_INFORMATION), &BytesReturned, NULL))
			{
				hubportcnt = nodeInformation.u.HubInformation.HubDescriptor.bNumberOfPorts;
			}
			else
			{
				PrintError(GetLastError(), _T(__FILE__), __LINE__);
				CloseHandle(hHub);
				continue;
			}
			PUSB_HUB_CAPABILITIES_EX hubCapabilityEx = NULL;
			Get_USB_HUB_CAPABILITIES_EX(hHub, &hubCapabilityEx);

			CloseHandle(hHub);

			PHUBINFO pinfo = new HUBINFO();
			if (pinfo != NULL)
			{
				pinfo->sHubName = sHubName;
				pinfo->sLocationpath = sLocPaths;
				pinfo->portcount = hubportcnt;
				pinfo->pHubCapabilityEx = hubCapabilityEx;
				logIt(_T("find:%s==locatiopath:%s==portcount:%d\n"), sHubName, sLocPaths, hubportcnt);
				allhubinfo.insert(std::map<CString, PHUBINFO> ::value_type(sHubName, pinfo));
			}
			else
			{
				if (hubCapabilityEx != NULL)
				{
					free(hubCapabilityEx);
				}
			}
		}
	}
	else
	{
		dwRet = GetLastError();
		PrintError(dwRet, _T(__FILE__), __LINE__);
	}
	if (hDeviceInfo != INVALID_HANDLE_VALUE){
		SetupDiDestroyDeviceInfoList(hDeviceInfo);
	}
	return dwRet;
}

int CUsbHub::WriteCalibration(CString sHub2, CString sLocpath2, CString sHub3, CString sLocpath3, int hub, int hubindex)
{
	extern TCHAR sIniFileName[MAX_PATH];
	extern int gStartLabel;
	DWORD dwRet(ERROR_SUCCESS);
	ENTER_FUNCTION();
	CString sLocatpath;
	int startindex = gStartLabel - 1 + hub;

	CString sHubName = sHub2;
	sHubName = sHubName.TrimLeft(_T("\\\\?\\")).TrimLeft(_T("\\\\.\\"));
	if (sHubName.GetLength() == 0 || sLocpath2.GetLength() == 0) return ERROR_INVALID_PARAMETER;
	CString strLab; 
	for (int i = 1; i <= 4; i++)
	{
		strLab.Format(_T("%d"), startindex + i-1);
		_tprintf(_T("[detected][label=%s]\n"), strLab.GetString());
		CString sHead; sHead.Format(_T("%d@"), portlabels[i]);
		if (!WritePrivateProfileString(_T("label"), strLab, sHead + sHubName, sIniFileName))
		{
			dwRet = GetLastError();
			PrintError(dwRet, _T(__FILE__), __LINE__);
		}//[label_2.0]
		if (!WritePrivateProfileString(_T("label_2.0"), strLab, sHead + sHubName, sIniFileName))
		{
			dwRet = GetLastError();
			PrintError(dwRet, _T(__FILE__), __LINE__);
		}
		sLocatpath = sHead + sLocpath2;
		if (!WritePrivateProfileString(_T("locationpaths"), strLab, sLocatpath, sIniFileName))
		{
			dwRet = GetLastError();
			PrintError(dwRet, _T(__FILE__), __LINE__);
		}
		if (!WritePrivateProfileString(_T("locationpaths_2.0"), strLab, sLocatpath, sIniFileName))
		{
			dwRet = GetLastError();
			PrintError(dwRet, _T(__FILE__), __LINE__);
		}
	}

	sHubName = sHub3;
	sHubName = sHubName.TrimLeft(_T("\\\\?\\")).TrimLeft(_T("\\\\.\\"));
	if (sHubName.GetLength() > 0 && sLocpath3.GetLength() > 0)
	{
		for (int i = 1; i <= 4; i++)
		{
			strLab.Format(_T("%d"), startindex + i - 1);
			//_tprintf(_T("[detected][label=%s]\n"), strLab);//USB3.0 can not out put
			CString sHead; sHead.Format(_T("%d@"), portlabels[i]);
	
			if (!WritePrivateProfileString(_T("label_3.0"), strLab, sHead + sHubName, sIniFileName))
			{
				dwRet = GetLastError();
				PrintError(dwRet, _T(__FILE__), __LINE__);
			}
			sLocatpath = sHead + sLocpath3;
			if (!WritePrivateProfileString(_T("locationpaths_3.0"), strLab, sLocatpath, sIniFileName))
			{
				dwRet = GetLastError();
				PrintError(dwRet, _T(__FILE__), __LINE__);
			}
		}

	}
	return dwRet;
}

int CUsbHub::CalibrationHubs()
{
	ENTER_FUNCTION();
	int ret = ERROR_NOT_FOUND;
	if (allhubinfo.size() == 0)
	{
		logIt(_T("not found fd smart hub.\n"));
		return ret;
	}
	if (allhubinfo.size() % 6 != 0){
		logIt(_T("not found total fd smart hub. we try to continue. maybe other hub found.\n"));
	}
	extern int gStartLabel;
	int startindex = gStartLabel - 1;
	extern TCHAR sIniFileName[MAX_PATH];
	ret = ERROR_SUCCESS;
	for (std::map<CString, PHUBINFO>::iterator iter = allhubinfo.begin(); iter != allhubinfo.end(); iter++)
	{
		if (CheckFDSmartHub(iter->first.GetString(), USB5734_20))
		{
			if (iter->second->pConnectionInfoEx == NULL){
				iter->second->pConnectionInfoEx = GetHubInfo((TCHAR *)iter->first.GetString());
				if (iter->second->pConnectionInfoEx == NULL) continue;
				_tprintf(_T("\n%x\n"), iter->second->pConnectionInfoEx->DeviceDescriptor.bcdDevice);
			}

			//check usb 20, get usb30
			TCHAR CompanionHub[MAX_PATH] = { 0 }; 
			int CompanionPort = 0;
			int hub = 0;
			int hubindex = 0;
			if (IsWindows7SP1OrGreater())
			{
				if (GetHubPeerHub((TCHAR *)iter->first.GetString(), CompanionHub, CompanionPort) == ERROR_SUCCESS)
				{ //USB#VID_0424&PID_5734#7&d9ad552&0&2#{f18a0e88-c30c-11d0-8815-00a0c906bed8}
				//2@USB#VID_0424&PID_5734#7&d9ad552&0&2#{f18a0e88-c30c-11d0-8815-00a0c906bed8}
					logIt(_T("Peer Hub connect:%d@%s"), CompanionPort, CompanionHub);
				}
			}
			if (iter->second->pConnectionInfoEx != NULL){
				BCDTOLABEL(iter->second->pConnectionInfoEx->DeviceDescriptor.bcdDevice, hub);
				BCDTOINDEX(iter->second->pConnectionInfoEx->DeviceDescriptor.bcdDevice, hubindex);
			}
			
			CString sHub2 = iter->first; 
			CString sLocpath2 = iter->second->sLocationpath;
			CString sHub3, sLocpath3;
			sHub3.Format(_T("\\\\?\\%ws"), CompanionHub);
			if (allhubinfo.find(sHub3) != allhubinfo.end()){
				sLocpath3 = allhubinfo[sHub3]->sLocationpath;
			}
			
			//write config
			TCHAR gLabel[MAX_PATH];
			_stprintf_s(gLabel, _T("submain%d_%d"), gStartLabel, hub);
			if (!WritePrivateProfileString(_T("ControlHub"), gLabel, sLocpath2, sIniFileName))
			{
				PrintError(GetLastError(), _T(__FILE__), __LINE__);
			}//[label_2.0]
			WriteCalibration(sHub2, sLocpath2, sHub3, sLocpath3, hub, hubindex);
		}
		else if (CheckFDSmartHub(iter->first.GetString(), USB5806_20))
		{
			//write ini save chainport
			CString sLocpath2 = iter->second->sLocationpath;
			TCHAR gLabel[MAX_PATH];
			_stprintf_s(gLabel, _T("main%d"), gStartLabel);
			if (!WritePrivateProfileString(_T("ControlHub"), gLabel, sLocpath2, sIniFileName))
			{
				PrintError(GetLastError(), _T(__FILE__), __LINE__);
			}//[label_2.0]
		}
	}
	CString scount; scount.Format(_T("%d"), startindex + 20);
	WritePrivateProfileString(_T("amount"), _T("labels"), scount, sIniFileName);
#include <VersionHelpers.h>
	if (IsWindows8Point1OrGreater()) {

	}
	return ret;
}

int CUsbHub::GetHubPeerHub(TCHAR *HubName, TCHAR CompanionHub[MAX_PATH], int &CompanionPort)
{
	ENTER_FUNCTION();
	int ret = ERROR_INVALID_PARAMETER;
	TCHAR symblName[1024] = { 0 };

	if ((_tcsncicmp(HubName, _T("\\\\?\\"), 4) == 0) || (_tcsncicmp(HubName, _T("\\\\.\\"), 4) == 0))
	{
		_stprintf_s(symblName, _T("%ws"), HubName);
	}
	else
	{
		_stprintf_s(symblName, _T("\\\\?\\%ws"), HubName);
	}

	BYTE pdata[4096] = { 0 };
	DWORD nLen = sizeof(pdata);
	DWORD nType = 0;
	if ((ret=GetDeviceCommonSymbl(symblName, (PDEVPROPKEY)&DEVPKEY_Device_Parent, pdata, nLen, nType)) == ERROR_SUCCESS)
	{
		TCHAR parentInstanceid[MAX_PATH] = { 0 };
		if (_tcslen((TCHAR*)pdata) > 0)
		{
			_stprintf_s(parentInstanceid, _T("%ws"), (TCHAR*)pdata);
			nLen = sizeof(pdata);
			ZeroMemory(pdata, nLen);
			if ((ret = GetDeviceCommonSymbl(symblName, (PDEVPROPKEY)&DEVPKEY_Device_LocationPaths, pdata, nLen, nType)) == ERROR_SUCCESS)
			{
				TCHAR sLocationpath[MAX_PATH] = { 0 };
				if (_tcslen((TCHAR*)pdata) > 0)
				{
					_stprintf_s(sLocationpath, _T("%ws"), (TCHAR*)pdata);
					TCHAR parentHubname[MAX_PATH + 1] = { 0 };
					if ((ret = GetDeviceSymblInstanceIdEx(parentInstanceid, parentHubname, MAX_PATH, (GUID *)&GUID_DEVINTERFACE_USB_HUB)) == ERROR_SUCCESS)
					{//get Parent symbol link
						HUBINFO hubinfo;
						hubinfo.sLocationpath = sLocationpath;
						int port = hubinfo.GetHubIndexFromLocPaths();
						HANDLE hHubDevice = CreateFile(symblName,
							GENERIC_WRITE,
							FILE_SHARE_WRITE,
							NULL,
							OPEN_EXISTING,
							0,
							NULL);
						if (hHubDevice != INVALID_HANDLE_VALUE)
						{
							PUSB_PORT_CONNECTOR_PROPERTIES ppPortConnectorProps = NULL;
							ret = Get_USB_PORT_CONNECTOR_PROPERTIES(hHubDevice, port, &ppPortConnectorProps);
							CloseHandle(hHubDevice);
							if (ret == ERROR_SUCCESS){
								CompanionPort = ppPortConnectorProps->CompanionPortNumber;
								_stprintf_s(CompanionHub, MAX_PATH, _T("%ws"), (TCHAR *)ppPortConnectorProps->CompanionHubSymbolicLinkName);
								if (ppPortConnectorProps == NULL){
									free(ppPortConnectorProps);
								}
								return ERROR_SUCCESS;
							}
							if (ppPortConnectorProps == NULL){
								free(ppPortConnectorProps);
							}
						}
						else{
							ret = GetLastError();
							PRINT_ERR(ret);
						}
					}
					else{
						PRINT_ERR(GetLastError());
					}
				}
			}
			else{
				PRINT_ERR(GetLastError());
			}

		}
	}
	else{
		PRINT_ERR(GetLastError());
	}

	return ret;
}

PUSB_NODE_CONNECTION_INFORMATION_EX    CUsbHub::GetHubInfo(TCHAR *HubName)
{
	TCHAR symblName[1024] = { 0 };
	PUSB_NODE_CONNECTION_INFORMATION_EX connectionInfoEx = NULL;
	ENTER_FUNCTION();
	if ((_tcsncicmp(HubName, _T("\\\\?\\"), 4) == 0) || (_tcsncicmp(HubName, _T("\\\\.\\"), 4) == 0))
	{
		_stprintf_s(symblName, _T("%ws"), HubName);
	}
	else
	{
		_stprintf_s(symblName, _T("\\\\?\\%ws"), HubName);
	}

	BYTE pdata[4096] = { 0 };
	DWORD nLen = sizeof(pdata);
	DWORD nType = 0;
	if (GetDeviceCommonSymbl(symblName, (PDEVPROPKEY)&DEVPKEY_Device_Parent, pdata, nLen, nType) == ERROR_SUCCESS)
	{
		TCHAR parentInstanceid[MAX_PATH] = { 0 };
		if (_tcslen((TCHAR*)pdata) > 0)
		{
			_stprintf_s(parentInstanceid, _T("%ws"), (TCHAR*)pdata);
			nLen = sizeof(pdata);
			ZeroMemory(pdata, nLen);
			if (GetDeviceCommonSymbl(symblName, (PDEVPROPKEY)&DEVPKEY_Device_LocationPaths, pdata, nLen, nType) == ERROR_SUCCESS)
			{
				TCHAR sLocationpath[MAX_PATH] = { 0 };
				if (_tcslen((TCHAR*)pdata) > 0)
				{
					_stprintf_s(sLocationpath, _T("%ws"), (TCHAR*)pdata);
					TCHAR parentHubname[MAX_PATH + 1] = { 0 };
					if (GetDeviceSymblInstanceIdEx(parentInstanceid, parentHubname, MAX_PATH, (GUID *)&GUID_DEVINTERFACE_USB_HUB) == ERROR_SUCCESS)
					{//get Parent symbol link
						HUBINFO hubinfo;
						hubinfo.sLocationpath = sLocationpath;
						int nPort = hubinfo.GetHubIndexFromLocPaths();
						connectionInfoEx = GetHubConnectionInfo(parentHubname, nPort);
						return connectionInfoEx;
					}
					else{
						PRINT_ERR(GetLastError());
					}
				}
			}
			else{
				PRINT_ERR(GetLastError());
			}

		}
	}
	else{
		PRINT_ERR(GetLastError());
	}

	if (connectionInfoEx != NULL) free(connectionInfoEx);
	return NULL;

}

PUSB_NODE_CONNECTION_INFORMATION_EX    CUsbHub::GetHubConnectionInfo(TCHAR *HubName, int hubport)
{
	TCHAR symblName[1024] = { 0 };
	ULONG nBytesEx;
	PUSB_NODE_CONNECTION_INFORMATION_EX connectionInfoEx = NULL;
	nBytesEx = sizeof(USB_NODE_CONNECTION_INFORMATION_EX) +
		(sizeof(USB_PIPE_INFO) * 30);
	ENTER_FUNCTION();
	connectionInfoEx = (PUSB_NODE_CONNECTION_INFORMATION_EX)malloc(nBytesEx);

	if ((_tcsncicmp(HubName, _T("\\\\?\\"), 4) == 0) || (_tcsncicmp(HubName, _T("\\\\.\\"), 4) == 0))
	{
		_stprintf_s(symblName, _T("%ws"), HubName);
	}
	else
	{
		_stprintf_s(symblName, _T("\\\\.\\%ws"), HubName);
	}
	HANDLE hHubDevice = CreateFile(symblName,
		GENERIC_WRITE,
		FILE_SHARE_WRITE,
		NULL,
		OPEN_EXISTING,
		0,
		NULL);
	if (hHubDevice != INVALID_HANDLE_VALUE)
	{
		connectionInfoEx->ConnectionIndex = hubport;

		BOOL success = DeviceIoControl(hHubDevice,
			IOCTL_USB_GET_NODE_CONNECTION_INFORMATION_EX,
			connectionInfoEx,
			nBytesEx,
			connectionInfoEx,
			nBytesEx,
			&nBytesEx,
			NULL);
		if (success)
		{
			return connectionInfoEx;
		}

		CloseHandle(hHubDevice);
	}
	else{
		PRINT_ERR(GetLastError());
	}
	if (connectionInfoEx != NULL)
	{
		free(connectionInfoEx);
		connectionInfoEx = NULL;
	}
	return NULL;
}


int CUsbHub::GetLocationPathRootHub(TCHAR *HubName, int ndex)
{
	int ret = ERROR_UNKNOWN_EXCEPTION;
	TCHAR symblName[1024] = { 0 };

	if ((_tcsncicmp(HubName, _T("\\\\?\\"), 4) == 0) || (_tcsncicmp(HubName, _T("\\\\.\\"), 4) == 0))
	{
		_stprintf_s(symblName, _T("%ws"), HubName);
	}
	else
	{
		_stprintf_s(symblName, _T("%ws%ws"), _T("\\\\?\\"), HubName);
	}

	HDEVINFO hDevInfo = SetupDiCreateDeviceInfoList(NULL, NULL);
	if (hDevInfo != INVALID_HANDLE_VALUE)
	{
		DWORD sz = 0;
		SP_DEVICE_INTERFACE_DATA devIntData;
		devIntData.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);
		if (SetupDiOpenDeviceInterface(hDevInfo, symblName, 0, &devIntData))
		{
			DEVPROPTYPE type;
			BYTE b[2048];
			ZeroMemory(b, sizeof(b));
			sz = 0;

			SP_DEVINFO_DATA devInfoData = { 0 };
			devInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
			SetupDiGetDeviceInterfaceDetail(hDevInfo, &devIntData, NULL, 0, &sz, &devInfoData);
			if (GetLastError() == ERROR_INSUFFICIENT_BUFFER)
			{
				if (SetupDiGetDeviceProperty(hDevInfo, &devInfoData, &DEVPKEY_Device_LocationPaths, &type, b, 2048, &sz, 0))
				{
					//logIt(_T("Property: #%d\n"), i + 1);
					//dump(p, type, b, sz);
					if (type == DEVPROP_TYPE_STRING_LIST)
					{
						TCHAR *instanceid = (TCHAR *)b;
						DWORD dwoffset = 0;
						while (instanceid != NULL && _tcslen(instanceid) > 0)
						{
							mLocatindexs[instanceid] = ndex;
							ret = ERROR_SUCCESS;
							break;
						}
					}
				}
				else{
					ret = GetLastError();
					PRINT_ERR(ret);
				}
			}
		}
		else{
			ret = GetLastError();
			PRINT_ERR(ret);
		}

		SetupDiDestroyDeviceInfoList(hDevInfo);
	}

	return ret;
}


VOID	CUsbHub::EnumerateHostControllers()
{
	HANDLE                           hHCDev = NULL;
	HDEVINFO                         deviceInfo = NULL;
	SP_DEVINFO_DATA                  deviceInfoData;
	SP_DEVICE_INTERFACE_DATA         deviceInterfaceData;
	PSP_DEVICE_INTERFACE_DETAIL_DATA deviceDetailData = NULL;
	ULONG                            index = 0;
	ULONG                            requiredLength = 0;
	BOOL                             success;

	// Iterate over host controllers using the new GUID based interface
	//
	deviceInfo = SetupDiGetClassDevs((LPGUID)&GUID_CLASS_USB_HOST_CONTROLLER,
		NULL,
		NULL,
		(DIGCF_PRESENT | DIGCF_DEVICEINTERFACE));

	deviceInfoData.cbSize = sizeof(SP_DEVINFO_DATA);

	for (index = 0;
		SetupDiEnumDeviceInfo(deviceInfo,
		index,
		&deviceInfoData);
	index++)
	{
		deviceInterfaceData.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);

		success = SetupDiEnumDeviceInterfaces(deviceInfo,
			0,
			(LPGUID)&GUID_CLASS_USB_HOST_CONTROLLER,
			index,
			&deviceInterfaceData);

		if (!success)
		{
			break;
		}

		success = SetupDiGetDeviceInterfaceDetail(deviceInfo,
			&deviceInterfaceData,
			NULL,
			0,
			&requiredLength,
			NULL);

		if (!success && GetLastError() != ERROR_INSUFFICIENT_BUFFER)
		{
			PRINT_ERR(GetLastError());
			break;
		}

		deviceDetailData = (PSP_DEVICE_INTERFACE_DETAIL_DATA)malloc(requiredLength);
		if (deviceDetailData == NULL)
		{
			PRINT_ERR(GetLastError());
			break;
		}

		deviceDetailData->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);

		success = SetupDiGetDeviceInterfaceDetail(deviceInfo,
			&deviceInterfaceData,
			deviceDetailData,
			requiredLength,
			&requiredLength,
			NULL);

		if (!success)
		{
			PRINT_ERR(GetLastError());
			break;
		}

		hHCDev = CreateFile(deviceDetailData->DevicePath,
			GENERIC_WRITE,
			FILE_SHARE_WRITE,
			NULL,
			OPEN_EXISTING,
			0,
			NULL);

		// If the handle is valid, then we've successfully opened a Host
		// Controller.  Display some info about the Host Controller itself,
		// then enumerate the Root Hub attached to the Host Controller.
		//
		if (hHCDev != INVALID_HANDLE_VALUE)
		{
			TCHAR *Name = GetRootHubName(hHCDev);
			if (Name != NULL)
			{
				logIt(_T("%d-%s\n"), index + 1, Name);

				GetLocationPathRootHub(Name, index + 1);
			}
			free(Name);
			CloseHandle(hHCDev);
		}

		free(deviceDetailData);
	}

	SetupDiDestroyDeviceInfoList(deviceInfo);

	return;
}

int GetDeviceSymblInstanceIdEx(TCHAR *device_iid, LPWSTR lpReturnedString, DWORD nSize, GUID *guid)
{
	// Open an enumeration handle so we can locate all devices of our
	// own class
	int ret = ERROR_UNKNOWN_FEATURE;
	ENTER_FUNCTION();
	HDEVINFO info = SetupDiGetClassDevs(guid, NULL, NULL, DIGCF_PRESENT | DIGCF_INTERFACEDEVICE);
	if (info == INVALID_HANDLE_VALUE)
		return GetLastError();

	// Enumerate all devices of our class. For each one, create a
	// CDeviceEntryList object. Then determine the friendly name of the
	// device by reading the registry.

	SP_DEVICE_INTERFACE_DATA ifdata;
	ifdata.cbSize = sizeof(ifdata);
	DWORD devindex;
	for (devindex = 0; SetupDiEnumDeviceInterfaces(info, NULL, guid, devindex, &ifdata); ++devindex)
	{						// for each device

		// Determine the symbolic link name for this device instance. Since
		// this is variable in length, make an initial call to determine
		// the required length.

		DWORD needed;
		SP_DEVINFO_DATA did = { sizeof(SP_DEVINFO_DATA) };

		SetupDiGetDeviceInterfaceDetail(info, &ifdata, NULL, 0, &needed, &did);

		TCHAR sInstanceId[MAX_PATH] = { 0 };
		if (SetupDiGetDeviceInstanceId(info, &did, sInstanceId, MAX_PATH, NULL))
		{
			if (_tcsncicmp(sInstanceId, device_iid, _tcslen(device_iid)) == 0)
			{
				TCHAR  DetailDataBuffer[1024] = { 0 };
				PSP_DEVICE_INTERFACE_DETAIL_DATA DetailData = (PSP_DEVICE_INTERFACE_DETAIL_DATA)DetailDataBuffer;
				DetailData->cbSize = sizeof SP_INTERFACE_DEVICE_DETAIL_DATA;

				if (SetupDiGetDeviceInterfaceDetail(info, &ifdata, DetailData, needed, NULL, NULL))
				{						// can't get detail info
					_stprintf_s(lpReturnedString, nSize, DetailData->DevicePath);
					ret = ERROR_SUCCESS;
					break;
				}
				else// can't get detail info
				{
					ret = GetLastError();
					PRINT_ERR(ret);
				}
			}
		}

	}						// for each device

	SetupDiDestroyDeviceInfoList(info);

	return ret;
}							// CDeviceList::Initialize
