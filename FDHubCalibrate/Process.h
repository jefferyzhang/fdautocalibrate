#pragma once
#define BUFSIZE 4096 

class CProcess
{
public:

	CProcess()
	{
	}

	~CProcess()
	{
	}

	int RunExe(LPCTSTR exe, LPCTSTR args, int timeout)
	{
		logIt(_T("RunExe++ %ws arg=%ws\n"), exe, args);
		SECURITY_ATTRIBUTES saAttr;
		BOOL fSuccess;
		BOOL bRet = FALSE;
		// Set the bInheritHandle flag so pipe handles are inherited.
		saAttr.nLength = sizeof(SECURITY_ATTRIBUTES);
		saAttr.bInheritHandle = TRUE;
		saAttr.lpSecurityDescriptor = NULL;

		// The steps for redirecting child process's STDOUT: 
		//     1. Save current STDOUT, to be restored later. 
		//     2. Create anonymous pipe to be STDOUT for child process. 
		//     3. Set STDOUT of the parent process to be write handle to 
		//        the pipe, so it is inherited by the child process. 
		//     4. Create a noninheritable duplicate of the read handle and
		//        close the inheritable read handle.  

		// Save the handle to the current STDOUT.  
		hSaveStdout = GetStdHandle(STD_OUTPUT_HANDLE);

		// Create a pipe for the child process's STDOUT.  
		if (!CreatePipe(&hChildStdoutRd, &hChildStdoutWr, &saAttr, 0))
		{
			logIt(_T("Stdout pipe creation failed\n"));
			return bRet;
		}

		// Set a write handle to the pipe to be STDOUT.  
		if (!SetStdHandle(STD_OUTPUT_HANDLE, hChildStdoutWr))
		{
			logIt(_T("Redirecting STDOUT failed\n"));
			return bRet;
		}

		// Create noninheritable read handle and close the inheritable read handle. 
		fSuccess = DuplicateHandle(GetCurrentProcess(), hChildStdoutRd,
			GetCurrentProcess(), &hChildStdoutRdDup,
			0, FALSE,
			DUPLICATE_SAME_ACCESS);
		if (!fSuccess)
		{
			logIt(_T("DuplicateHandle failed\n"));
			return bRet;
		}
		CloseHandle(hChildStdoutRd);

		// The steps for redirecting child process's STDIN: 
		//     1.  Save current STDIN, to be restored later. 
		//     2.  Create anonymous pipe to be STDIN for child process. 
		//     3.  Set STDIN of the parent to be the read handle to the 
		//         pipe, so it is inherited by the child process. 
		//     4.  Create a noninheritable duplicate of the write handle, 
		//         and close the inheritable write handle.  

		// Save the handle to the current STDIN. 
		hSaveStdin = GetStdHandle(STD_INPUT_HANDLE);

		// Create a pipe for the child process's STDIN.  
		if (!CreatePipe(&hChildStdinRd, &hChildStdinWr, &saAttr, 0))
		{
			logIt(_T("Stdin pipe creation failed\n"));
			return bRet;
		}
		// Set a read handle to the pipe to be STDIN.  
		if (!SetStdHandle(STD_INPUT_HANDLE, hChildStdinRd))
		{
			logIt(_T("Redirecting Stdin failed\n"));
			return bRet;
		}
		// Duplicate the write handle to the pipe so it is not inherited.  
		fSuccess = DuplicateHandle(GetCurrentProcess(), hChildStdinWr,
			GetCurrentProcess(), &hChildStdinWrDup,
			0, FALSE,                  // not inherited       
			DUPLICATE_SAME_ACCESS);
		if (!fSuccess)
		{
			logIt(_T("DuplicateHandle failed\n"));
			return bRet;
		}
		CloseHandle(hChildStdinWr);

		TCHAR shellCmd[BUFSIZE] = { 0 };
		_stprintf_s(shellCmd, _T("%ws %ws"), exe, args);
		// Now create the child process. 
		if (!CreateChildProcess(shellCmd, dwProcessId))
		{
			logIt(_T("CreateChildProcess failed\n"));
			return bRet;
		}
		// After process creation, restore the saved STDIN and STDOUT.  
		if (!SetStdHandle(STD_INPUT_HANDLE, hSaveStdin))
		{
			logIt(_T("Re-redirecting Stdin failed\n"));
			return bRet;
		}
		if (!SetStdHandle(STD_OUTPUT_HANDLE, hSaveStdout))
		{
			logIt(_T("Re-redirecting Stdout failed\n"));
			return bRet;
		}
		unsigned int thID1 =0;
		m_hReadThread = (HANDLE)_beginthreadex(NULL, 0, ReadPipeThreadProc, this, 0, &thID1);
		if (!m_hReadThread)
		{
			logIt(_T("Cannot start read-redirect thread!\n"));
			return bRet;
		}

		DWORD exitCode = -1;
		if (WaitForSingleObject(m_hReadThread, timeout) == WAIT_OBJECT_0)
		{
			GetExitCodeProcess(piProcInfo.hProcess, &exitCode);
		}
		logIt(_T("Run Exe exit code =%d\n"), exitCode);

		CloseHandle(piProcInfo.hProcess);
		CloseHandle(piProcInfo.hThread);

		return bRet;
	}

	BOOL CreateChildProcess(TCHAR shellCmd[BUFSIZE], DWORD& dwProcessId)
	{
		// Set up members of STARTUPINFO structure.  
		ZeroMemory(&siStartInfo, sizeof(STARTUPINFO));
		siStartInfo.cb = sizeof(STARTUPINFO);

		siStartInfo.dwFlags = STARTF_USESTDHANDLES;
		siStartInfo.hStdInput = hChildStdinRd;
		siStartInfo.hStdOutput = hChildStdoutWr;
		siStartInfo.hStdError = hChildStdoutWr;

		//TCHAR shellCmd[BUFSIZE] = { 0 };
//		if (!GetEnvironmentVariable(_T("ComSpec"), shellCmd, _MAX_PATH))
//			return FALSE;
//#ifdef _UNICODE
//		_tcscat(shellCmd, _T(" /U"));
//#else	
//		_tcscat(shellCmd, _T(" /A"));
//#endif
		// Create the child process.  
		BOOL ret = CreateProcess(NULL,
			shellCmd,       // applicatin name
			NULL,          // process security attributes 
			NULL,          // primary thread security attributes 
			TRUE,          // handles are inherited 
			DETACHED_PROCESS, // creation flags 
			NULL,          // use parent's environment 
			NULL,          // use parent's current directory 
			&siStartInfo,  // STARTUPINFO pointer 
			&piProcInfo);  // receives PROCESS_INFORMATION 
		if (ret)
			dwProcessId = piProcInfo.dwProcessId;
		return ret;
	}

	VOID WriteToPipe(LPCTSTR line)
	{
		DWORD dwWritten;

		WriteFile(hChildStdinWrDup, line, _tcslen(line)*sizeof(TCHAR),
			&dwWritten, NULL);
	}
	
	static UINT WINAPI ReadPipeThreadProc(LPVOID pParam)
	{
		DWORD dwRead, dwAvail;
		TCHAR chBuf[BUFSIZE];
		CProcess* pp = (CProcess*)pParam;

		logIt(_T("ReadPipe Thread begin run\n"));

		for (;;)
		{
			if (!PeekNamedPipe(pp->hChildStdoutRdDup, NULL, NULL, &dwRead, &dwAvail, NULL) || dwAvail <= 0)
			{
				Sleep(10);
				continue;
			}
			else
				break;
		}
		for (;;)
		{
			if (!ReadFile(pp->hChildStdoutRdDup, chBuf, BUFSIZE, &dwRead, NULL) || dwRead == 0)
				break;
			chBuf[dwRead / sizeof(TCHAR)] = _T('\0');
			logIt(_T("%S\n"), chBuf);
		}
		CloseHandle(pp->hChildStdinRd);
		CloseHandle(pp->hChildStdoutWr);
		CloseHandle(pp->hChildStdinWrDup);
		CloseHandle(pp->hChildStdoutRdDup);
		pp->m_hReadThread = NULL;
		pp->dwProcessId = DWORD(-1);
		return 1;
	}
	

private:
	HANDLE hChildStdinRd, hChildStdinWr, hChildStdinWrDup,
		hChildStdoutRd, hChildStdoutWr, hChildStdoutRdDup,
		hSaveStdin, hSaveStdout;
	HANDLE	m_hReadThread;
	DWORD dwProcessId;

	PROCESS_INFORMATION piProcInfo;
	STARTUPINFO siStartInfo;

};

