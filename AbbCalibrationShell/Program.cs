﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AbbCalibrationShell
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Configuration.Install.InstallContext _arg = new System.Configuration.Install.InstallContext(null, args);
            if (_arg.IsParameterTrue("debug"))
            {
                System.Console.WriteLine("Wait for debugger, press any key to continue...");
                System.Console.ReadKey();
            }
            // dump version
            logIt(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileVersionInfo.ToString());
            // dump args
            logIt(string.Format("called by arg: ({0})", args.Length));
            foreach (string s in args)
                logIt(s);
            //FDCalibration_V3.exe -single -inifile=E:\Works\Calibration\FDCalibration_V3\Release\calibration.ini
            String exePath = System.IO.Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "FDCalibration_V3.exe");
            if (System.IO.File.Exists(exePath))
            {
                int exitcode = 0;
                string sourceFile = System.IO.Path.Combine(getApstTmpFolder(), "cali_031121.ini");
                string sParm = $"-single -inifile={ sourceFile }";
                string[] rets = runExe(exePath, sParm, out exitcode, 120000);
                if (exitcode == 0)
                {
                    int ncount = 0;
                    foreach(string s in rets)
                    {
                        if (s.StartsWith("[detected][label="))
                        {
                            ncount++;
                        }
                    }
                    if (ncount > 0 && System.IO.File.Exists(sourceFile))
                    {
                        string destFile = getCalibrationFilename();
                        if (_arg.Parameters.ContainsKey("inifile"))
                        {
                            destFile = System.Environment.ExpandEnvironmentVariables(_arg.Parameters["inifile"]);
                        }
                        try
                        {
                            System.IO.File.Copy(sourceFile, destFile, true);
                        }
                        finally
                        {
                            System.IO.File.Delete(sourceFile);
                        }
                    }
                }
            }

        }

        static public void logIt(string s)
        {
            Console.WriteLine($"[{DateTime.Now}]: {s}");
            System.Diagnostics.Trace.WriteLine($"[calbrationshell]: {s}");
        }

        static public string getApstTmpFolder()
        {
            string s = System.IO.Path.Combine(System.Environment.ExpandEnvironmentVariables("%apsthome%"), "temp");
            try { System.IO.Directory.CreateDirectory(s); }
            catch (Exception) { }
            return s;
        }


        static public string[] runExe(string exeFilename, string args, out int exitCode, int timeout = 60 * 1000, string workingDir = "")
        {
            List<string> ret = new List<string>();
            exitCode = -1;
            try
            {
                if (System.IO.File.Exists(exeFilename))
                {
                    logIt(string.Format("[runEXE]: {0} arg={1}", exeFilename, args));
                    System.Diagnostics.Process p = new System.Diagnostics.Process();
                    p.StartInfo.FileName = exeFilename;
                    p.StartInfo.Arguments = args;
                    p.StartInfo.CreateNoWindow = true;
                    p.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                    if (!string.IsNullOrEmpty(workingDir))
                        p.StartInfo.WorkingDirectory = workingDir;
                    p.StartInfo.UseShellExecute = false;
                    p.StartInfo.RedirectStandardOutput = true;
                    p.OutputDataReceived += (sender, e) =>
                    {
                        if (!string.IsNullOrEmpty(e.Data))
                        {
                            logIt(string.Format("[runEXE]: {0}", e.Data));
                            ret.Add(e.Data);
                        }
                    };
                    p.Start();
                    p.BeginOutputReadLine();
                    p.WaitForExit(timeout);
                    if (!p.HasExited)
                    {
                        p.Kill();
                        exitCode = -1460;
                    }
                    else
                        exitCode = p.ExitCode;
                    logIt(string.Format("[runEXE]: exit code={0}", exitCode));
                }
                else
                    exitCode = 2;
            }
            catch (Exception) { }
            return ret.ToArray();
        }
        static public string getCalibrationFilename()
        {
            return System.IO.Path.Combine(System.Environment.ExpandEnvironmentVariables("%apsthome%"), "calibration.ini");
        }
    }
}
