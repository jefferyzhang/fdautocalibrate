﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Serialization;
using System.Diagnostics;
using System.Collections;

namespace HubSettings
{
    class ConfigInfo
    {
        static public void logIt(String msg)
        {
            String ss =$"[changecalibration]: {msg}";
            Trace.WriteLine(ss);
            Console.WriteLine(ss);
        }

        public static string[] runExe(string exeFilename, string param, out int exitCode, System.Collections.Specialized.StringDictionary env = null, int timeout = 180 * 1000)
        {
            List<string> ret = new List<string>();
            exitCode = 1;
            logIt($"[runExe]: ++ exe={exeFilename}, param={param}");
            try
            {
                if (System.IO.File.Exists(exeFilename))
                {
                    System.Threading.AutoResetEvent ev = new System.Threading.AutoResetEvent(false);
                    Process p = new Process();
                    p.StartInfo.FileName = exeFilename;
                    p.StartInfo.Arguments = param;
                    p.StartInfo.UseShellExecute = false;
                    p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    p.StartInfo.RedirectStandardOutput = true;
                    p.StartInfo.CreateNoWindow = true;
                    if (env != null && env.Count > 0)
                    {
                        foreach (DictionaryEntry de in env)
                        {
                            p.StartInfo.EnvironmentVariables.Add(de.Key as string, de.Value as string);
                        }
                    }
                    p.OutputDataReceived += (obj, args) =>
                    {
                        if (!string.IsNullOrEmpty(args.Data))
                        {
                            logIt($"[runExe]: {args.Data}");
                            ret.Add(args.Data);
                        }
                        if (args.Data == null)
                            ev.Set();
                    };
                    p.Start();
                    p.BeginOutputReadLine();
                    if (p.WaitForExit(timeout))
                    {
                        ev.WaitOne(timeout);
                        if (!p.HasExited)
                        {
                            exitCode = 1460;
                            p.Kill();
                        }
                        else
                            exitCode = p.ExitCode;
                    }
                    else
                    {
                        if (!p.HasExited)
                        {
                            p.Kill();
                        }
                        exitCode = 1460;
                    }
                }
            }
            catch (Exception ex)
            {
                logIt($"[runExe]: {ex.Message}");
                logIt($"[runExe]: {ex.StackTrace}");
            }
            logIt($"[runExe]: -- ret={exitCode}");
            return ret.ToArray();
        }

        private string _configFile;
        public string ConfigFile
        {
            get
            {
                if (string.IsNullOrEmpty(_configFile))
                {
                    _configFile = Environment.ExpandEnvironmentVariables("%APSTHOME%\\fdcalibrationv3config.xml");
                    if (!File.Exists(_configFile))
                    {
                        _configFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "fdcalibrationv3config.xml");
                    }
                }
                return _configFile;
            }
        }

        public void ChangeConfigXML(int Type)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Hubs));
            /* If the XML document has been altered with unknown
            nodes or attributes, handle them with the
            UnknownNode and UnknownAttribute events.*/
            serializer.UnknownNode += new
            XmlNodeEventHandler(serializer_UnknownNode);
            serializer.UnknownAttribute += new
            XmlAttributeEventHandler(serializer_UnknownAttribute);

            Hubs po;

            // A FileStream is needed to read the XML document.
            using (FileStream fs = new FileStream(ConfigFile, FileMode.Open))
            {    // Declare an object variable of the type to be deserialized.
                /* Use the Deserialize method to restore the object's state with
                data from the XML document. */
                po = (Hubs)serializer.Deserialize(fs);
            }

            //<hub vid="2109" pid="2811" usb30="VID_2109&amp;PID_8110" portcnt="4" comment="StartTech 4 Port Hub">
            foreach (var hh in po.Hub)
            {
                if (string.Compare(hh.Vid, "24FF", true) == 0 &&
                    string.Compare(hh.Pid, "8013", true) == 0 &&
                    string.Compare(hh.Usb30, "VID_24FF&PID_8013", true) == 0 /* &&
                    string.Compare(hh.Portcnt, "5", true) == 0 &&
                    string.Compare(hh.Comment, "Acroname USB 3.0 Hub(8Ports)", true) == 0*/)
                {
                    if (hh.Hubindex.Count > 0)
                    {
                        if (Type == 1)
                        {
                            hh.Hubindex[0].Index = "3";
                        }
                        else
                        {
                            hh.Hubindex[0].Index = "4";
                        }
                    }
                    //hh.Hubindex.Clear();
                    //if (Type == 0)
                    //{
                    //    var items = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<Dictionary<string, string>>(Properties.Settings.Default.HubSet0);
                    //    foreach (var it in items)
                    //    {
                    //        Hubindex hui = new Hubindex();
                    //        hui.Index = it.Key;
                    //        hui.Label = it.Value;
                    //        hh.Hubindex.Add(hui);
                    //    }
                    //}
                    //else
                    //{
                    //    var items = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<Dictionary<string, string>>(Properties.Settings.Default.HubSet1);
                    //    foreach (var it in items)
                    //    {
                    //        Hubindex hui = new Hubindex();
                    //        hui.Index = it.Key;
                    //        hui.Label = it.Value;
                    //        hh.Hubindex.Add(hui);
                    //    }
                    //}
                }
            }

            //Create our own namespaces for the output
            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();

            //Add an empty namespace and empty value
            ns.Add("", "");

            TextWriter writer = new StreamWriter(ConfigFile);
            serializer.Serialize(writer, po, ns);
            writer.Close();
        }

        private void serializer_UnknownAttribute(object sender, XmlAttributeEventArgs e)
        {
            System.Xml.XmlAttribute attr = e.Attr;
            logIt($"Unknown attribute {attr.Name} ={ attr.Value}");
        }

        private void serializer_UnknownNode(object sender, XmlNodeEventArgs e)
        {
            logIt($"Unknown Node:{e.Name}\t{e.Text}");
        }
    }
}
