﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HubSettings
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
            cmbGoup.SelectedIndex = Properties.Settings.Default.CurrentIndex;
        }

        
        private void button1_Click(object sender, EventArgs e)
        {
            int index = cmbGoup.SelectedIndex;
            if (index == 0 || index == 1)
            {
                ConfigInfo configInfo = new ConfigInfo();
                configInfo.ChangeConfigXML(index);
            }
            String sinfo = index == 0 ? "Set 1":"Set 2";
            //MessageBox.Show($"Current setting {sinfo}");
            Properties.Settings.Default.CurrentIndex = index;
            Properties.Settings.Default.Save();

            //< item cmd = "%apsthome%\FDCalibration_V3.exe" param = "-single -inifile=C:\ProgramData\FutureDial\CMC\calibration.ini" wait = "true" />
            int exitcode = 0;
            ConfigInfo.runExe(Environment.ExpandEnvironmentVariables("%apsthome%\\FDCalibration_V3.exe"), @"-single -inifile=C:\ProgramData\FutureDial\CMC\calibration.ini", out exitcode);
            if (exitcode != 0)
            {
                MessageBox.Show($"Current setting {sinfo} failed. Please reboot retry.");
            }
            Close();
        }
    }
}
