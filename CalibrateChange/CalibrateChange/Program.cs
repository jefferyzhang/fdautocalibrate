﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CalibrateChange
{
    class Program
    {
        static void Main(string[] args)
        {
            ConfigInfo configInfo = new ConfigInfo();
            int index = 0;

            configInfo.ChangeConfigXML(1);
            Properties.Settings.Default.CurrentIndex = index;
            Properties.Settings.Default.Save();

            int exitcode = 0;
            ConfigInfo.runExe(Environment.ExpandEnvironmentVariables("%apsthome%\\FDCalibration_V3.exe"), @"-single -inifile=C:\ProgramData\FutureDial\CMC\calibration.ini", out exitcode);
            if (exitcode != 0)
            {
                ConfigInfo.logIt($"Current setting {index} failed. Please reboot retry.");
            }
        }
    }
}
