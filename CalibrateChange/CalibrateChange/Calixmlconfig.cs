﻿ /* 
    Licensed under the Apache License, Version 2.0
    
    http://www.apache.org/licenses/LICENSE-2.0
    */
using System;
using System.Xml.Serialization;
using System.Collections.Generic;
namespace CalibrateChange
{
    [XmlRoot(ElementName = "hubindex")]
    public class Hubindex
    {
        [XmlAttribute(AttributeName = "index")]
        public string Index { get; set; }
        [XmlAttribute(AttributeName = "label")]
        public string Label { get; set; }
        [XmlElement(ElementName = "hub")]
        public Hub Hub { get; set; }
        [XmlAttribute(AttributeName = "deivce")]
        public string Deivce { get; set; }
    }

    [XmlRoot(ElementName = "hub")]
    public class Hub
    {
        [XmlElement(ElementName = "hubindex")]
        public List<Hubindex> Hubindex { get; set; }
        [XmlAttribute(AttributeName = "vid")]
        public string Vid { get; set; }
        [XmlAttribute(AttributeName = "pid")]
        public string Pid { get; set; }
        [XmlAttribute(AttributeName = "usb30")]
        public string Usb30 { get; set; }
        [XmlAttribute(AttributeName = "portcnt")]
        public string Portcnt { get; set; }
        [XmlAttribute(AttributeName = "comment")]
        public string Comment { get; set; }
    }

    [XmlRoot(ElementName = "hubs")]
    public class Hubs
    {
        [XmlElement(ElementName = "hub")]
        public List<Hub> Hub{ get; set; }
        [XmlAttribute(AttributeName = "version")]
        public string Version { get; set; }


    }

}
