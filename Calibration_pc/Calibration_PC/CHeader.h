#pragma once

#include "CUsbhub.h"

void logIt(TCHAR* fmt, ...);
void PrintError(DWORD dwError, __in PCTSTR sFile, ULONG   Line);

#define ENTER_FUNCTION()	logIt(_T("%S ++\n"), __FUNCTION__)
#define EXIT_FUNCTION()		logIt(_T("%S --\n"), __FUNCTION__)
#define EXIT_FUNCTRET(ret)		logIt(_T("%S -- return=%d\n"), __FUNCTION__, ret)
#define PRINT_ERR(error)	PrintError(error, _T(__FILE__), __LINE__)

int GetDeviceInstanceID(TCHAR *sName, PDEVPROPKEY pdevkey, PBYTE pdata, DWORD &nLen, DWORD &nType);
int countSubstring(const CString& str, const CString& sub);
int GetDeviceCommonSymbl(TCHAR *sName, PDEVPROPKEY pdevkey, PBYTE pdata, DWORD &nLen, DWORD &nType);