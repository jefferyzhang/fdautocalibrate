﻿#include "stdafx.h"
#include <map>
#include <list>
#include <VersionHelpers.h>
#include "CUsbhub.h"
#include "CHeader.h"
#include "CAllHubs.h"

#include "EZUSB.h"
#include "ConfigInfo.h"
using namespace System;
using namespace System::IO;
using namespace System::Diagnostics;
using namespace System::Text::RegularExpressions;
using namespace System::Collections::Specialized;
using namespace System::Configuration::Install;

HANDLE hEventWaitPlugin = CreateEvent(NULL, TRUE, FALSE, NULL);
TCHAR sDongleInstanceID[MAX_PATH] = { 0 };
void logIt(System::String^ msg)
{
	System::String^ ss = System::String::Format(L"[CALIBRATION]{0}", msg);
	System::Diagnostics::Trace::WriteLine(ss);
	Console::WriteLine(ss);
}


void USBEventHandler(Object^ sender, EventArrivedEventArgs^ e)
{
	if (e->NewEvent->ClassPath->ClassName == "__InstanceCreationEvent")
	{
		logIt("USB plug in time:" + DateTime::Now);
	}
	else if (e->NewEvent->ClassPath->ClassName == "__InstanceDeletionEvent")
	{
		logIt("USB plug out time:" + DateTime::Now);
	}
	for each(USBControllerDevice^ Device in EZUSB::WhoUSBControllerDevice(e))
	{//Dependent HID\\VID_3689&PID_8762\\7&17E8BD4A&0&0000
		logIt("\tAntecedent:" + Device->Antecedent);
		logIt("\tDependent:" + Device->Dependent);
		String^ sInstanceid = Device->Dependent->ToUpper()->Replace("\\\\", "\\");
		//String^ sInst = String::Format("USB\\VID_{0:X4}&PID_{1:X4}", DongleVID, DonglePID);
		//if (sInstanceid->StartsWith(sInst))
		{
			extern TCHAR sDongleInstanceID[MAX_PATH];
			using namespace Runtime::InteropServices;
			const wchar_t* lpInstanceid =
				(const wchar_t*)(Marshal::StringToHGlobalUni(sInstanceid)).ToPointer();
			ZeroMemory(sDongleInstanceID, MAX_PATH);
			_sntprintf_s(sDongleInstanceID, MAX_PATH, lpInstanceid);

			SetEvent(hEventWaitPlugin);
		}

	}
}

void Usage()
{
	Console::WriteLine("command Line:");
	Console::WriteLine("\t -inifile=   (full path ini file name. Must)");
	Console::WriteLine("\t -startlabel=   [modified for label. default is 1]");
	Console::WriteLine("\t -allports   [get PC all available hub, without param waiting for usb device plug in for one port ]");
	Console::WriteLine("\t -timeout=   [wait for device plug in, s, default 60s]");
}

String^ GetHubIDsFromXML(CHub^ hub)
{
	ENTER_FUNCTION();
	String^ sID = String::Format("usb#vid_{0}&pid_{1}#", hub->vid, hub->pid);
	if (hub->IsSupportUsb30()) sID = String::Format("{0}|usb#{1}#", sID, hub->usb30pidvid);
	for each(auto ss in hub->hubports)
	{
		if (ss.Value->GetType() == CHub::typeid)
		{
			CHub^ temp = (CHub^)ss.Value;
			String^ ss = GetHubIDsFromXML(temp);
			if (sID->IndexOf(ss) < 0)
				sID += "|" + ss;
		}
	}
	return sID;
}

BOOL SamePIDHUB2_0and3_0(CHub^ hub)
{
	BOOL bRet = FALSE;
	if (!String::IsNullOrEmpty(hub->usb30pidvid)) {
		String^ ss = String::Format("VID_{0X4}&PID_{1X4}", hub->vid, hub->pid);
		bRet = String::Compare(ss, hub->usb30pidvid, true) == 0;
	}

	return bRet;
}

void GetHubCalibration(CAllHubs* phubs, CHub^ pHub, int startindex, int &Count, int labelcount);
BOOL FindLocalPaths(CHub^ pHub, std::map<CString, CUsbhub*> lochubinfo, std::list<CString> &localpaths);
int ConfigUsbPort(String^ sloc);
DWORD CalibrateStaticHub(int Startlabel, CAllHubs* pallhubs)
{
	ENTER_FUNCTION();
	CConfigInfo^ info = gcnew CConfigInfo();
	DWORD dwRet = info->LoadConfigXml();
	if (dwRet != ERROR_SUCCESS) return dwRet;

	if (info->hubs->Count == 0) return ERROR_NOT_FOUND;


	int startindex = Startlabel - 1;
	int findsupporthubcount = 1;

	for each(auto hub in info->hubs)
	{
		String^ sID = GetHubIDsFromXML(hub);
		System::Diagnostics::Trace::WriteLine(sID);
		Regex^ pidvidregex = gcnew Regex(sID, RegexOptions::IgnoreCase | RegexOptions::Compiled);
		std::map<CString, CUsbhub*> lochubinfo;
		std::list<CString> locpathlist;
		for (std::map<CString, CUsbhub*>::iterator iter = pallhubs->LocaPathHubInfo().begin(); iter != pallhubs->LocaPathHubInfo().end(); iter++)
		{
			Match^ m = pidvidregex->Match(gcnew String(iter->second->HubName));
			if (m->Success)
			{
				locpathlist.push_back(iter->first);
				lochubinfo.insert(std::map<CString, CUsbhub*>::value_type(iter->second->GetHubLocationPaths(), iter->second));
			}
		}

		if (lochubinfo.size()>0 && FindLocalPaths(hub, lochubinfo, locpathlist))
		{
			locpathlist.sort();
			int minsp = countSubstring(locpathlist.front(), _T("#"));;
			if (!IsWindows7SP1OrGreater() && SamePIDHUB2_0and3_0(hub)) {
				BOOL bRemove = false;
				for each(auto it in locpathlist) {
					int tt = countSubstring(it, _T("#"));
					if (tt < minsp) {
						minsp = tt;
					}
					else if (tt == minsp) {
						//delete
						bRemove = true;
					}
					if (bRemove) {
						locpathlist.remove(it);
					}
				}
			}
			using namespace Runtime::InteropServices;
			logIt(_T("[hubcount][hubindex=%d, hubcomment=%s]\n"), findsupporthubcount++, (const wchar_t*)(Marshal::StringToHGlobalUni(hub->sComment)).ToPointer());
			for each(auto lp in locpathlist)
			{
				int nCount = 0;
				int tt = countSubstring(lp, _T("#"));
				if (tt == minsp) { startindex = Startlabel - 1; }
				else continue;

				hub->SetLocationPath(gcnew String(lp.GetString()));
				GetHubCalibration(pallhubs, hub, startindex, nCount, startindex+1);
				logIt(_T("[count][count=%d]\n"), nCount);
				startindex += nCount;
			}
		}
		else
		{
			logIt(_T("[hubcount]Not find match hubs.\n"));
		}
	}
	return dwRet;
}

int main(array<System::String ^> ^args)
{
	extern TCHAR sIniFileName[MAX_PATH];
	_sntprintf_s(sIniFileName, MAX_PATH, _T("E:\\temp\\calibration.ini"));
	int nStartLabel = 1;

	logIt(String::Format("{0} start: ++ version: {1}",
		Path::GetFileName(Process::GetCurrentProcess()->MainModule->FileName),
		Process::GetCurrentProcess()->MainModule->FileVersionInfo->FileVersion));


	EZUSB^ ezusb = gcnew EZUSB();
	ezusb->AddUSBEventWatcher(gcnew EventArrivedEventHandler(USBEventHandler), nullptr, TimeSpan(0, 0, 1));

	InstallContext^ myInstallContext = gcnew InstallContext(nullptr, args);
	if (myInstallContext->IsParameterTrue("debug"))
	{
		logIt("Wait any key to dubug, Please attach...\n");
		System::Console::ReadKey();
	}
	if (myInstallContext->IsParameterTrue("help"))
	{
		Usage();
		return ERROR_SUCCESS;
	}

	int g_timeout=60000;

	if (myInstallContext->Parameters->ContainsKey("timeout"))
	{
		g_timeout = Convert::ToInt32(myInstallContext->Parameters["timeout"]);
	}

	if (myInstallContext->Parameters->ContainsKey("inifile"))
	{
		String^ sFN = myInstallContext->Parameters["inifile"];
		using namespace Runtime::InteropServices;
		const wchar_t* lpIniFileName =
			(const wchar_t*)(Marshal::StringToHGlobalUni(sFN)).ToPointer();
		ZeroMemory(sIniFileName, MAX_PATH);
		_sntprintf_s(sIniFileName, MAX_PATH, lpIniFileName);
	}

	if (myInstallContext->Parameters->ContainsKey("startlabel"))
	{
		nStartLabel = Convert::ToInt32(myInstallContext->Parameters["startlabel"]);
	}
	
	CString sPath;
	BOOL bDeviceArrived = FALSE;
	CAllHubs allhubs;

	if (!myInstallContext->IsParameterTrue("allports"))
	{
		if (WaitForSingleObject(hEventWaitPlugin, g_timeout) == WAIT_OBJECT_0) {
			bDeviceArrived = TRUE;
			int iretry = 0;
			while (allhubs.PluginLocationpaths(sDongleInstanceID, sPath) != ERROR_SUCCESS) {
				Sleep(1000);
				if (iretry++ > 10) break;
			}
		}
	}

	allhubs.ListAllUsbHubLocation();

	if (myInstallContext->Parameters->ContainsKey("single"))
	{
		return CalibrateStaticHub(nStartLabel, &allhubs);
	}

	if (bDeviceArrived) {
		int pid = 0, vid = 0;
		int hubport = 0;
		//USB\VID_413C&PID_2113&MI_00\7&33A5BE17&0&0000
		Regex^ pidvid = gcnew Regex("^USB\\\\VID_([0-9a-fA-F]+)&PID_([0-9a-fA-F]+)", RegexOptions::IgnoreCase);
		Match^ m = pidvid->Match(gcnew String(sDongleInstanceID));
		if (m->Success) {
			vid = Convert::ToInt32(m->Groups[1]->Value, 16);
			pid = Convert::ToInt32(m->Groups[2]->Value, 16);
		}
		if (!sPath.IsEmpty()) {
			//PCIROOT(0)#PCI(1400)#USBROOT(0)#USB(11)#USB(4)#USBMI(0)
			Regex^ lcport = gcnew Regex("^(.*?)#USB\\((\\d+)\\)$", RegexOptions::IgnoreCase);
			Match^ lp = lcport->Match(gcnew String(sPath));
			if (lp->Success) {
				String^ location = lp->Groups[1]->Value;
				using namespace Runtime::InteropServices;
				const wchar_t* lplocpath =
					(const wchar_t*)(Marshal::StringToHGlobalUni(location)).ToPointer();
				sPath = lplocpath;
				hubport = Convert::ToInt32(lp->Groups[2]->Value, 10);
			}
		}

		allhubs.PrintHubCalibrate(nStartLabel, pid, vid, sPath, hubport);
	}
	else {
		allhubs.PrintHubCalibrate(nStartLabel);
	}

    return 0;
}
