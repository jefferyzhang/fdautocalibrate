#include "stdafx.h"
#include "cheader.h"
#include <VersionHelpers.h>
#include <SetupAPI.h>
#include <devpkey.h>
#include <map>
#include <set>
#include <list>
#include "CAllHubs.h"


#pragma comment(lib,"Setupapi.lib")

#include "ConfigInfo.h"
#using <system.dll>

#define HUBINDEXCONNECTDONGLEPORT		1

using namespace System;
using namespace System::Text::RegularExpressions;
using namespace System::Text;
using namespace Runtime::InteropServices;
using namespace System::Diagnostics;

// returns count of non-overlapping occurrences of 'sub' in 'str'
int countSubstring(const CString& str, const CString& sub)
{
	if (sub.GetLength() == 0) return 0;
	int count = 0;
	for (size_t offset = str.Find(sub); offset != -1;
		offset = str.Find(sub, offset + sub.GetLength()))
	{
		++count;
	}
	return count;
}

int ConfigUsbPort(String^ sloc)
{
	int nret = -1;
	if (IsWindows7SP1OrGreater()) return nret;
	System::Collections::Specialized::NameValueCollection^ nvc = System::Configuration::ConfigurationManager::AppSettings;
	if (nvc == nullptr) return nret;
	if (nvc->AllKeys->Length == 0) return nret;
	array<String^>^ values = gcnew array<String^>(nvc->AllKeys->Length);
	int i = 0;
	for (; i < nvc->AllKeys->Length; i++) {
		values[i] = nvc->Get(nvc->AllKeys[i]);
	}
	Regex^ regx = gcnew Regex(".*?#USB\\((\\d+)\\)", RegexOptions::IgnoreCase);
	Match^ m = regx->Match(sloc);
	if (m->Success)
	{
		Trace::WriteLine(m->Groups[1]->Value);
		if (Array::IndexOf(nvc->AllKeys, m->Groups[1]->Value) >= 0)
		{
			Trace::WriteLine("find usb2.0");
			nret = USB200;
		}
		else if (Array::IndexOf(values, m->Groups[1]->Value) >= 0)
		{
			Trace::WriteLine("find usb3.0");
			nret = USB300;
		}
	}
	return nret;
}

void GetHubCalibration(CAllHubs* phubs, CHub^ pHub, int startindex, int &Count, int labelcount)
{
	extern TCHAR sIniFileName[MAX_PATH];
	ENTER_FUNCTION();
	const wchar_t* lpLocpth =
		(const wchar_t*)(Marshal::StringToHGlobalUni(pHub->locationpath)).ToPointer();

	if (phubs->LocaPathHubInfo().find(lpLocpth) == phubs->LocaPathHubInfo().end()) return;
	CString sHubName = phubs->LocaPathHubInfo()[lpLocpth]->HubName;
	sHubName = sHubName.TrimLeft(_T("\\\\?\\")).TrimLeft(_T("\\\\.\\"));

	BOOL bUsb30 = pHub->IsCurrentUSB30(gcnew String(sHubName.GetString()));
	int hubType = ConfigUsbPort(pHub->locationpath);
	if (hubType == USB300) bUsb30 = TRUE;

	for each(auto ss in pHub->hubports)
	{
		if (ss.Value->GetType() == CHub::typeid)
		{
			CHub^ temp = (CHub^)ss.Value;
			temp->SetLocationPath(String::Format("{0}#USB({1})", pHub->locationpath, ss.Key));
			GetHubCalibration(phubs, temp, startindex, Count, labelcount);
		}
		else
		{
			if (String::IsNullOrEmpty(ss.Value->ToString()) || String::Compare(ss.Value->ToString(), gcnew String("0"), true) == 0) continue;
			CString strLab; 
			strLab.Format(_T("%d"), startindex + Convert::ToInt16(ss.Value));
			logIt(_T("[detected][label=%s]\n"), strLab);
			CString sLocatpath;
			sLocatpath.Format(_T("%d@%s"), ss.Key, sHubName);
			if (!bUsb30)
			{
				if (!WritePrivateProfileString(_T("label"), strLab, sLocatpath, sIniFileName))
				{
					PrintError(GetLastError(), _T(__FILE__), __LINE__);
				}
				if (!WritePrivateProfileString(_T("label_2.0"), strLab, sLocatpath, sIniFileName))
				{
					PrintError(GetLastError(), _T(__FILE__), __LINE__);
				}
			}
			else
			{
				if (!WritePrivateProfileString(_T("label_3.0"), strLab, sLocatpath, sIniFileName))
				{
					PrintError(GetLastError(), _T(__FILE__), __LINE__);
				}
			}

			sLocatpath.Format(_T("%d@%s"), ss.Key, lpLocpth);
			if (!bUsb30)
			{
				if (!WritePrivateProfileString(_T("locationpaths"), strLab, sLocatpath, sIniFileName))
				{
					PrintError(GetLastError(), _T(__FILE__), __LINE__);
				}
				if (!WritePrivateProfileString(_T("locationpaths_2.0"), strLab, sLocatpath, sIniFileName))
				{
					PrintError(GetLastError(), _T(__FILE__), __LINE__);
				}
			}
			else
			{
				if (!WritePrivateProfileString(_T("locationpaths_3.0"), strLab, sLocatpath, sIniFileName))
				{
					PrintError(GetLastError(), _T(__FILE__), __LINE__);
				}
			}
			if (!bUsb30)
				Count++;
		}
	}
	if (!bUsb30)
	{
		sHubName.Format(_T("%d"), Count + labelcount - 1);
		WritePrivateProfileString(_T("amount"), _T("labels"), sHubName, sIniFileName);
	}
}

BOOL MatchLocalPaths(CHub^ pHub, std::map<CString, CUsbhub*> lochubinfo)
{

	const wchar_t* lpLocpth =
		(const wchar_t*)(Marshal::StringToHGlobalUni(pHub->locationpath)).ToPointer();
	BOOL bRet = lochubinfo.find(lpLocpth) != lochubinfo.end();
	if (!bRet) return bRet;
	for each(auto ss in pHub->hubports)
	{
		if (ss.Value->GetType() == CHub::typeid)
		{
			CHub^ temp = (CHub^)ss.Value;
			bRet = MatchLocalPaths(temp, lochubinfo);
			if (!bRet) return bRet;
		}
	}
	return bRet;
}

BOOL FindLocalPaths(CHub^ pHub, std::map<CString, CUsbhub*> lochubinfo, std::list<CString> &localpaths)
{
	ENTER_FUNCTION();
	BOOL bmatched = TRUE;

	pHub->SetLocationPath(gcnew String(localpaths.front()));
	for each(auto ss in pHub->hubports)
	{
		if (ss.Value->GetType() == CHub::typeid)
		{
			CHub^ temp = (CHub^)ss.Value;
			String^ ssloc = String::Format("{0}#USB({1})", pHub->locationpath, ss.Key);
			temp->SetLocationPath(ssloc);
			const wchar_t* lpLocpth =
				(const wchar_t*)(Marshal::StringToHGlobalUni(ssloc)).ToPointer();
			bmatched = (std::find(localpaths.begin(), localpaths.end(), lpLocpth) != localpaths.end());
			if (!bmatched) break;
			bmatched = FindLocalPaths(temp, lochubinfo, localpaths);
			if (!bmatched) break;
		}
	}

	/*for (std::map<CString, CUsbhub*>::iterator iter = lochubinfo.begin(); iter != lochubinfo.end(); iter++)
	{
		pHub->SetLocationPath(gcnew String(iter->first.GetString()));

		String^ hubname = gcnew String(iter->second->HubName);

		if (MatchLocalPaths(pHub, lochubinfo))
		{
			bmatched = TRUE;
			if (pHub->IsCurrentUSB30(hubname))
			{
				localpaths.insert(localpaths.begin(), iter->first);
			}
			else
			{
				localpaths.push_back(iter->first);
			}
		}
	}*/
	return bmatched;
}

