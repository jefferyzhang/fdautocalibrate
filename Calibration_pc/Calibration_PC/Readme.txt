	Console::WriteLine("command Line:");
	Console::WriteLine("\t -inifile=   (full path ini file name. Must)");
	Console::WriteLine("\t -startlabel=   [modified for label. default is 1]");
	Console::WriteLine("\t -allports   [get PC all available hub, without param waiting for usb device plug in for one port ]");
	Console::WriteLine("\t -timeout=   [wait for device plug in, s, default 60s]");

	Console::WriteLine("\t -single   [get xml from fdcalibrationv3config.xml, app.config]");