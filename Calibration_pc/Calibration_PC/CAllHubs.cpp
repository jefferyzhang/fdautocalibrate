#include "stdafx.h"
#include <devpkey.h>
#include "CAllHubs.h"
#include "CHeader.h"
#include <list>


CAllHubs::CAllHubs()
{
}


CAllHubs::~CAllHubs()
{
	for each(auto item in locpathhubinfo)
	{
		if (item.second){
			delete item.second;
		}
	}
	locpathhubinfo.clear();
}


DWORD CAllHubs::PluginLocationpaths(CString sInstanceID, CString &parentLocation)
{
	BYTE pdata[4096] = { 0 };
	DWORD nLen = sizeof(pdata);
	DWORD nType = 0;
	DWORD dwRet(ERROR_SUCCESS);
	ENTER_FUNCTION();
	if ((dwRet = GetDeviceInstanceID((TCHAR *)sInstanceID.GetString(), (PDEVPROPKEY)&DEVPKEY_Device_LocationPaths, pdata, nLen, nType)) == ERROR_SUCCESS)
	{
		if (nType == DEVPROP_TYPE_STRING_LIST)
		{
			//add locationPaths PCIROOT(0)#PCI(1400)#USBROOT(0)#USB(11)#USB(4)#USBMI(0)

			parentLocation = (TCHAR *)pdata;
			logIt(_T("find plugin locationPaths=%ws"), parentLocation);
			int nFind = parentLocation.Find(_T("#USBMI("));
			if (nFind > 0) {
				parentLocation = parentLocation.Left(nFind);
			}
		}
	}
	else
	{
		PrintError(dwRet, _T(__FILE__), __LINE__);
	}
	return dwRet;
}

DWORD CAllHubs::CreateCalibrateFile(CUsbhub *PHub, CUsbport* PUsbPort, int indexlabel, BOOL bCompanion)
{
	DWORD dwRet(ERROR_SUCCESS);
	extern TCHAR sIniFileName[MAX_PATH];
	ENTER_FUNCTION();
	CString sLocatpath;
	int nCount = indexlabel;

	CString sHubName = PHub->HubName;
	sHubName = sHubName.TrimLeft(_T("\\\\?\\")).TrimLeft(_T("\\\\.\\"));
	CString strLab;
	strLab.Format(_T("%d"), indexlabel);
	int portnum = PUsbPort->GetHubPort();
	CString hubnameport;
	hubnameport.Format(_T("%d@%ws"), portnum, sHubName);
	if (!bCompanion)
	{
		logIt(_T("[detected][label=%s]\n"), strLab);
		if (!WritePrivateProfileString(_T("label"), strLab, hubnameport, sIniFileName))
		{
			dwRet = GetLastError();
			PrintError(dwRet, _T(__FILE__), __LINE__);
		}
	}

	if (PUsbPort->GetPortType()&USB200) {
		if (!WritePrivateProfileString(_T("label_2.0"), strLab, hubnameport, sIniFileName))
		{
			dwRet = GetLastError();
			PrintError(dwRet, _T(__FILE__), __LINE__);
		}
	}
	else if (PUsbPort->GetPortType()&USB300) {
		if (!WritePrivateProfileString(_T("label_3.0"), strLab, hubnameport, sIniFileName))
		{
			dwRet = GetLastError();
			PrintError(dwRet, _T(__FILE__), __LINE__);
		}
	}

	sLocatpath.Format(_T("%d@%s"), PUsbPort->GetHubPort(), PHub->GetHubLocationPaths());
	if (!bCompanion)
	{
		if (!WritePrivateProfileString(_T("locationpaths"), strLab, sLocatpath, sIniFileName))
		{
			dwRet = GetLastError();
			PrintError(dwRet, _T(__FILE__), __LINE__);
		}
	}

	if (PUsbPort->GetPortType()&USB200) {
		if (!WritePrivateProfileString(_T("locationpaths_2.0"), strLab, sLocatpath, sIniFileName))
		{
			dwRet = GetLastError();
			PrintError(dwRet, _T(__FILE__), __LINE__);
		}
	}
	else if (PUsbPort->GetPortType()&USB300) {
		if (!WritePrivateProfileString(_T("locationpaths_3.0"), strLab, sLocatpath, sIniFileName))
		{
			dwRet = GetLastError();
			PrintError(dwRet, _T(__FILE__), __LINE__);
		}
	}

	sLocatpath.Format(_T("%d"), indexlabel);
	WritePrivateProfileString(_T("amount"), _T("labels"), sLocatpath, sIniFileName);
	EXIT_FUNCTRET(dwRet);
	return dwRet;
}

DWORD CAllHubs::CreateCalibrateFile(CString hubname, int hubport, CString sLocation, int indexlabel, int porttype)
{
	DWORD dwRet(ERROR_SUCCESS);
	extern TCHAR sIniFileName[MAX_PATH];
	ENTER_FUNCTION();
	CString sLocatpath;
	int nCount = indexlabel;

	CString sHubName = hubname;
	sHubName = sHubName.TrimLeft(_T("\\\\?\\")).TrimLeft(_T("\\\\.\\"));
	CString strLab;
	strLab.Format(_T("%d"), indexlabel);
	sHubName.Format(_T("%d@%s"), hubport, sHubName);
	if (porttype&USB200) {
		if (!WritePrivateProfileString(_T("label_2.0"), strLab,sHubName, sIniFileName))
		{
			dwRet = GetLastError();
			PrintError(dwRet, _T(__FILE__), __LINE__);
		}
	}
	else if (porttype&USB300) {
		if (!WritePrivateProfileString(_T("label_3.0"), strLab, sHubName, sIniFileName))
		{
			dwRet = GetLastError();
			PrintError(dwRet, _T(__FILE__), __LINE__);
		}
	}

	sLocatpath.Format(_T("%d@%s"), hubport,sLocation);
	
	if (porttype&USB200) {
		if (!WritePrivateProfileString(_T("locationpaths_2.0"), strLab, sLocatpath, sIniFileName))
		{
			dwRet = GetLastError();
			PrintError(dwRet, _T(__FILE__), __LINE__);
		}
	}
	else if (porttype&USB300) {
		if (!WritePrivateProfileString(_T("locationpaths_3.0"), strLab, sLocatpath, sIniFileName))
		{
			dwRet = GetLastError();
			PrintError(dwRet, _T(__FILE__), __LINE__);
		}
	}
	EXIT_FUNCTRET(dwRet);
	return dwRet;
}

BOOL CAllHubs::FindLocPathsFromHubName(CString sHubName, CString &sLocpaths)
{
	BOOL bFind = FALSE;
	for each(auto item in locpathhubinfo)
	{
		CString ssHubName = item.second->HubName;
		ssHubName = ssHubName.TrimLeft(_T("\\\\?\\")).TrimLeft(_T("\\\\.\\"));
		if (sHubName.CompareNoCase(ssHubName) == 0) {
			sLocpaths = item.first;
			bFind = TRUE;
			break;
		}
	}
	EXIT_FUNCTRET(bFind);
	return bFind;
}


DWORD CAllHubs::PrintHubCalibrate(int nLabelStart)
{
	ENTER_FUNCTION();
	DWORD dwRet = ERROR_SUCCESS;
	for each(auto item in locpathhubinfo)
	{
		for each(auto port in item.second->USBPorts()) {
			if ((!port.second->PortConnected()) &&
				((port.second->GetPortType() & USB300)!=USB300)) {
				int label = nLabelStart++;
				CreateCalibrateFile(item.second, port.second, label);
				if (!port.second->CompanionHubName().IsEmpty() && port.second->CompanionHubPort() > 0) {
					if (item.second->HubName.CompareNoCase(port.second->CompanionHubName()) == 0) {
						CreateCalibrateFile(item.second->HubName, port.second->CompanionHubPort(), item.second->GetHubLocationPaths(), label, USB300);
					}
					else {
						CString sLocationpaths;
						FindLocPathsFromHubName(port.second->CompanionHubName(), sLocationpaths);
						CreateCalibrateFile(port.second->CompanionHubName(), port.second->CompanionHubPort(), sLocationpaths, label, USB300);
					}
				}
			}
		}
	}
	return dwRet;
}


DWORD CAllHubs::ListAllUsbHubLocation()
{
	ENTER_FUNCTION();
	HDEVINFO                            hDeviceInfo;
	SP_DEVICE_INTERFACE_DATA            _sdid;
	PSP_DEVICE_INTERFACE_DETAIL_DATA    _psdidd;
	DWORD dwLength;
	DWORD dwBytesX;
	DWORD dwRet = ERROR_SUCCESS;
	std::list<CUsbhub*> lstNoLocationpaths;

	hDeviceInfo = SetupDiGetClassDevs(&GUID_DEVINTERFACE_USB_HUB, NULL, NULL, DIGCF_PRESENT | DIGCF_DEVICEINTERFACE);
	if (hDeviceInfo != INVALID_HANDLE_VALUE)
	{
		_sdid.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);
		for (int i = 0; SetupDiEnumDeviceInterfaces(hDeviceInfo, NULL, &GUID_DEVINTERFACE_USB_HUB, i, &_sdid); i++)
		{
			SetupDiGetDeviceInterfaceDetail(hDeviceInfo, &_sdid, NULL, 0, &dwBytesX, NULL);
			dwLength = dwBytesX;
			_psdidd = (PSP_DEVICE_INTERFACE_DETAIL_DATA)GlobalAlloc(GPTR, dwLength);
			if (_psdidd == NULL)
			{
				continue;
			}

			_psdidd->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);
			if (!SetupDiGetDeviceInterfaceDetail(hDeviceInfo, &_sdid, _psdidd, dwLength, &dwBytesX, NULL))
			{
				GlobalFree(_psdidd);
				continue;
			}

			CUsbhub* pinfo = new CUsbhub();
			if (pinfo == NULL) continue;

			pinfo->HubName = _psdidd->DevicePath;
			GlobalFree(_psdidd);
			pinfo->HubExtraInfo();
			if (pinfo->HubLocationPaths() != ERROR_SUCCESS) {
				//delete pinfo;
				lstNoLocationpaths.push_back(pinfo);
				continue;
			}

			if (pinfo->HubPortCount() != ERROR_SUCCESS) {
				delete pinfo;
				continue;
			}

			if (pinfo != NULL)
			{
				pinfo->Get_USB_HUB_CAPABILITIES_EX();
				pinfo->Get_USB_HUB_INFORMATION_EX();
				pinfo->ListUsbPorts();
				locpathhubinfo[pinfo->GetHubLocationPaths()] = pinfo;
				pinfo->CloseUsbHub();
			}
		}
	}
	else
	{
		dwRet = GetLastError();
		PrintError(dwRet, _T(__FILE__), __LINE__);
	}
	if (hDeviceInfo != INVALID_HANDLE_VALUE) {
		SetupDiDestroyDeviceInfoList(hDeviceInfo);
	}
	return dwRet;
}

DWORD CAllHubs::HubParent(CString HubName, CString &sParent) {
	BYTE pdata[4096] = { 0 };
	DWORD nLen = sizeof(pdata);
	DWORD nType = 0;
	DWORD dwRet(ERROR_SUCCESS);
	ENTER_FUNCTION();
	if ((dwRet = GetDeviceCommonSymbl((TCHAR *)HubName.GetString(), (PDEVPROPKEY)&DEVPKEY_Device_Parent, pdata, nLen, nType)) == ERROR_SUCCESS)
	{
		sParent = (TCHAR *)pdata;
	}
	else
	{
		PrintError(dwRet, _T(__FILE__), __LINE__);
	}
	return dwRet;
}

BOOL CAllHubs::FindCalcLoc(CUsbhub* hub) {
	BOOL bRet = FALSE;
//USB#VID_05E3&PID_0610#5&1f98154a&0&3#{f18a0e88-c30c-11d0-8815-00a0c906bed8}
//USB\VID_05E3&PID_0610\5&1F98154A&0&3

	for each(auto item in locpathhubinfo)
	{
		if (item.second->GetInstanceID().CompareNoCase(hub->GetParent()))
		{
			for each(auto port in item.second->USBPorts()) {
				if (port.second->GetDriverKey().CompareNoCase(item.second->GetDriverKey()) == 0) {
					//port.second->GetHubPort();
					CString sItem = item.second->GetHubPortLocationPaths(port.second->GetHubPort());
					locpathhubinfo[sItem] = hub;
					bRet = TRUE;
					break;
				}

			}
		}
		if (bRet) break;
	}

	return bRet;
}

void CAllHubs::CalcLocationPaths(std::list<CUsbhub *> hubs)
{
	if (hubs.size() > 0) {
		std::list<CUsbhub *>::iterator iter = hubs.begin();
		for (; iter != hubs.end(); ++iter) {
			if ((*iter)->GetParent().IsEmpty())
			{
				CString s;
				if (HubParent((*iter)->HubName, s) == ERROR_SUCCESS) {
					(*iter)->SetParent(s);

					if (FindCalcLoc(*iter)) {
						hubs.erase(iter);
						break;
					}
				}
			}
		}
	}
}

DWORD CAllHubs::PrintHubCalibrate(int nStartLabel, int pid, int vid, CString sPath, int hubport)
{
	ENTER_FUNCTION();
	DWORD dwRet = ERROR_SUCCESS;

	if (sPath.IsEmpty() || hubport == 0) {
		for each(auto item in locpathhubinfo)
		{
			for each(auto port in item.second->USBPorts()) {
				if (port.second->PortConnected() && port.second->PID()==pid && port.second->VID()==vid) {
					int label = nStartLabel++;
					CreateCalibrateFile(item.second, port.second, label);
					if (!port.second->CompanionHubName().IsEmpty() && port.second->CompanionHubPort() > 0) {
						if (item.second->HubName.CompareNoCase(port.second->CompanionHubName()) == 0) {
							CreateCalibrateFile(item.second->HubName, port.second->CompanionHubPort(), item.second->GetHubLocationPaths(), label, (port.second->GetPortType()&USB300) ? USB200 : USB300);
						}
						else {
							CString sLocationpaths;
							FindLocPathsFromHubName(port.second->CompanionHubName(), sLocationpaths);
							CreateCalibrateFile(port.second->CompanionHubName(), port.second->CompanionHubPort(), sLocationpaths, label, (port.second->GetPortType()&USB300) ? USB200 : USB300);
						}
					}
				}
			}
		}
	}
	else {
		if (locpathhubinfo.find(sPath) == locpathhubinfo.end()) {
			PrintError(ERROR_NOT_FOUND, _T(__FILE__), __LINE__);
			return ERROR_NOT_FOUND;
		}
		CUsbhub * phub = locpathhubinfo[sPath];
		if (phub->USBPorts().find(hubport) == phub->USBPorts().end()) {
			PrintError(ERROR_RANGE_NOT_FOUND, _T(__FILE__), __LINE__);
			return ERROR_RANGE_NOT_FOUND;
		}
		CUsbport * pPort = phub->USBPorts()[hubport];
		CreateCalibrateFile(phub, pPort, nStartLabel);
		if (!pPort->CompanionHubName().IsEmpty() && pPort->CompanionHubPort() > 0) {
			if (phub->HubName.CompareNoCase(pPort->CompanionHubName()) == 0) {
				CreateCalibrateFile(phub->HubName, pPort->CompanionHubPort(), sPath, nStartLabel, (pPort->GetPortType()&USB300)?USB200:USB300);
			}
			else {
				CString sLocationpaths;
				FindLocPathsFromHubName(pPort->CompanionHubName(), sLocationpaths);
				CreateCalibrateFile(pPort->CompanionHubName(), pPort->CompanionHubPort(), sLocationpaths, nStartLabel, (pPort->GetPortType()&USB300) ? USB200 : USB300);
			}
		}
	}


	return dwRet;
}

CUsbhub* CAllHubs::FindCompanionHub(CUsbhub *hub, int port, int &companport) {
	CUsbhub* companhub = NULL;
	if (hub == NULL) return companhub;

	if (hub->PortCount > port)
	{
		CUsbport* usbport = hub->USBPorts()[port];
		if (usbport != NULL) {
			CString sHubName = usbport->CompanionHubName();
			companport = usbport->CompanionHubPort();
			for each(auto item in locpathhubinfo)
			{
				CString ssHubName = item.second->HubName;
				ssHubName = ssHubName.TrimLeft(_T("\\\\?\\")).TrimLeft(_T("\\\\.\\"));
				if (sHubName.CompareNoCase(ssHubName) == 0) {
					companhub = item.second;
					break;
				}
			}
		}
	}
	
	return companhub;
}