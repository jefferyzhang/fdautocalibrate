#pragma once
using namespace System;
using namespace System::Management;
using namespace System::Collections::Generic;

/// <summary>
/// USB控制设备类型
/// </summary>
ref class USBControllerDevice
{
public:
	/// <summary>
	/// USB控制器设备ID
	/// </summary>
	String^ Antecedent;
	/// <summary>
	/// USB即插即用设备ID
	/// </summary>
	String^ Dependent;

	USBControllerDevice(String^ Ant, String^ Dep)
	{
		Antecedent = Ant;
		Dependent = Dep;
	}
};

ref class EZUSB
{
private:
	/// <summary>
	/// USB插入事件监视
	/// </summary>
	ManagementEventWatcher^ insertWatcher = nullptr;
	/// <summary>
	/// USB拔出事件监视
	/// </summary>
	ManagementEventWatcher^ removeWatcher = nullptr;

public:
	EZUSB();
	/// <summary>
	/// 添加USB事件监视器
	/// </summary>
	/// <param name="usbInsertHandler">USB插入事件处理器</param>
	/// <param name="usbRemoveHandler">USB拔出事件处理器</param>
	/// <param name="withinInterval">发送通知允许的滞后时间</param>
	BOOL AddUSBEventWatcher(EventArrivedEventHandler^ usbInsertHandler, EventArrivedEventHandler^ usbRemoveHandler, TimeSpan withinInterval);

	/// <summary>
	/// 移去USB事件监视器
	/// </summary>
	void RemoveUSBEventWatcher();

	/// <summary>
	/// 定位发生插拔的USB设备
	/// </summary>
	/// <param name="e">USB插拔事件参数</param>
	/// <returns>发生插拔现象的USB控制设备ID</returns>
	static List<USBControllerDevice^>^ WhoUSBControllerDevice(EventArrivedEventArgs^ e);
};

