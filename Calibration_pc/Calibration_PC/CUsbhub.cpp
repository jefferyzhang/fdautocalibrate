#include "stdafx.h"
#include <devpkey.h>
#include "CUsbhub.h"
#include "CHeader.h"

#pragma comment(lib,"Setupapi.lib")

TCHAR sIniFileName[MAX_PATH] = { 0 };

int GetDeviceCommonSymbl(TCHAR *sName, PDEVPROPKEY pdevkey, PBYTE pdata, DWORD &nLen, DWORD &nType)
{
	int ret = ERROR_UNKNOWN_FEATURE;
	if (sName == NULL || _tcslen(sName) == 0)
		return ERROR_INVALID_PARAMETER;

	TCHAR symblName[1024] = { 0 };

	if ((_tcsncicmp(sName, _T("\\\\?\\"), 4) == 0) || (_tcsncicmp(sName, _T("\\\\.\\"), 4) == 0))
	{
		_stprintf_s(symblName, _T("%ws"), sName);
	}
	else
	{
		_stprintf_s(symblName, _T("\\\\?\\%ws"), sName);
	}

	HDEVINFO hDevInfo = SetupDiCreateDeviceInfoList(NULL, NULL);
	if (hDevInfo != INVALID_HANDLE_VALUE)
	{
		DWORD sz = 0;
		SP_DEVICE_INTERFACE_DATA devIntData;
		devIntData.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);
		if (SetupDiOpenDeviceInterface(hDevInfo, symblName, 0, &devIntData))
		{
			BYTE b[4096];
			ZeroMemory(b, sizeof(b));
			sz = 0;

			SP_DEVINFO_DATA devInfoData = { 0 };
			devInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
			SetupDiGetDeviceInterfaceDetail(hDevInfo, &devIntData, NULL, 0, &sz, &devInfoData);
			if (GetLastError() == ERROR_INSUFFICIENT_BUFFER)
			{
				if (SetupDiGetDeviceProperty(hDevInfo, &devInfoData, pdevkey, &nType, b, 2048, &sz, 0))
				{
					if (sz <= nLen) {
						memcpy_s(pdata, nLen, b, sz);
						ret = ERROR_SUCCESS;
					}
					else
						ret = ERROR_INSUFFICIENT_BUFFER;
					nLen = sz;
				}
				else
				{
					ret = GetLastError();
					PrintError(ret, _T(__FILE__), __LINE__);
				}
			}
			else
			{
				ret = GetLastError();
				PrintError(ret, _T(__FILE__), __LINE__);
			}
		}
		else
		{
			ret = GetLastError();
			PrintError(ret, _T(__FILE__), __LINE__);
		}

		SetupDiDestroyDeviceInfoList(hDevInfo);
	}
	else
	{
		ret = GetLastError();
		PrintError(ret, _T(__FILE__), __LINE__);
	}

	return ret;
}

int GetDeviceInstanceID(TCHAR *sName, PDEVPROPKEY pdevkey, PBYTE pdata, DWORD &nLen, DWORD &nType)
{
	int ret = ERROR_UNKNOWN_FEATURE;
	if (sName == NULL || _tcslen(sName) == 0)
		return ERROR_INVALID_PARAMETER;

	TCHAR symblName[1024] = { 0 };
	_stprintf_s(symblName, _T("%ws"), sName);

	HDEVINFO hDevInfo = SetupDiCreateDeviceInfoList(NULL, NULL);
	if (hDevInfo != INVALID_HANDLE_VALUE)
	{
		DWORD sz = 0;
		SP_DEVINFO_DATA devInfoData = { 0 };
		devInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
		if (SetupDiOpenDeviceInfo(hDevInfo, symblName, NULL,0, &devInfoData))
		{
			BYTE b[4096];
			ZeroMemory(b, sizeof(b));
			sz = 0;

			if (SetupDiGetDeviceProperty(hDevInfo, &devInfoData, pdevkey, &nType, b, 2048, &sz, 0))
			{
				if (sz <= nLen) {
					memcpy_s(pdata, nLen, b, sz);
					ret = ERROR_SUCCESS;
				}
				else
					ret = ERROR_INSUFFICIENT_BUFFER;
				nLen = sz;
			}
			else
			{
				ret = GetLastError();
				PrintError(ret, _T(__FILE__), __LINE__);
			}
		}
		else
		{
			ret = GetLastError();
			PrintError(ret, _T(__FILE__), __LINE__);
		}

		SetupDiDestroyDeviceInfoList(hDevInfo);
	}
	else
	{
		ret = GetLastError();
		PrintError(ret, _T(__FILE__), __LINE__);
	}

	return ret;
}

CUsbport::CUsbport() :portType(0), hubport(0)
{

}

CUsbport::~CUsbport()
{
	if (pConnectionInfoEx != NULL) {
		delete pConnectionInfoEx;
		pConnectionInfoEx = NULL;
	}
	if (pPortConnectorProps != NULL) {
		delete pPortConnectorProps;
		pPortConnectorProps = NULL;
	}

}

DWORD CUsbport::Get_USB_NODE_CONNECTION_INFORMATION_EX()
{
	ENTER_FUNCTION();
	DWORD dwRet(ERROR_SUCCESS);
	BOOL        success = FALSE;
	if (hHubDevice == INVALID_HANDLE_VALUE || hubport < 1)
	{
		dwRet = ERROR_INVALID_PARAMETER;
		return dwRet;
	}
	ULONG nBytesEx;
	PUSB_NODE_CONNECTION_INFORMATION_EX_V2 connectionInfoExV2 = NULL;
	//
	// Allocate space to hold the connection info for this port.
	// For now, allocate it big enough to hold info for 30 pipes.
	//
	// Endpoint numbers are 0-15.  Endpoint number 0 is the standard
	// control endpoint which is not explicitly listed in the Configuration
	// Descriptor.  There can be an IN endpoint and an OUT endpoint at
	// endpoint numbers 1-15 so there can be a maximum of 30 endpoints
	// per device configuration.
	//
	// Should probably size this dynamically at some point.
	//

	nBytesEx = sizeof(USB_NODE_CONNECTION_INFORMATION_EX) +
		(sizeof(USB_PIPE_INFO) * 30);

	pConnectionInfoEx = (PUSB_NODE_CONNECTION_INFORMATION_EX)malloc(nBytesEx);

	if (pConnectionInfoEx == NULL)
	{
		dwRet = ERROR_NOT_ENOUGH_MEMORY;
		PrintError(dwRet, _T(__FILE__), __LINE__);
		return dwRet;
	}

	connectionInfoExV2 = (PUSB_NODE_CONNECTION_INFORMATION_EX_V2)
		malloc(sizeof(USB_NODE_CONNECTION_INFORMATION_EX_V2));

	if (connectionInfoExV2 == NULL)
	{
		free(pConnectionInfoEx);
		dwRet = ERROR_NOT_ENOUGH_MEMORY;
		PrintError(dwRet, _T(__FILE__), __LINE__);
		return dwRet;
	}
	ULONG nBytes = 0;
	connectionInfoExV2->ConnectionIndex = hubport;
	connectionInfoExV2->Length = sizeof(USB_NODE_CONNECTION_INFORMATION_EX_V2);
	connectionInfoExV2->SupportedUsbProtocols.Usb300 = 1;

	success = DeviceIoControl(hHubDevice,
		IOCTL_USB_GET_NODE_CONNECTION_INFORMATION_EX_V2,
		connectionInfoExV2,
		sizeof(USB_NODE_CONNECTION_INFORMATION_EX_V2),
		connectionInfoExV2,
		sizeof(USB_NODE_CONNECTION_INFORMATION_EX_V2),
		&nBytes,
		NULL);

	if (!success || nBytes < sizeof(USB_NODE_CONNECTION_INFORMATION_EX_V2))
	{
		free(connectionInfoExV2);
		connectionInfoExV2 = NULL;
	}
	if (connectionInfoExV2 != NULL)
	{
		if (connectionInfoExV2->SupportedUsbProtocols.Usb300) {
			portType |= USB300;
		}
		else if (connectionInfoExV2->SupportedUsbProtocols.Usb200) {
			portType |= USB200;
		}
		else if (connectionInfoExV2->SupportedUsbProtocols.Usb110) {
			portType |= USB110;
		}
	}
	
	(pConnectionInfoEx)->ConnectionIndex = hubport;

	success = DeviceIoControl(hHubDevice,
		IOCTL_USB_GET_NODE_CONNECTION_INFORMATION_EX,
		pConnectionInfoEx,
		nBytesEx,
		pConnectionInfoEx,
		nBytesEx,
		&nBytesEx,
		NULL);

	if (success)
	{
		//
		// Since the USB_NODE_CONNECTION_INFORMATION_EX is used to display
		// the device speed, but the hub driver doesn't support indication
		// of superspeed, we overwrite the value if the super speed
		// data structures are available and indicate the device is operating
		// at SuperSpeed.
		// 

		if ((pConnectionInfoEx)->Speed == UsbHighSpeed
			&& connectionInfoExV2 != NULL
			&& connectionInfoExV2->Flags.DeviceIsOperatingAtSuperSpeedOrHigher)
		{
			(pConnectionInfoEx)->Speed = UsbSuperSpeed;
		}
	}
	else
	{
		PUSB_NODE_CONNECTION_INFORMATION    connectionInfo = NULL;

		// Try using IOCTL_USB_GET_NODE_CONNECTION_INFORMATION
		// instead of IOCTL_USB_GET_NODE_CONNECTION_INFORMATION_EX
		//

		nBytes = sizeof(USB_NODE_CONNECTION_INFORMATION) +
			sizeof(USB_PIPE_INFO) * 30;

		connectionInfo = (PUSB_NODE_CONNECTION_INFORMATION)malloc(nBytes);

		if (connectionInfo == NULL)
		{

			free(pConnectionInfoEx);
			if (connectionInfoExV2 != NULL)
			{
				free(connectionInfoExV2);
			}
			dwRet = ERROR_NOT_ENOUGH_MEMORY;
			PrintError(dwRet, _T(__FILE__), __LINE__);
			return dwRet;
		}

		connectionInfo->ConnectionIndex = hubport;

		success = DeviceIoControl(hHubDevice,
			IOCTL_USB_GET_NODE_CONNECTION_INFORMATION,
			connectionInfo,
			nBytes,
			connectionInfo,
			nBytes,
			&nBytes,
			NULL);

		if (!success)
		{
			dwRet = GetLastError();
			free(connectionInfo);
			free(pConnectionInfoEx);

			if (connectionInfoExV2 != NULL)
			{
				free(connectionInfoExV2);
			}
			PrintError(dwRet, _T(__FILE__), __LINE__);
			return dwRet;
		}

		// Copy IOCTL_USB_GET_NODE_CONNECTION_INFORMATION into
		// IOCTL_USB_GET_NODE_CONNECTION_INFORMATION_EX structure.
		//
		pConnectionInfoEx->ConnectionIndex = connectionInfo->ConnectionIndex;
		pConnectionInfoEx->DeviceDescriptor = connectionInfo->DeviceDescriptor;
		pConnectionInfoEx->CurrentConfigurationValue = connectionInfo->CurrentConfigurationValue;
		pConnectionInfoEx->Speed = connectionInfo->LowSpeed ? UsbLowSpeed : UsbFullSpeed;
		pConnectionInfoEx->DeviceIsHub = connectionInfo->DeviceIsHub;
		pConnectionInfoEx->DeviceAddress = connectionInfo->DeviceAddress;
		pConnectionInfoEx->NumberOfOpenPipes = connectionInfo->NumberOfOpenPipes;
		pConnectionInfoEx->ConnectionStatus = connectionInfo->ConnectionStatus;

		memcpy(&pConnectionInfoEx->PipeList[0],
			&connectionInfo->PipeList[0],
			sizeof(USB_PIPE_INFO) * 30);

		free(connectionInfo);
	}


	return dwRet;
}

DWORD CUsbport::Get_USB_PORT_CONNECTOR_PROPERTIES()
{
	ENTER_FUNCTION();
	DWORD dwRet(ERROR_SUCCESS);
	BOOL        success = FALSE;
	USB_PORT_CONNECTOR_PROPERTIES          portConnectorProps;
	ZeroMemory(&portConnectorProps, sizeof(portConnectorProps));
	ULONG nBytes = 0;

	if (hHubDevice == INVALID_HANDLE_VALUE || hubport < 1)
	{
		dwRet = ERROR_INVALID_PARAMETER;
		return dwRet;
	}

	//
	// Now query USBHUB for the structures
	// for this port.  This will tell us if a device is attached to this
	// port, among other things.
	// The fault tolerate code is executed first.
	//

	portConnectorProps.ConnectionIndex = hubport;
	success = DeviceIoControl(hHubDevice,
		IOCTL_USB_GET_PORT_CONNECTOR_PROPERTIES,
		&portConnectorProps,
		sizeof(USB_PORT_CONNECTOR_PROPERTIES),
		&portConnectorProps,
		sizeof(USB_PORT_CONNECTOR_PROPERTIES),
		&nBytes,
		NULL);

	if (success && nBytes == sizeof(USB_PORT_CONNECTOR_PROPERTIES))
	{
		int nLen = portConnectorProps.ActualLength;
		pPortConnectorProps = (PUSB_PORT_CONNECTOR_PROPERTIES)malloc(nLen);
		ZeroMemory(pPortConnectorProps, nLen);
		if (pPortConnectorProps != NULL)
		{
			(pPortConnectorProps)->ConnectionIndex = hubport;
			nBytes = 0;
			success = DeviceIoControl(hHubDevice,
				IOCTL_USB_GET_PORT_CONNECTOR_PROPERTIES,
				pPortConnectorProps,
				nLen,
				pPortConnectorProps,
				nLen,
				&nBytes,
				NULL);

			if (!success || nBytes < portConnectorProps.ActualLength)
			{
				PrintError(GetLastError(), _T(__FILE__), __LINE__);
				free(pPortConnectorProps);
				pPortConnectorProps = NULL;
			}
		}
		else {
			logIt(_T("Malloc failed."));
			dwRet = ERROR_NOT_ENOUGH_MEMORY;
		}
	}
	else
	{
		dwRet = GetLastError();
		PrintError(dwRet, _T(__FILE__), __LINE__);
	}

	return dwRet;
}

CString CUsbport::GetDriverKeyName()
{
	BOOL                                success;
	ULONG                               nBytes;
	USB_NODE_CONNECTION_DRIVERKEY_NAME  driverKeyName;
	PUSB_NODE_CONNECTION_DRIVERKEY_NAME driverKeyNameW;
	CString                               dkName;

	driverKeyNameW = NULL;

	driverKeyName.ConnectionIndex = hubport;

	success = DeviceIoControl(hHubDevice, IOCTL_USB_GET_NODE_CONNECTION_DRIVERKEY_NAME, &driverKeyName, sizeof(driverKeyName), &driverKeyName, sizeof(driverKeyName), &nBytes, NULL);
	if (!success)
	{
		goto GetDriverKeyNameError;
	}

	nBytes = driverKeyName.ActualLength;
	if (nBytes <= sizeof(driverKeyName))
	{
		goto GetDriverKeyNameError;
	}

	driverKeyNameW = (PUSB_NODE_CONNECTION_DRIVERKEY_NAME)malloc(nBytes);
	if (driverKeyNameW == NULL)
	{
		goto GetDriverKeyNameError;
	}

	driverKeyNameW->ConnectionIndex = hubport;

	success = DeviceIoControl(hHubDevice, IOCTL_USB_GET_NODE_CONNECTION_DRIVERKEY_NAME, driverKeyNameW, nBytes, driverKeyNameW, nBytes, &nBytes, NULL);
	if (!success)
	{
		goto GetDriverKeyNameError;
	}
	dkName = (TCHAR *)driverKeyNameW->DriverKeyName;
	free(driverKeyNameW);

	return dkName;

GetDriverKeyNameError:
	if (driverKeyNameW != NULL)
	{
		free(driverKeyNameW);
		driverKeyNameW = NULL;
	}

	return dkName;
}

void CUsbport::HubDriverKey()
{
	if (PortConnectHub())
	{
		sDriverKey = GetDriverKeyName();
	}
}

//////////////////////////////////////////////////////////////
//
//
//////////////////////////////////////////////////////////////

CUsbhub::CUsbhub():hHubDevice(INVALID_HANDLE_VALUE), PortCount(0)
{
}


CUsbhub::~CUsbhub()
{
	if (pHubInfoEx = NULL) {
		free(pHubInfoEx);
	}
	if (pHubCapabilityEx = NULL) {
		free(pHubCapabilityEx);
	}

	ClearUsbPorts();

	CloseUsbHub();
}

void CUsbhub::CloseUsbHub()
{
	if (hHubDevice != INVALID_HANDLE_VALUE)
	{
		CloseHandle(hHubDevice);
		hHubDevice = INVALID_HANDLE_VALUE;
	}

}

DWORD CUsbhub::Get_USB_HUB_INFORMATION_EX()
{
	ENTER_FUNCTION();
	DWORD dwRet(ERROR_SUCCESS);
	BOOL        success = FALSE;
	if (hHubDevice == INVALID_HANDLE_VALUE)
	{
		dwRet = ERROR_INVALID_PARAMETER;
		return dwRet;
	}
	ULONG nBytes = 0;

	pHubInfoEx = (PUSB_HUB_INFORMATION_EX)malloc(sizeof(USB_HUB_INFORMATION_EX));
	if (pHubInfoEx == NULL)
	{
		dwRet = ERROR_NOT_ENOUGH_MEMORY;
		PrintError(dwRet, _T(__FILE__), __LINE__);
		return dwRet;
	}
	//
	// Obtain Hub Capabilities
	//
	success = DeviceIoControl(hHubDevice,
		IOCTL_USB_GET_HUB_INFORMATION_EX,
		pHubInfoEx,
		sizeof(USB_HUB_INFORMATION_EX),
		pHubInfoEx,
		sizeof(USB_HUB_INFORMATION_EX),
		&nBytes,
		NULL);

	//
	// Fail gracefully
	//
	if (!success || nBytes < sizeof(USB_HUB_INFORMATION_EX))
	{
		pHubInfoEx = NULL;
		dwRet = GetLastError();
		PrintError(dwRet, _T(__FILE__), __LINE__);
	}

	return dwRet;
}


DWORD CUsbhub::Get_USB_HUB_CAPABILITIES_EX()
{
	ENTER_FUNCTION();
	DWORD dwRet(ERROR_SUCCESS);
	BOOL        success = FALSE;
	if (hHubDevice == INVALID_HANDLE_VALUE)
	{
		dwRet = ERROR_INVALID_PARAMETER;
		return dwRet;
	}
	ULONG nBytes = 0;

	pHubCapabilityEx = (PUSB_HUB_CAPABILITIES_EX)malloc(sizeof(USB_HUB_CAPABILITIES_EX));
	if (pHubCapabilityEx == NULL)
	{
		dwRet = ERROR_NOT_ENOUGH_MEMORY;
		PrintError(dwRet, _T(__FILE__), __LINE__);
		return dwRet;
	}
	//
	// Obtain Hub Capabilities
	//
	success = DeviceIoControl(hHubDevice,
		IOCTL_USB_GET_HUB_CAPABILITIES_EX,
		pHubCapabilityEx,
		sizeof(USB_HUB_CAPABILITIES_EX),
		pHubCapabilityEx,
		sizeof(USB_HUB_CAPABILITIES_EX),
		&nBytes,
		NULL);

	//
	// Fail gracefully
	//
	if (!success || nBytes < sizeof(USB_HUB_CAPABILITIES_EX))
	{
		pHubCapabilityEx = NULL;
		dwRet = GetLastError();
		PrintError(dwRet, _T(__FILE__), __LINE__);
	}
	
	return dwRet;
}

DWORD CUsbhub::ListUsbPorts()
{
	ENTER_FUNCTION();
	DWORD dwRet(ERROR_SUCCESS);
	if (PortCount > 0) {
		for (int i = 1; i <= PortCount; i++) {
			CUsbport* tmpPort = new CUsbport();
			if (!tmpPort->SetHubHandle(hHubDevice, i) || !tmpPort->IsUserConnectable()) {
				delete tmpPort;
				tmpPort = NULL;
				continue;
			}

			m_usbPorts[i] = tmpPort;
		}
	}
	return dwRet;
}

void CUsbhub::ClearUsbPorts()
{
	ENTER_FUNCTION();
	for each(auto item in m_usbPorts) {
		if(item.second!=NULL)
			delete item.second;
	}
	m_usbPorts.clear();
}

DWORD CUsbhub::HubLocationPaths() {
	BYTE pdata[4096] = { 0 };
	DWORD nLen = sizeof(pdata);
	DWORD nType = 0;
	DWORD dwRet(ERROR_SUCCESS);
	ENTER_FUNCTION();
	if ((dwRet = GetDeviceCommonSymbl((TCHAR *)HubName.GetString(), (PDEVPROPKEY)&DEVPKEY_Device_LocationPaths, pdata, nLen, nType)) == ERROR_SUCCESS)
	{
		if (nType == DEVPROP_TYPE_STRING_LIST)
		{
			//add locationPaths
			sLocationpath = (TCHAR *)pdata;
		}
	}
	else
	{
		PrintError(dwRet, _T(__FILE__), __LINE__);
	}
	return dwRet;
}


DWORD CUsbhub::HubExtraInfo() {
	BYTE pdata[4096] = { 0 };
	DWORD nLen = sizeof(pdata);
	DWORD nType = 0;
	DWORD dwRet(ERROR_SUCCESS);
	ENTER_FUNCTION();
	if ((dwRet = GetDeviceCommonSymbl((TCHAR *)HubName.GetString(), (PDEVPROPKEY)&DEVPKEY_Device_InstanceId, pdata, nLen, nType)) == ERROR_SUCCESS)
	{
		sInstanceID = (TCHAR *)pdata;
	}
	else
	{
		PrintError(dwRet, _T(__FILE__), __LINE__);
	}
	nLen = sizeof(pdata); nType = 0;
	if ((dwRet = GetDeviceCommonSymbl((TCHAR *)HubName.GetString(), (PDEVPROPKEY)&DEVPKEY_Device_Driver, pdata, nLen, nType)) == ERROR_SUCCESS)
	{
		sDriverKey = (TCHAR *)pdata;
	}
	else
	{
		PrintError(dwRet, _T(__FILE__), __LINE__);
	}

	return dwRet;
}

DWORD CUsbhub::OpenUSBHub()
{
	DWORD dwRet(ERROR_SUCCESS);
	if (hHubDevice == INVALID_HANDLE_VALUE) {
		hHubDevice = CreateFile(HubName, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, NULL);
		if (hHubDevice == INVALID_HANDLE_VALUE) {
			dwRet = GetLastError();
		}
	}

	return dwRet;
}

DWORD CUsbhub::HubPortCount() {
	DWORD dwRet(ERROR_SUCCESS);
	if ((dwRet=OpenUSBHub()) == ERROR_SUCCESS)
	{
		DWORD BytesReturned;
		USB_NODE_INFORMATION nodeInformation;
		ZeroMemory(&nodeInformation, sizeof(USB_NODE_INFORMATION));
		if (DeviceIoControl(hHubDevice, IOCTL_USB_GET_NODE_INFORMATION, &nodeInformation, sizeof(USB_NODE_INFORMATION), &nodeInformation, sizeof(USB_NODE_INFORMATION), &BytesReturned, NULL))
		{
			PortCount = nodeInformation.u.HubInformation.HubDescriptor.bNumberOfPorts;
		}
		else
		{
			dwRet = GetLastError();
			PrintError(dwRet, _T(__FILE__), __LINE__);
			CloseHandle(hHubDevice);
			hHubDevice = INVALID_HANDLE_VALUE;
		}
	}
	EXIT_FUNCTRET(dwRet);
	return dwRet;
}

