#pragma once

#include <map>

#define USB110	0x01
#define USB200	0x20
#define USB300	0x40

class CUsbport 
{
private:
	HANDLE hHubDevice;
	int hubport;


	PUSB_NODE_CONNECTION_INFORMATION_EX    pConnectionInfoEx = NULL;
	PUSB_PORT_CONNECTOR_PROPERTIES         pPortConnectorProps = NULL;

	int portType;

	DWORD Get_USB_NODE_CONNECTION_INFORMATION_EX();
	DWORD Get_USB_PORT_CONNECTOR_PROPERTIES();

	CString sDriverKey;
	CString GetDriverKeyName();
	void HubDriverKey();

public:
	CUsbport();
	~CUsbport();

	CString GetDriverKey() {
		return sDriverKey;
	}

	BOOL PortConnected() {
		return pConnectionInfoEx->ConnectionStatus != NoDeviceConnected;
	}

	BOOL PortConnectHub() {		
		return pConnectionInfoEx->DeviceIsHub;
	}

	USHORT PID() {
		return pConnectionInfoEx->DeviceDescriptor.idProduct;
	}

	USHORT VID() {
		return pConnectionInfoEx->DeviceDescriptor.idVendor;
	}

	BOOL SetHubHandle(HANDLE h, int port)
	{
		hHubDevice = h;
		hubport = port;

		Get_USB_NODE_CONNECTION_INFORMATION_EX();
		Get_USB_PORT_CONNECTOR_PROPERTIES();
		HubDriverKey();
		return pConnectionInfoEx != NULL;
	}

	int GetPortType() {
		return portType;
	}

	int GetHubPort() {
		return hubport;
	}

	BOOL IsUserConnectable() {
		if (pPortConnectorProps == NULL) return true;
		return pPortConnectorProps->UsbPortProperties.PortIsUserConnectable;
	}

	BOOL IsDebugCapable() {
		if (pPortConnectorProps == NULL) return false;
		return pPortConnectorProps->UsbPortProperties.PortIsDebugCapable;
	}

	int CompanionHubPort() {
		if (pPortConnectorProps == NULL) return 0;
		return pPortConnectorProps->CompanionPortNumber;
	}

	CString CompanionHubName() {
		CString CompanionHubName;
		if (pPortConnectorProps == NULL) return CompanionHubName;
		CompanionHubName.Format(_T("%ws"), pPortConnectorProps->CompanionHubSymbolicLinkName);
		return CompanionHubName;
	}

};

class CUsbhub
{
public:
	CUsbhub();
	~CUsbhub();

	CString HubName;
	int		PortCount;

private:
	PUSB_HUB_CAPABILITIES_EX			   pHubCapabilityEx = NULL;
	PUSB_HUB_INFORMATION_EX				   pHubInfoEx = NULL;

	CString sLocationpath;
	CString sInstanceID;
	CString sDriverKey;
	CString sParent;

	BOOL isUsed;

	HANDLE hHubDevice;
	
	std::map<int, CUsbport*> m_usbPorts;

	void ClearUsbPorts();

	DWORD OpenUSBHub();

public:
	BOOL IsUsed() {
		return isUsed;
	}

	void SetParent(CString s) {
		sParent = s;
	}

	CString GetParent() {
		return sParent;
	}

	CString GetInstanceID()
	{
		return sInstanceID;
	}

	CString GetDriverKey()
	{
		return sDriverKey;
	}

	std::map<int, CUsbport*> USBPorts() {
		return m_usbPorts;
	}

	void CloseUsbHub();
	DWORD HubLocationPaths();
	DWORD HubExtraInfo();
	DWORD HubPortCount();
	DWORD Get_USB_HUB_INFORMATION_EX();
	DWORD Get_USB_HUB_CAPABILITIES_EX();

	USB_HUB_TYPE GetUSbHubType() {
		return pHubInfoEx->HubType;
	}

	void SetHubLocationPaths(CString s) {
		sLocationpath = s;
	}

	CString GetHubLocationPaths() {
		return sLocationpath;
	}

	CString GetHubPortLocationPaths(int index) {
		CString sPortLoc;
		sPortLoc.Format(_T("#USB(%d)"), index);
		return sLocationpath + sPortLoc;
	}

	DWORD ListUsbPorts();
};

