#pragma once
#include <list>
#include "CUsbhub.h"
class CAllHubs
{
public:
	CAllHubs();
	~CAllHubs();
private:
	std::map<CString, CUsbhub*> locpathhubinfo;
	DWORD CreateCalibrateFile(CUsbhub *PHub, CUsbport* PUsbPort, int indexlabel, BOOL bCompanion = FALSE);
	DWORD CreateCalibrateFile(CString hubname, int hubport, CString sLocation, int indexlabel, int porttype);
	BOOL FindLocPathsFromHubName(CString sHubName, CString &sLocpaths);

	void CalcLocationPaths(std::list<CUsbhub *> phubs);
	DWORD HubParent(CString HubName, CString &sParent);

public:
	DWORD PluginLocationpaths(CString sInstanceID, CString &parentLocation);
	DWORD PrintHubCalibrate(int nLabelStart);
	DWORD PrintHubCalibrate(int nStartLabel, int pid, int vid, CString sPath, int hubport);
	DWORD ListAllUsbHubLocation();
	CUsbhub* FindCompanionHub(CUsbhub *hub, int port, int &companport);

	std::map<CString, CUsbhub*> &LocaPathHubInfo() {
		return locpathhubinfo;
	}

	BOOL FindCalcLoc(CUsbhub* hub);

};

